package it.uniroma1.di.fourprofiles.worker.msgcombiner;

import org.apache.giraph.combiner.MessageCombiner;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.util.C;


public class Worker_MsgCombiner implements MessageCombiner<IntWritable,MapWritable> {

	private final static Logger logger = Logger.getLogger(Worker_MsgCombiner.class);
	
	@Override
	public void combine(IntWritable targetVertexId, MapWritable combinedMessages, MapWritable inputMessageToCombine) {
		
		combinedMessages.putAll(inputMessageToCombine);
		
		
		if (logger.isDebugEnabled()) {
					
					
			        StringBuilder sb = new StringBuilder();
			        
			
					sb.append("targetVertexID "+targetVertexId+C.N2);
					
					sb.append("input message (length="+inputMessageToCombine.size()+"):"+C.N);
					for (Writable key:inputMessageToCombine.keySet()) {
		
							sb.append(key+" ---> "+inputMessageToCombine.get(key)+C.N);
					}
					
					sb.append("combined messages (before length="+inputMessageToCombine.size()+"):"+C.N);
					for (Writable key:combinedMessages.keySet()) {
						
						sb.append(key+" ---> "+combinedMessages.get(key)+C.N);
				
					}
					
					logger.debug(sb.toString());
				
		}
	}

	@Override
	public MapWritable createInitialMessage() {
	
		if (logger.isDebugEnabled()) logger.debug("createInitialMessage()"+C.N4);
		
		return new MapWritable();
	}

	
	
}
