package it.uniroma1.di.fourprofiles.worker.superstep0.gas1;

import java.io.IOException;

import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
//import org.apache.logging.log4j.LogManager;//LOG4J2
//import org.apache.logging.log4j.Logger;//LOG4J2
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;

public class Worker_Superstep0_GAS1 
	extends BasicComputation<
	    IntWritable, // vertex id data type
	    VertexData,  // vertex data type
	    EdgeData,    // edge data type
	    MapWritable  // message data type 
    > {

	private final static Logger logger = Logger.getLogger(Worker_Superstep0_GAS1.class);
	//private static final Logger logger = LogManager.getLogger(Superstep0_GAS1.class);//LOG4J2
	
	
	
	@Override
	public void compute(
			Vertex<IntWritable, VertexData, EdgeData> vertex, 
			Iterable<MapWritable> messages)
		throws IOException {
		
		if (logger.isDebugEnabled()) logger.debug("BEGIN SUPERSTEP "+this.getSuperstep()+" (GAS 1) OF VERTEX "+vertex.getId()+C.S);
		//if (logger.isInfoEnabled()) logger.info("PROCESSING SUPERSTEP "+this.getSuperstep()+" (GAS 1) OF VERTEX "+vertex.getId());
		
		
		IntHashSetWritable sourceNeighborhood = new IntHashSetWritable();
		String sourceNeighborhoodStr = Util.getNeighborhood(vertex, sourceNeighborhood);
		
        //MODIFICA_VICINATO 
		//vertex.getValue().sourceNeighborhood=sourceNeighborhood;
		//vertex.getValue().sourceNeighborhoodStr=sourceNeighborhoodStr;
		
		sendNeighborhoodToNeighborhood(
	    		vertex, 
	    		sourceNeighborhood,
	    		sourceNeighborhoodStr);

	    
		if (logger.isDebugEnabled()) logger.debug("END SUPERSTEP "+this.getSuperstep()+" (GAS 1) OF VERTEX "+vertex.getId()+C.SN);
	   
	   
	
    }
	
	
	
	
	private void sendNeighborhoodToNeighborhood(
			Vertex<IntWritable, VertexData, EdgeData> vertex,
			IntHashSetWritable sourceNeighborhood,
			String neighborhoodStr) {
		
		MapWritable mapW = new MapWritable();
	 
	    // mappa composta sempre da una sola chiave e un solo valore
	    mapW.put(vertex.getId(), sourceNeighborhood);
	   
	    	   
	    if (logger.isDebugEnabled()) logger.debug("SEND MSG \""+vertex.getId()+"->["+neighborhoodStr+"]\" FROM VERTEX "+vertex.getId()+" TO VERTEXES "+neighborhoodStr);
	   
	    //sto spedendo una mappa n che contiene una sola chiave e un solo valore
	    //la chiave identifica il vertice
	    //il valore è un hashset che contiene il vicinato del vertice
	    sendMessageToAllEdges(vertex, mapW ); //sto spedendo n a tutti i vertici incidenti
	    
	}
	

}
