package it.uniroma1.di.fourprofiles.worker.superstep1.persaggr1;

import org.apache.giraph.aggregators.BasicAggregator;
import org.apache.log4j.Logger;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts1;
import it.uniroma1.di.fourprofiles.util.C;

/**
   ### NOTES ###
   
   1)
   C:\TESI_CRISTINA\03 - EBOOKS\APACHE GIRAPH\01_GiraphBookSourceCodes-master\GiraphBookSourceCodes-master\
   chapter04_05\src\main\java\bookExamples\ch5\dataSharing\SUMAggregator.java
   
   2)
   There are
   two types of aggregators: nonpersistent and persistent aggregators. The nonpersistent
   aggregators will be reset to their default initial value at the end of the superstep before
   applying any value changes from workers of the current superstep. The persistent
   aggregators, on the other hand, will not reset the aggregator value and it will apply
   any changes requested by theworkers of the current superstep on the latest aggregator
   value. Since the aggregator value is made available to both master and worker nodes,
   Giraph will keep synchronizing its value across workers even if no worker in the
   next superstep requires to access the aggregator value.
 
   3)
   http://giraph.apache.org/aggregators.html
 */
public class GlobalCounts1Aggregator extends BasicAggregator<GlobalCounts1> {
	
	//private static final Logger logger = LogManager.getLogger(global_countsAggregator.class);
	private final static Logger logger = Logger.getLogger(GlobalCounts1Aggregator.class);
	
	

	@Override
	public GlobalCounts1 createInitialValue() {
		
		
		/*
		### OBSOLETE ###
		// Used to see in log what giraph framework methods call this method
		
		try {
			throw new Exception("createInitialValue");
		}catch (Throwable t) {
			logger.debug(Utils.stackTraceToString(t));
		}*/
		
		
		
		GlobalCounts1 gc1 = new GlobalCounts1();
		
		if (logger.isDebugEnabled()) {
			logger.debug("BEGIN INITIAL AGGREGATED VALUES OF GLOBAL COUNT 1 (gc1):");
			logger.debug("gc1.num_disc="+gc1.num_disc);
			logger.debug("gc1.num_empty="+gc1.num_empty);
			logger.debug("gc1.num_wedges_c="+gc1.num_wedges_c);
			logger.debug("gc1.num_wedges_e="+gc1.num_wedges_e);
			logger.debug("gc1.num_triangles="+gc1.num_triangles);
			logger.debug("gc1.average_num_triple="+gc1.average_num_triple);
			//logger.debug("gc1.global_num_triple="+gc1.global_num_triple);
			logger.debug("gc1.u="+gc1.u);
			logger.debug("gc1.eqn10const="+gc1.eqn10const);
			logger.debug("END INITIAL AGGREGATED VALUES OF GLOBAL COUNT 1 (gc1)"+C.N2);
		}
		
		return gc1;
	}

	

	@Override
	public void aggregate(GlobalCounts1 input) {
		
		
		
		/*
		### OBSOLETE ###
		// Used to see in log what giraph framework methods call this method
		
		try {
			throw new Exception("aggregate");
		}catch (Throwable t) {
			logger.debug(Utils.stackTraceToString(t));
		}*/
		
		
		
		//#######################################################################################
		//contiene il valore aggregato fino a questo momento
		GlobalCounts1 gc1_partial = this.getAggregatedValue();
		//########################################################################################
		
		if (logger.isDebugEnabled()) {
			logger.debug(C.N4);
			logger.debug("BEGIN PREVIOUS AGGREGATED VALUES OF GLOBAL COUNT 1 (gc1):");
			logger.debug("gc1.num_disc="+gc1_partial.num_disc);
			logger.debug("gc1.num_empty="+gc1_partial.num_empty);
			logger.debug("gc1.num_wedges_c="+gc1_partial.num_wedges_c);
			logger.debug("gc1.num_wedges_e="+gc1_partial.num_wedges_e);
			logger.debug("gc1.num_triangles="+gc1_partial.num_triangles);
			logger.debug("gc1.average_num_triple="+gc1_partial.average_num_triple);
			//logger.debug("gc1.global_num_triple="+gc1_partial.global_num_triple);
			logger.debug("gc1.u="+gc1_partial.u);
			logger.debug("gc1.eqn10const="+gc1_partial.eqn10const);
			logger.debug("END PREVIOUS AGGREGATED VALUES OF GLOBAL COUNT 1 (gc1)"+C.N2);
			
//######################### INIZIO LOG DA METTERE TEMPORANEAMENTE A INFO PER STAMPARE SOLAMENTE L'INPUT ####################################
			logger.debug("BEGIN INPUT OF aggregate() METHOD:");
			logger.debug("input.num_disc="+input.num_disc);
			logger.debug("input.num_empty="+input.num_empty);
			logger.debug("input.num_wedges_c="+input.num_wedges_c);
			logger.debug("input.num_wedges_e="+input.num_wedges_e);
			logger.debug("input.num_triangles="+input.num_triangles);
			logger.debug("input.average_num_triple="+input.average_num_triple);
			//logger.debug("input.global_num_triple="+input.global_num_triple);
			logger.debug("input.u="+input.u);
			logger.debug("input.eqn10const="+input.eqn10const);
			logger.debug("END INPUT OF aggregate() METHOD"+C.N2);
//######################### FINE LOG DA METTERE TEMPORANEAMENTE A INFO PER STAMPARE SOLAMENTE L'INPUT ####################################
		}
		
			
			
		gc1_partial.num_disc.set(
				gc1_partial.num_disc.get().add(input.num_disc.get()));
		gc1_partial.num_empty.set(
				gc1_partial.num_empty.get().add(input.num_empty.get()));
		gc1_partial.num_wedges_c.set(
				gc1_partial.num_wedges_c.get().add(input.num_wedges_c.get()));
		gc1_partial.num_wedges_e.set(
				gc1_partial.num_wedges_e.get().add(input.num_wedges_e.get()));
		gc1_partial.num_triangles.set(
				gc1_partial.num_triangles.get().add(input.num_triangles.get()));
		gc1_partial.average_num_triple.set(
				gc1_partial.average_num_triple.get() + input.average_num_triple.get());
		gc1_partial.global_num_triple.set(
		gc1_partial.global_num_triple.get().add(input.global_num_triple.get()));		
		
		
		
		
		/*
		Le istruzioni nel corpo dell' "if (U.GLOBAL_3==0)" seguente non sono necessarie se viene richiesto
		di calcolare solamente il 3global profiles e non il 4global profiles. Pertanto, per evitare di eseguire 
		queste istruzioni nel metodo compute della classe Worker_Superstep1 viene impostata la variabile globale
		U.GLOBAL come mostrato dalle istruzioni riportate, per comodità, di seguito:
		
		boolean is3Global = Boolean.parseBoolean(
				getConf().get(U.THREE_GLOBAL_PARAMETER));
		
		//Questo set deve essere fatto prima della chiamamta del metodo aggregate
		if (is3Global==true) U.GLOBAL_3=1; else U.GLOBAL_3=0;		
		
		
		Una volta impostata la variabile globale, il seguente "if (U.GLOBAL_3==0)" determina se eseguire le istruzioni
		necessarie in caso di 4global o 4globalFromLocal ma non 3global
		*/
		if (C.GLOBAL_3==0) {
		
			/* ### NOTES ###
			   Al metodo aggregate viene sempre passato un vettore di dimensione 12 poiché nella classe Worker_Superstep1_GAS2 vengono
			   calcolati per ogni vertice i primi 12 elementi del vettore u, i restanti 5 elementi verranno invece calcolati nella
			   classe Worker_Superstep2_GAS4 
			 */
			for(int i=0; i<C.USIZE_12; i++) {
				
				gc1_partial.u.set(i, new BigIntegerWritable(
						gc1_partial.u.get(i).get().add(input.u.get(i).get())));
			}
	
		    gc1_partial.eqn10const.set(
		    		gc1_partial.eqn10const.get().add(input.eqn10const.get()));
		}
		
	    if (logger.isDebugEnabled()) {
	    	logger.debug("GLOBAL_3 (0==>is3Global=false -- 1==>is3Global=true):="+C.GLOBAL_3);
	    	logger.debug("BEGIN UPDATED AGGREGATED VALUES OF GLOBAL COUNT 1 (gc1):");
			logger.debug("gc1.num_disc="+gc1_partial.num_disc);
			logger.debug("gc1.numO_empty="+gc1_partial.num_empty);
			logger.debug("gc1.num_wedges_c="+gc1_partial.num_wedges_c);
			logger.debug("gc1.num_wedges_e="+gc1_partial.num_wedges_e);
			logger.debug("gc1.num_triangles="+gc1_partial.num_triangles);
			logger.debug("gc1.average_num_triple="+gc1_partial.average_num_triple);
			//logger.debug("gc1.global_num_triple="+gc1_partial.global_num_triple);
			logger.debug("gc1.u="+gc1_partial.u);
			logger.debug("gc1.eqn10const="+gc1_partial.eqn10const);
			logger.debug("END UPDATED AGGREGATED VALUES OF GLOBAL COUNT 1 (gc1)"+C.N2);
			logger.debug(C.N4);
	    }
	    
	    /* Not Useful
		//#########################################################################		
		if (logger.isInfoEnabled() && U.CUSTOM_INFO_LOG_LEVEL >= U.CUSTOM_INFO_LOG_LEVEL_1)
			logger.info("aggregate gc1.u="+gc1_partial.u);
		//#########################################################################		
		*/

		
		///////////////////////////////////////////////////////////////////////////////////
	    //aggiorna il valore aggregato fino a questo momento
		this.setAggregatedValue(gc1_partial);
		///////////////////////////////////////////////////////////////////////////////////
		
	}


}
