package it.uniroma1.di.fourprofiles.worker.superstep1.gas2;

import java.math.BigInteger;
import java.util.Iterator;

import org.apache.giraph.edge.MutableEdge;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
//import org.apache.logging.log4j.LogManager;//LOG4J2
//import org.apache.logging.log4j.Logger;//LOG4J2
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDs;
import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDsHashSetWritable;
import it.uniroma1.di.fourprofiles.data.memory.edge.EdgeCounts;
import it.uniroma1.di.fourprofiles.data.memory.twohop.TwoHop;
import it.uniroma1.di.fourprofiles.data.memory.vertex.VertexDataMem;
import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts1;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global3Profiles;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global4Profiles;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;
import it.uniroma1.di.fourprofiles.worker.superstep1.gas3.Worker_Superstep1_GAS3;
import it.uniroma1.di.fourprofiles.worker.superstep2.gas4.Worker_Superstep2_GAS4;

public class Worker_Superstep1_GAS2 {

	private final static Logger logger = Logger.getLogger(Worker_Superstep1_GAS2.class);
	//private static final Logger logger = LogManager.getLogger(Superstep1_GAS2.class);//LOG4J2
	
	

	/**
	 * This method calculate:
	 * - u vector (first 12 elements, last 5 elements will be calculated by Worker_Superstep2_GAS4 only if "-ca globalFromLocal=true" argument is set)
	 * - num_disc, num_empty, num_wedges_c, num_wedges_e, num_triangles scalars
	 * 
	 * All these values will be aggregated by global_counts1Aggregator 
	 * (together with the eq10const calculated by Worker_Superstep1_GAS3)
	 * 
	 * 
	 * Furthermore this method calculates conn_neighboors of current vertex that is used by Worker_Superstep1_GAS3 (only if "-ca global=true" argument is not set)
	 * to calculate eq10_const_sum
	 */
	public GlobalCounts1 execute_gas2(
				Vertex<IntWritable, VertexData, EdgeData> sourceVertex,
				VertexDataMem sourceVertexM,
				boolean is3Global) {
		
		if (logger.isDebugEnabled()) logBeginGAS2(sourceVertex, sourceVertexM);
		
        
        EdgeCounts ecounts = new EdgeCounts();
        


        /**
         * This method performs a for cycle on every edge of current vertex to calculate:
         * - edge values for local 3 Profiles
         * - edge counts
         * - two hop (memorized in ecounts parameter)
         * - conn_neighbors (memorized in vertexM parameter)
         */
        processAllEdgesOfCurrentVertex(
    			sourceVertex,
    			sourceVertexM,
    			ecounts,
    			is3Global);
		
		if (logger.isDebugEnabled()) {
			logger.debug("ALL EDGES COUNTS OF VERTEX "+sourceVertex.getId()+" HAS BEEN CALCULATED:"+C.N);
			logger.debug("- ecounts_n1 = "+ecounts.n1+", ecounts_n3 ="+ecounts.n3+", ecounts_n1_n3 = "+ecounts.n1_n3+", ecounts_n2c_n2e = "+ecounts.n2c_n2e+", etc.");
			logger.debug("  will be used to calculate scalars and u vector"+C.N);
			logger.debug("- ecounts_twohop = "+ecounts.twohop);
			logger.debug("  ecounts_twohop will be used to calculate u[11]"+C.N);
			logger.debug("- conn_neighbors = "+sourceVertexM.conn_neighbors);
			logger.debug("  if \"-ca global=true\" argument is set, then conn_neighboors of vertex "+sourceVertex.getId()+" will be used by Worker_Superstep1_GAS3 " + 
					"to calculate eq10_const_sum (that in turn will be used by "+Master_Superstep2_Global4Profiles.class.getSimpleName()+
					" to calculate toInvert[9])"+C.N4);
		}
		
		
		
		if (logger.isDebugEnabled()) logger.debug("BEGIN CALCULUS OF LOCAL 3-PROFILES OF VERTEX "+sourceVertex.getId()+C.SN2);
		
		/*
		 * Calculate local 3-profiles: num_triangles, num_wedges_c, num_wedges_e, num_disc, num_empty values of vertex
		 *  
		 * @@@@@ 4-PROFILES PAPER - pag. 485 - Figure 3 (a) @@@@@
		 * @@@@@ 3-PROFILES PAPER - paragraph 4.1 - (12) @@@@@
		 */
		calculateLocal3Profiles(
				sourceVertex,
				sourceVertexM,
				ecounts);

		if (logger.isDebugEnabled()) logger.debug("END CALCULUS OF LOCAL 3-PROFILES OF VERTEX "+sourceVertex.getId()+C.SN4);
		
		if (is3Global == false) {
		
			if (logger.isDebugEnabled()) logger.debug("BEGIN CALCULUS OF u VECTOR OF VERTEX "+sourceVertex.getId()+C.SN2);
			/*
			 *  Calculate first 12 elements of u vector
			 *  (last 5 elements of u vector will be calculated by Worker_Superstep2_GAS4.calculateUVector() only if "-ca globalFromLocal=true" argument is set)
			 */
			// STOP 3PROFILE
			// SALTARE QUESTO FOR SE IL NUOVO PARAMETRO E' =3
			calculateUVector(
					sourceVertex, 
					sourceVertexM,
					ecounts);
			if (logger.isDebugEnabled()) logger.debug("END CALCULUS OF u VECTOR OF VERT)EX "+sourceVertex.getId()+C.SN4);
	
		}
		
		if (logger.isDebugEnabled()) logger.debug("END GAS 2 OF VERTEX "+sourceVertex.getId() + " (SUPERSTEP "+sourceVertexM.superstep+")"+C.SN4);	
		
		
		GlobalCounts1 gc1 = buildGlobalCount1(sourceVertex);
		
			
		return gc1;
	}



	private void logBeginGAS2(Vertex<IntWritable, VertexData, EdgeData> sourceVertex, VertexDataMem sourceVertexM) {
		logger.debug("BEGIN GAS 2 OF VERTEX "+sourceVertex.getId() + " (SUPERSTEP "+sourceVertexM.superstep+")"+C.SN);	
		
		logger.debug("IN GAS 2 WILL BE CALCULATED:"+C.N);
		
		logger.debug("- local 3 profiles"+C.N);
		
		logger.debug("- n3, n2c for every edge of vertex");
		logger.debug("  if \"-ca globalFromLocal=true\" argument is set, then n3 and n2c will be used in "+Worker_Superstep2_GAS4.class.getSimpleName()+" to calculate last 5 elements of u vector of vertex "+sourceVertex.getId()+C.N);
		
		logger.debug("- num_disc, num_empty, num_wedges_c, num_wedges_e, num_triangles");
		logger.debug("  will be used to calculate global 3-Profiles in "+Master_Superstep2_Global3Profiles.class.getSimpleName()+C.N);
		
		logger.debug("- u vector (first 12 elements)");
		logger.debug("  some of them will be used* to calculate global 4-profiles in "+Master_Superstep2_Global4Profiles.class.getSimpleName());
		logger.debug("  *(together with eq10const that will be calculated in "+Worker_Superstep1_GAS3.class.getSimpleName()+")"+C.N4);
		

		
		int k=1;
		logger.debug("EDGES OF VERTEX "+sourceVertex.getId()+" TO BE PROCESSED:");
		for (IntWritable targetVertexId:sourceVertexM.sourceNeighborhood) {
			String n = (k==sourceVertexM.sourceNeighborhood.size())?C.N2:C.E;
			
			logger.debug("  "+sourceVertex.getId()+" -> "+targetVertexId+n);
			k++;
		}
	}

	
	
	/**
	 * This method performs a for cycle on every edge of current vertex to calculate:
	 * - edge values for local 3 profiles
	 * - edge counts
	 * - two hop (memorized in ecounts parameter)
	 * - conn_neighbors (memorized in vertexM parameter)
	 */
	private void processAllEdgesOfCurrentVertex(
			Vertex<IntWritable, VertexData, EdgeData> sourceVertex,
			VertexDataMem sourceVertexM,
			EdgeCounts ecounts,
			boolean is3Global
	) {
	
		
		

		//Prendo i vicinati mapOfTargetNeighborhoods memorizzati in sourceVertexM 
		//e per ogni chiave (target vertex id)
		for (Writable key:sourceVertexM.mapOfTargetNeighborhoods.keySet()) {
		//https://issues.apache.org/jira/browse/GIRAPH-547
        //for (MutableEdge<IntWritable, edge_data> me:vertex.getMutableEdges()) {
	
			IntWritable targetVertexId = (IntWritable)key;
			if (logger.isDebugEnabled()) logger.debug("START PROCESSING EDGE "+sourceVertex.getId()+" -> "+/*me.getTargetVertexId()*/targetVertexId + C.N);
			
			//costruisco i vicinato del vertice target
		    IntHashSetWritable targetNeighborhood = (IntHashSetWritable)sourceVertexM.mapOfTargetNeighborhoods.get(key);
		    		
	
		
			String sourceNeighborhoodLog = C.E;
			String targetNeighborhoodLog = C.E;
			
			if (logger.isDebugEnabled()) {
				logger.debug("source = " + sourceVertex.getId());
				logger.debug("target = "+targetVertexId);
				sourceNeighborhoodLog = Util.logNeighborhood(sourceVertex.getId(), sourceVertexM.sourceNeighborhood);
				logger.debug("source neighborhood = "+sourceNeighborhoodLog);
				targetNeighborhoodLog = Util.logNeighborhood(/*me.getTargetVertexId()*/targetVertexId, targetNeighborhood);
				logger.debug("target neighborhood = "+targetNeighborhoodLog);
			}
			
			
// START CALCULUS OF INTERSECTION BETWEEN SOURCE AND TARGET NEIGHBORHOOD //////////////////////////			
			IntHashSetWritable intersection = getIntersection(sourceVertexM.sourceNeighborhood, targetNeighborhood);
			
			/* ### NOTES ###
			   intersection is saved on mapOfIntersections parameter in order to reuse it in execute_gas3 method of Worker_Superstep1_GAS3 class */
			sourceVertexM.mapOfIntersections.put(targetVertexId, intersection);
			
			if (logger.isDebugEnabled()) logger.debug("intersection between source and target neighborhoods = "+intersection + C.N2);
// END CALCULUS OF INTERSECTION BETWEEN SOURCE AND TARGET NEIGHBORHOOD ////////////////////////////			
			
			
			
			
			
// START CALCULUS OF EDGE VALUES FOR LOCAL 3-PROFILES /////////////////////////////////////////////////////////////	
			
			EdgeData edge = calculateEdgeValuesForLocal3Profiles(
					sourceVertex, sourceVertexM, targetVertexId, targetNeighborhood, intersection);
			
// END CALCULUS OF EDGE VALUES FOR LOCAL 3-PROFILES ///////////////////////////////////////////////////////////////			

			
			
			

// START CALCULUS OF EDGE COUNTS //////////////////////////////////////////////////////////////////			
			
			calculateEdgeCounts(sourceVertex, ecounts, targetVertexId, edge, is3Global);
			
// END CALCULUS OF EDGE COUNTS ////////////////////////////////////////////////////////////////////
	        
// STOP 3PROFILE dentro ecount abbiamo calcolato le 4 variabili n1, n2e, n2c e n3 necessarie per il 3 profile
// IPOTESI E' DI FARE IL CONTINUE SE IL NUOVO PARAMETRO DI INPUT E' UGUALE A 3

	        if (is3Global == false) {
// START CALCULUS OF ecounts_twohop AND conn_neighbors /////////////////////////////////////////////////
	        
		        if (logger.isDebugEnabled()) logger.debug("##### START OF CALCULUS OF TWO HOP FOR EDGE "+sourceVertex.getId()+" -> "+targetVertexId +" #####");
		        
		    	/*
		    	 * Every invocation of this method has a different targetNeighborhood as parameter.
		    	 * The result of this method is saved in ecounts.twohop parameter.
		    	 */
		        // ### TESI ### paragrafo "Calcolo dell'istogramma 2hop" 
		        calculateTwoHop(
		        		sourceVertex, 
		        		targetVertexId, 
		        		sourceVertexM.sourceNeighborhood,
		        		targetNeighborhood, 
						sourceNeighborhoodLog, 
						targetNeighborhoodLog,
						ecounts.twohop);
		        
		        if (logger.isDebugEnabled()) logger.debug("##### END OF CALCULUS OF TWO HOP FOR EDGE "+sourceVertex.getId()+" -> "+targetVertexId +" #####"+C.N2);
		        
		        
		        
		        
	
				
		        if (logger.isDebugEnabled()) logger.debug("##### START OF CALCULUS OF CLIQUE COUNTING - conn_neighbors FOR EDGE "+sourceVertex.getId()+" -> "+targetVertexId +" #####");
	
		    	/*
		    	 * Calculate Clique Counting
				 * 
				 * ### PAPER ###
				 * 4-PROFILES PAPER - page 486 - paragraph 2.2 Clique Counting
				 * 
				 * 
		    	 * Every invocation of this method has a different targetNeighborhood as parameter.
		    	 * The result of this method is saved in conn_neighbors parameter.
		    	 */
		        calculateConn_neighbors(
		        		sourceVertex, 
		        		targetVertexId, 
		        		intersection,
		        		sourceNeighborhoodLog,
						targetNeighborhoodLog,
						sourceVertexM.conn_neighbors);
	
		        if (logger.isDebugEnabled()) logger.debug("##### END OF CALCULUS OF CLIQUE COUNTING - conn_neighbors FOR EDGE "+sourceVertex.getId()+" -> "+targetVertexId +" #####"+C.N2);
				
				
	// END CALCULUS OF ecounts_twohop AND conn_neighbors ///////////////////////////////////////////////////
	
		        
		        
		        // TODO sembrano non necessarie queste istruzioni poiché edge viene aggiornato tramite puntatori
		        /*
		        me.setValue(edge);  
		        vertex.setEdgeValue(me.getTargetVertexId(), edge);
		        */
		        
		        
		        if (logger.isDebugEnabled()) logger.debug("END PROCESSING EDGE "+sourceVertex.getId()+" -> "+/*me.getTargetVertexId()*/targetVertexId + C.N4);
		        
			}
		} // END FOR CYCLE ON ALL EDGES OF CURRENT VERTEX 
		
		
	}








	private EdgeData calculateEdgeValuesForLocal3Profiles(
			Vertex<IntWritable, VertexData, EdgeData> sourceVertex,
			VertexDataMem sourceVertexM, 
			IntWritable targetVertexId, 
			IntHashSetWritable targetNeighborhood,
			IntHashSetWritable intersection) {
					
		/*
		  ### GRAPHLAB ###
		  
		  if (targetlist.vid_set.size() < srclist.vid_set.size()) {
	        tmp = count_set_intersect(targetlist.vid_set, srclist.vid_set);
	      }
	      else {
	        tmp = count_set_intersect(srclist.vid_set, targetlist.vid_set);
	      }
	      // tmp2 = srclist.vid_set.size() + targetlist.vid_set.size();

	      // ### PAPER ###
	      // vedi in alto a sx pag. 486
	      edge.data().n3 = tmp;

	      edge.data().n2c = srclist.vid_set.size() - tmp - 1;
	      edge.data().n2e = targetlist.vid_set.size() - tmp - 1;

	      edge.data().n1 = context.num_vertices() - 
	                       (srclist.vid_set.size() + targetlist.vid_set.size() - tmp);
		 */
		
		long tmp = intersection.size();
		BigInteger n3 = new BigInteger(String.valueOf(tmp));
		//long vicinato = sourceVertexM.sourceNeighborhood.size();
		//BigInteger triple = new BigInteger(String.valueOf((vicinato*(vicinato-1)/2)));
		
		BigInteger n2c = new BigInteger(String.valueOf(sourceVertexM.sourceNeighborhood.size() - tmp - 1));
		BigInteger n2e = new BigInteger(String.valueOf(targetNeighborhood.size() - tmp - 1));
		
		// https://giraph.apache.org/apidocs/org/apache/giraph/graph/AbstractComputation.html#getTotalNumVertices--
		// TODO verificare che this.getTotalNumVertices() sia uguale rispetto al comportamento di graphlab
		BigInteger n1 = new BigInteger(String.valueOf(
				sourceVertexM.totalNumVertices - (sourceVertexM.sourceNeighborhood.size() + targetNeighborhood.size() - tmp)));

		if (logger.isDebugEnabled()) {
			logger.debug("@@@@@ 4-PROFILES PAPER - top left of pag. 486 @@@@@");
			logger.debug("@@@@@ 3-PROFILES PAPER - paragraph 4.1 - (11) @@@@@");
			logger.debug("tmp="+tmp + "   ( cardinality of intersection between source and target neighborhoods )");
			logger.debug("n3="+n3+ "   ( n3 = tmp )");
			logger.debug("n2c="+n2c+ "   ( n2c = sourceNeighborhood.size() - tmp - 1 )");
			logger.debug("n2e="+n2e+ "   ( n2e = targetNeighborhood.size() - tmp - 1 )");
			logger.debug("n1="+n1+ "   ( n1 = totalNumVertices - (sourceNeighborhood.size() + targetNeighborhood.size() - tmp) )"+C.N2);
			//logger.debug("triple="+triple);
		}
		// vertex è il vertice sorgente e targetVertexId è quello destinazione, 
		// l'oggetto edge è l'arco, nell'arco memorizzo le variabili dell'arco i valori calcolati sopra.

		
		
		
		
		
		//edge_data edge = me.getValue();
		//TODO trovare un alternativa al seguente ciclo for break
		//AGGIORNAMENTO 20/05/2018 qui sembrerebbe che fa lo stesso ciclo:
		//https://www.programcreek.com/java-api-examples/?class=org.apache.giraph.edge.MutableEdge&method=setValue
		EdgeData edge = null;
		for (MutableEdge<IntWritable, EdgeData> me:sourceVertex.getMutableEdges()) {
			if (me.getTargetVertexId().get() == targetVertexId.get()) {
				edge = me.getValue();
				break;
			}
		}
		//edge_data edge=vertex.getEdgeValue(targetVertexId);

			
			
			
					
		// START SAVING OF n3, n2c IN EDGE ////////////////////////////////////////////////////////////////
		// (THEY WILL BE USED IN Worker_Superstep2_GAS4 TO CALCULATE LAST 5 ELEMENTS OF u VECTOR OF SOURCE VERTEX)
		//memorizzo in edge data gli scalari calcolati			
		edge.n3=new BigIntegerWritable (n3);
		edge.n2c=new BigIntegerWritable (n2c);
		
		/* TODO: da verificare!
		 * It is not necessary to save following n2e and n1 members in hadoop file system
		 * because they are only used internally by Worker_Superstep1_GAS2 class, i.e. only in superstep 1.
		 * 
		 * Therefore n2e and n1 members are not used in write and readFields methods of edge_data class.
		 */
		edge.n2e=new BigIntegerWritable (n2e);
		edge.n1=new BigIntegerWritable (n1);
		//edge.triple=new BigIntegerWritable (triple);
		if (logger.isDebugEnabled()) {
			logger.debug("n3, n2c saved in EDGE "+sourceVertex.getId()+" -> "+targetVertexId);
			logger.debug("(if \"-ca globalFromLocal=true\" argument is set, then n3 and n2c will be used in "+Worker_Superstep2_GAS4.class.getSimpleName()+" to calculate last 5 elements of u vector of source vertex "+sourceVertex.getId()+")"+C.N2);
		}
		// END SAVING OF n3, n2c IN EDGE //////////////////////////////////////////////////////////////////			
		return edge;
	}








	private void calculateEdgeCounts(
			Vertex<IntWritable, VertexData, EdgeData> souceVertex, 
			EdgeCounts ecounts,
			IntWritable targetVertexId, 
			EdgeData edge,
			boolean is3Global) {
		
		
		if (logger.isDebugEnabled()) {
			
			logger.debug("gather variables are related to current EDGE "+souceVertex.getId()+" -> "+targetVertexId);
			logger.debug("ecounts variables are related to ALL EDGES of VERTEX "+souceVertex.getId()+C.N);
		}
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n1e,v"
		BigInteger gather_n1 = edge.n1.get();
		ecounts.n1=ecounts.n1.add(gather_n1);
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n3,v"
		BigInteger gather_n3 = edge.n3.get();
		ecounts.n3=ecounts.n3.add(gather_n3);
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n2e,v"
		BigInteger gather_n2e = edge.n2e.get();
		ecounts.n2e=ecounts.n2e.add(gather_n2e);

		// ### TESI ### paragrafo "Equazione per il calcolo di n2c,v"
		BigInteger gather_n2c = edge.n2c.get();
		ecounts.n2c=ecounts.n2c.add(gather_n2c);
		
		//BigInteger gather_triple = edge.triple.get();
		//ecounts.triple=gather_triple;
		
		if (logger.isDebugEnabled()) {
			logger.debug("gather_n1="+gather_n1 + "   ( gather_n1 = n1 )");
			logger.debug("ecounts_n1="+ecounts.n1 + "   ( ecounts_n1+=gather_n1 )"+C.N);

			logger.debug("gather_n3="+gather_n3 + "   ( gather_n3 = edge.n3 )");
			logger.debug("ecounts_n3="+ecounts.n3 + "   ( ecounts_n3+=gather_n3 )"+C.N);
			
			logger.debug("gather_n2e="+gather_n2e + "   ( gather_n2e = edge.n2e )");
			logger.debug("ecounts_n2e="+ecounts.n2e + "   ( ecounts_n2e+=gather_n2e )"+C.N);

		    logger.debug("gather_n2c="+gather_n2c + "   ( gather_n2c = edge.n2c )");
		    logger.debug("ecounts_n2c="+ecounts.n2c + "   ( ecounts_n2c+=gather_n2c )"+C.N);
		    
		    //logger.debug("gather_triple="+gather_triple);
		    //logger.debug("ecounts_triple="+ecounts.triple+U.N);
		}		
		// TODO STOP 3PROFILE
		// TODO Verificare il motivio per cui nel codice graphlab originale queste variabili sono dichiarate con il suffisso _double
		
		if (is3Global == false) {
		
		
			/*
			 * ### PAPER 4-PROFILES ###
			 * equation of left column of pag. 486 
			 */
			
			// equation 1
			BigInteger gather_n1_long = (edge.n1.get().multiply(
											edge.n1.get().subtract(C.ONE)))
										.divide(C.TWO);
			ecounts.n1_double=ecounts.n1_double.add(gather_n1_long);
			
			// equation 2
			BigInteger gather_n2c_long = (edge.n2c.get().multiply(
											edge.n2c.get().subtract(C.ONE)))
										.divide(C.TWO);
			ecounts.n2c_double=ecounts.n2c_double.add(gather_n2c_long);
			
			// equation 3
			BigInteger gather_n3_long = (edge.n3.get().multiply(
											edge.n3.get().subtract(C.ONE)))
										.divide(C.TWO);
			ecounts.n3_double=ecounts.n3_double.add(gather_n3_long);
			
			// equation 4
			BigInteger gather_n1_n2c = edge.n1.get().multiply(edge.n2c.get());
			ecounts.n1_n2c=ecounts.n1_n2c.add(gather_n1_n2c);
			
			// equation 5
			BigInteger gather_n1_n3 = edge.n1.get().multiply(edge.n3.get());
			ecounts.n1_n3=ecounts.n1_n3.add(gather_n1_n3);
			
			// equation 6
			BigInteger gather_n2c_n2e = edge.n2c.get().multiply(edge.n2e.get());
			ecounts.n2c_n2e=ecounts.n2c_n2e.add(gather_n2c_n2e);
			
			
	
			// ### GRAPHLAB ### 
			//gather.n3 = edge.data().n3;
			//gather.n1_long = (edge.data().n1*(edge.data().n1-1))/2;
			//gather.n3_long = (edge.data().n3*(edge.data().n3-1))/2;
			//gather.n1_n3 = edge.data().n1*edge.data().n3;
			//gather.n2c_n2e = edge.data().n2c*edge.data().n2e;
	
			// ### GRAPHLAB ###
			//Questa if non è necessario secondo quanto spiegato nel file 03 EDGE BIDIREZIONALI IN GIRAPH.docx
			//dentro la cartella C:\TESI_CRISTINA\TESI_CRISTINA_GOOGLE_DRIVE\02 - APPUNTI\05 APPUNTI SU APACHE GIRAPH\
			//if (vertex.id() == edge.source().id()){
			
	
	
			// equation 7
			BigInteger gather_n2c_n3 = edge.n2c.get().multiply(edge.n3.get());
			ecounts.n2c_n3=ecounts.n2c_n3.add(gather_n2c_n3);
	
			// equation 9
			BigInteger gather_n2e_long = (edge.n2e.get().multiply(
												edge.n2e.get().subtract(C.ONE)))
										.divide(C.TWO);
			ecounts.n2e_double=ecounts.n2e_double.add(gather_n2e_long);
	
			// equation 10
			BigInteger gather_n1_n2e = edge.n1.get().multiply(edge.n2e.get());
			ecounts.n1_n2e=ecounts.n1_n2e.add(gather_n1_n2e);
	
			// equation 11
			BigInteger gather_n2e_n3 = edge.n2e.get().multiply(edge.n3.get());
			ecounts.n2e_n3=ecounts.n2e_n3.add(gather_n2e_n3);
		
			
			if (logger.isDebugEnabled()) {
			    
			    logger.debug("@@@@@ 4-PROFILES PAPER - equations in left column of page 486 @@@@@"+C.N);
			    
			    // 1
			    logger.debug("equation 1:");
				logger.debug("gather_n1_long="+gather_n1_long + "   ( gather_n1_long = (n1*(n1-1))/2 )");
				logger.debug("ecounts_n1_long="+ecounts.n1_double + "   ( ecounts_n1_long+=gather_n1_long )"+C.N);
				
				// 2
				logger.debug("equation 2:");
				logger.debug("gather_n2c_long="+gather_n2c_long + "   ( gather_n2c_long = (edge.n2c*(edge.n2c-1))/2 )");
			    logger.debug("ecounts_n2c_long="+ecounts.n2c_double + "   ( ecounts_n2c_long+=gather_n2c_long )"+C.N);
			    
				// 3
			    logger.debug("equation 3:");
			    logger.debug("gather_n3_long="+gather_n3_long + "   ( gather_n3_long = (edge.n3*(edge.n3-1))/2 )");
				logger.debug("ecounts_n3_long="+ecounts.n3_double + "   ( ecounts_n3_long+=gather_n3_long )"+C.N);
				
				// 4
				logger.debug("equation 4:");
				logger.debug("gather_n1_n2c="+gather_n1_n2c + "   ( gather_n1_n2c = n1*edge.n2c )");
			    logger.debug("ecounts_n1_n2c="+ecounts.n1_n2c + "   ( ecounts_n1_n2c+=gather_n1_n2c )"+C.N);
			    
				// 5
			    logger.debug("equation 5:");
			    logger.debug("gather_n1_n3="+gather_n1_n3 + "   ( gather_n1_n3 = n1*edge.n3 )");
				logger.debug("ecounts_n1_n3="+ecounts.n1_n3 + "   ( ecounts_n1_n3+=gather_n1_n3 )"+C.N);
				
				// 6
				logger.debug("equation 6:");
				logger.debug("gather_n2c_n2e="+gather_n2c_n2e + "   ( gather_n2c_n2e = edge.n2c*n2e )");
				logger.debug("ecounts_n2c_n2e="+ecounts.n2c_n2e + "   ( ecounts_n2c_n2e+=gather_n2c_n2e )"+C.N);
				
			    // 7
				logger.debug("equation 7:");
				logger.debug("gather_n2c_n3="+gather_n2c_n3 + "   ( gather_n2c_n3 = edge.n2c*edge.n3 )");
			    logger.debug("ecounts_n2c_n3="+ecounts.n2c_n3 + "   ( ecounts_n2c_n3+=gather_n2c_n3 )"+C.N);
			    
			    // 9
			    logger.debug("equation 9:");
			    logger.debug("gather_n2e_long="+gather_n2e_long + "   ( gather_n2e_long = (n2e*(n2e-1))/2 )");
			    logger.debug("ecounts_n2e_long="+ecounts.n2e_double + "   ( ecounts_n2e_long+=gather_n2e_long )"+C.N);
			    
			    // 10
			    logger.debug("equation 10:");
			    logger.debug("gather_n1_n2e="+gather_n1_n2e + "   ( gather_n1_n2e = n1*n2e) )");
			    logger.debug("ecounts_n1_n2e="+ecounts.n1_n2e + "   ( ecounts_n1_n2e+=gather_n1_n2e )"+C.N);
			    
			    // 11
			    logger.debug("equation 11:");
			    logger.debug("gather_n2e_n3="+gather_n2e_n3 + "   ( gather_n2e_n3 = n2e*edge.n3 )");
			    logger.debug("ecounts_n2e_n3="+ecounts.n2e_n3 + "   ( ecounts_n2e_n3+=gather_n2e_n3 )"+C.N2);
			}
		}
	}



	
	/**
	 * This method calculate ecounts_twohop that is the difference between target and source neighborhoods excluding source.
	 * 
	 * Every invocation of this method has a different targetNeighborhood as parameter.
	 * The result of this method is saved in ecounts_twohop parameter.
	 */
	
	// ### TESI ### paragrafo "Calcolo dell'istogramma TWOHOP"
	private void calculateTwoHop(
			Vertex<IntWritable, VertexData, EdgeData> souceVertex,
			IntWritable targetVertexId,
			IntHashSetWritable sourceNeighborhood, 
			IntHashSetWritable targetNeighborhood, 
			String sourceNeighborhoodLog, 
			String targetNeighborhoodLog,
			TwoHop ecounts_twohop) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("EDGE "+souceVertex.getId()+" -> "+/*me.getTargetVertexId()*/targetVertexId);
			logger.debug("source = "+souceVertex.getId());
			logger.debug("target = "+targetVertexId.get());	
			logger.debug("previous ecounts_twohop = "+ ecounts_twohop +"   (calculated on previous processed edges of SOURCE VERTEX "+souceVertex.getId()+")");
			logger.debug("source neighborhood = "+sourceNeighborhoodLog);
			logger.debug("target neighborhood = "+targetNeighborhoodLog);
		}
		
        /* ### GRAPHLAB ###
        const vertex_data_type& targetlist = edge.target().data();
        nbh_intersect_complement(targetlist.vid_set, 
        
                edge.source().data().vid_se t, 
                edge.source().id(), // NOTA BENE qui graphlab prende il source.id perché 
                					// potrebbe essere diverso dal vertex id
                gather.b);
        */
		TwoHop gather_twohop = TwoHop.calculateTwoHop(
				targetNeighborhood, 
				sourceNeighborhood,
				souceVertex.getId());
		
		if (logger.isDebugEnabled()) {
			logger.debug("gather_twohop = "+gather_twohop.toString());
			logger.debug("gather_twohop is a list of pairs (vertex, count) where:");
			logger.debug("- vertex belongs to difference between target and source neighborhoods excluding source");
			logger.debug("- count is number of times that vertex occurs");
			
			if (!gather_twohop.map_vertex_count.isEmpty())
				logger.debug("(count is initialized to 1 for every vertex)"+C.N);
		}
		
		ecounts_twohop.sumTwoHop(gather_twohop);  // ecounts_twohop contiene la somma insiemistica di tutte le differenze di vicinato per ogni target 
		
		if (logger.isDebugEnabled()) {
			logger.debug("ecounts_twohop="+ecounts_twohop.toString());
			logger.debug("ecounts_twohop is the sum of all gather_twohop calculated for every edge of SOURCE VERTEX "+souceVertex.getId());
			logger.debug("ecounts_twohop will be used after to calculate u[11] of SOURCE VERTEX "+souceVertex.getId());
		}
	}	

	
	
	
	/**
	 * Calculate Clique Counting
	 * 
	 * ### PAPER ###
	 * 4-PROFILES PAPER - page 486 - paragraph 2.2 Clique Counting
	 * 
	 * conn_neighbors 
	 * It is a set of twoids obtained iterating on all intersections between source neighborhood and target neighborhoods
	 * In every twoids:
	 * - the first one is the always the source vertex id
	 * - the second one is a vertex belonging to neighborhoods intersection always having id greater than source vertex id
	 * 
	 * Every invocation of this method has a different intersection as parameter.
	 * The result of this method is saved in conn_neighbors parameter.
	 * 
	 * - if "-ca global=true" argument is set, then conn_neighboors will be used by Worker_Superstep1_GAS3
	 *   to calculate eq10_const_sum
	 * 
	 * - if "-ca globalFromLocal=true" argument is set, then conn_neighboors will be used by Worker_Superstep2_GAS4
	 *   to calculate ecounts_h8v, ecounts_h10v
	 */
	private void calculateConn_neighbors(
			Vertex<IntWritable, VertexData, EdgeData> souceVertex,
			IntWritable targetVertexId, 
			IntHashSetWritable intersection,
			String sourceNeighborhoodLog,
			String targetNeighborhoodLog, 
			TwoIDsHashSetWritable conn_neighbors) {
		
		/* ### GRAPHLAB ###
		for(size_t i=0;i<interlist.size();i++){

		    if (edge.target().id()< interlist.at(i)){
		      twoids ele;
		      ele.first=edge.target().id();
		      ele.second=interlist.at(i);
		      gather.common.push_back(ele);
		    }
		  For the accumulating src, if target < member of list then push (target,member). Avoids long counts,
		  if inequality is other way, this will be done at the other vertex 
		}*/

		
		Iterator<IntWritable> iter = intersection.iterator();  // dichiaro iter di tipo iterator per scorrere intersection

		
		// conn_neighbors 
		// insieme di twoids costruiti scorrendo l'intersezione tra il vicinato del sorgente e il vicinato del destinatario,
		// nello specifico in ogni twoids il primo è sempre il sorgente e il secondo è sempre uno dei vertici dell'intersezione avente
		// id maggiore del sorgente
		
		if (logger.isDebugEnabled()) {
			logger.debug("@@@@@ 4-PROFILES PAPER - paragraph 2.2 Clique Counting - page 486 @@@@@");
			logger.debug("EDGE "+souceVertex.getId()+" -> "+/*me.getTargetVertexId()*/targetVertexId);
			logger.debug("source = "+souceVertex.getId());
			logger.debug("target = "+targetVertexId.get());
			logger.debug("previous conn_neighbors = "+conn_neighbors +"   (calculated on previous processed edges of SOURCE VERTEX "+souceVertex.getId()+")");
			logger.debug("source neighborhood = "+sourceNeighborhoodLog);
			logger.debug("target neighborhood = "+targetNeighborhoodLog);
			logger.debug("intersection = "+intersection+"   (between source and target neighborhoods)");
		}
		
		if (!intersection.isEmpty()) {
			if (logger.isDebugEnabled()) logger.debug("iterating on intersection:");
		    int i = 0;
		    boolean newTwoidsFound = false;
		    while (iter.hasNext()) {                     // finché iterator ha un successivo si procede
		    	IntWritable i_intersection = iter.next();   // prendo l'i-esimo elemento della lista (i_intersection)
		    	if (logger.isDebugEnabled()) {
			    	logger.debug("");
			    	logger.debug("   index "+i+" of intersection:");
			    	logger.debug("   intersection["+i+"] = "+i_intersection);
		    	}
		    	if (/*me.getTargetVertexId()*/targetVertexId.get()<i_intersection.get()) {
		    		newTwoidsFound = true;
		    		TwoIDs gather_ele = new TwoIDs();
		    		gather_ele.first=/*me.getTargetVertexId()*/targetVertexId;
		    		gather_ele.second=i_intersection;
		    		
		    		if (logger.isDebugEnabled()) logger.debug("   new twoids found = ("+targetVertexId+", "+i_intersection+")   ((target="+targetVertexId+", intersection["+i+"]="+i_intersection+") where target="+targetVertexId+" < intersection["+i+"]="+i_intersection+")");
		    		conn_neighbors.add(gather_ele); // poiché è un insieme non devo fare "+=" ma devo fare "add"
		    		if (logger.isDebugEnabled()) logger.debug("   new twoids just found added to conn_neighbors that now is = "+conn_neighbors);
		       	}
		    	else {
		    		if (logger.isDebugEnabled()) logger.debug("   new twoids not found   (target="+targetVertexId+" >= intersection["+i+"]="+i_intersection+")");
		    	}
		    	i++;
		    	
		    }
		    
		    
		    if (!newTwoidsFound) {
		    	if (logger.isDebugEnabled()) logger.debug("no new twoids found, conn_neighbors remain = "+conn_neighbors);
		    }
		    else {
		    	if (logger.isDebugEnabled()) logger.debug("updated conn_neighbors = "+conn_neighbors);
		    }
		}
		else {
			if (logger.isDebugEnabled()) logger.debug("since intersection is empty no new twoids are added to conn_neighbors that remain = "+conn_neighbors);
		}
		
	}




	


	/**
	 * Calculate local 3-profiles: num_triangles, num_wedges_c, num_wedges_e, num_disc, num_empty values of vertex
	 * 
	 * @@@@@ 4-PROFILES PAPER - pag. 485 - Figure 3 (a) @@@@@
	 * @@@@@ 3-PROFILES PAPER - paragraph 4.1 - (12) @@@@@
	 */
	private void calculateLocal3Profiles(
			Vertex<IntWritable, VertexData, EdgeData> vertex, 
			VertexDataMem vertexM,
			EdgeCounts ecounts) {
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n3,v"
		vertex.getValue().num_triangles=new BigIntegerWritable(ecounts.n3.divide(C.TWO));
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n2c,v"
		vertex.getValue().num_wedges_c=new BigIntegerWritable(ecounts.n2c.divide(C.TWO));
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n2e,v"
		vertex.getValue().num_wedges_e=new BigIntegerWritable(ecounts.n2e);
				
		long vicinato = vertexM.sourceNeighborhood.size();
		BigInteger triple = new BigInteger(String.valueOf((vicinato*(vicinato-1)/2)));
		vertex.getValue().global_num_triple = new BigIntegerWritable(triple);
				
		if (triple.compareTo(BigInteger.ZERO) != 0) {
				
				vertex.getValue().average_num_triple=new DoubleWritable(
						vertex.getValue().num_triangles.get().doubleValue()/
						triple.doubleValue()
					);
				
		}
		else {
				vertex.getValue().average_num_triple=new DoubleWritable(0.0);
		}
		
		if (logger.isDebugEnabled()) {
			logger.debug("@@@@@ 4-PROFILES PAPER - pag. 485 - Figure 3 (a) @@@@@");
			logger.debug("@@@@@ 3-PROFILES PAPER - paragraph 4.1 - (12) @@@@@");
			logger.debug("vertex.num_triangles="+vertex.getValue().num_triangles+ "   ( num_triangles=ecounts_n3/2 )");
			logger.debug("vertex.num_wedges_c="+vertex.getValue().num_wedges_c+ "   ( num_wedges_c=ecounts_n2c/2 )");
			logger.debug("vertex.num_wedges_e="+vertex.getValue().num_wedges_e+ "   ( num_wedges_e=ecounts_n2e )");		
			logger.debug("vertex.average_triple="+vertex.getValue().average_num_triple);
			//logger.debug("vertex.global_num_triple="+vertex.getValue().global_num_triple);
			
		}

		
		
		
		// ### GRAPHLAB ###
		// https://giraph.apache.org/apidocs/org/apache/giraph/graph/AbstractComputation.html#getTotalNumEdges--
		/*
	    vertex.data().num_disc = ecounts.n1 + total_edges -
	        (vertex.data().vid_set.size() + vertex.data().num_wedges_e + vertex.data().num_triangles);
		 */
		// ### TESI ### paragrafo "Equazione per il calcolo di n1d,v"
		BigInteger num_disc=
				// ### PAPER ###
				// ecount_n1 sarebbe n1e = SOMMATORIA n1 PER OGNI VERTICE APPARTENENTE AL VICINATO DI v
				// (VEDI PARAGRAFO 4.1, LA SECONDA FORMULA DI (12)
				/*ecounts.n1+
				    vertexM.totalNumEdges-
					(vertex.getValue().sourceNeighborhood.size()+
					vertex.getValue().num_wedges_e.get()+
					vertex.getValue().num_triangles.get());*/
				ecounts.n1.add(
				    new BigInteger(String.valueOf(vertexM.totalNumEdges)).subtract(
						new BigInteger(String.valueOf(vertexM.sourceNeighborhood.size())).add(
						vertex.getValue().num_wedges_e.get().add(
						vertex.getValue().num_triangles.get()))));
		
		vertex.getValue().num_disc=new BigIntegerWritable(num_disc);
		if (logger.isDebugEnabled()) logger.debug("vertex.num_disc="+vertex.getValue().num_disc+"   ( num_disc=ecounts_n1+totalNumEdges-sourceNeighboorhod.size()+num_wedges_e+num_triangles )");
		
		// ### TESI ### paragrafo "Equazione per il calcolo di n1e,v"
		vertex.getValue().num_disc_e=new BigIntegerWritable(ecounts.n1);
		
		// ### GRAPHLAB ###
		// https://giraph.apache.org/apidocs/org/apache/giraph/graph/AbstractComputation.html#getTotalNumVertices--
		/*
    	vertex.data().num_empty = (context.num_vertices()  - 1)*(context.num_vertices() - 2)/2 -
        	(vertex.data().num_triangles + vertex.data().num_wedges_c + vertex.data().num_wedges_e + vertex.data().num_disc);
		 */
		
		//### TESI ### paragrafo "Equazione per il calcolo di n0,v"
		vertex.getValue().num_empty=new BigIntegerWritable(
				// (5-1)*(5-2)/2-
				// (1+2+2+7)
				// coefficiente binomiale
				new BigInteger(String.valueOf((vertexM.totalNumVertices-1)*(vertexM.totalNumVertices-2)/2)).subtract(
				// ### PAPER ###
				// dall'articolo 3-profiles, par. 4 e 4.1:
				// num_triagles sarebbe n3
				// num_disc sarebbe n1d
				// num_wedges_e + num_wedges_c sarebbe n2e + n2c che è uguale a n2 (vedi par. 4 per n2)
				(vertex.getValue().num_triangles.get().add(vertex.getValue().num_wedges_c.get()).add(
				 vertex.getValue().num_wedges_e.get()).add(vertex.getValue().num_disc.get()))));
		
		if (logger.isDebugEnabled()) logger.debug("vertex.num_empty="+vertex.getValue().num_empty+"   ( num_empty=(totalNumVertices-1)*(totalNumVertices-2)/2-(num_triangles+num_wedges_c+num_wedges_e+num_disc) )"+C.N);
	
	}

	
	
	
	/**
	 *  Calculate first 12 elements of u vector
	 *  (last 5 elements of u vector will be calculated by Worker_Superstep2_GAS4.calculateUVector() only if "-ca globalFromLocal=true" argument is set)
	 */
	private void calculateUVector(
			Vertex<IntWritable, VertexData, EdgeData> vertex, 
			VertexDataMem vertexM,
			EdgeCounts ecounts) {
		
		
		//4 PROFILE EQUATIONS 
		if (logger.isDebugEnabled()) {
			logger.debug("@@@@@ 4-PROFILES PAPER - pag. 485 - Figure 3 (b) @@@@@");
			logger.debug("Calculate first 12 elements of u vector");
			logger.debug("(if \"-ca globalFromLocal=true\" argument is set, then last 5 elements of u vector will be calculated by Worker_Superstep2_GAS4.calculateUVector())"+C.N);
		}
		
		vertex.getValue().u = new BigIntegerArrayListWritable();
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n1_double));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n2c_double));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n2e_double));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n3_double));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n1_n2c));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n1_n2e));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n1_n3));
	    vertex.getValue().u.add(new BigIntegerWritable(ecounts.n2c_n2e));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n2c_n3));
		vertex.getValue().u.add(new BigIntegerWritable(ecounts.n2e_n3));
		//### TESI ### paragrafo "Calcolo delle Equazioni Edge Pivot" 
		//memorizzo in u10 l'equazione 8
		BigIntegerWritable u10 = new BigIntegerWritable(
			  (vertex.getValue().num_disc.get().subtract(ecounts.n1)).multiply(new BigInteger(String.valueOf(vertexM.sourceNeighborhood.size()))));
		vertex.getValue().u.add(u10);
		
		/* ### GRAPHLAB ###
        std::vector<idcount>::const_iterator it = ecounts.b.idc_vec.begin();
        while (it!=ecounts.b.idc_vec.end()){
          // ### PAPER ### vedi (5) pag. 486
          vertex.data().u[11]+= (it->count * (it->count - 1))/2; // doing count choose 2 if the id is not the vertex neighbor.
          ++it;
        } */
		long u11 = 0;
		for (Integer count:ecounts.twohop.map_vertex_count.values()) {  //scorro la lista dei valori della mappa
			u11 += (count * (count - 1)) / 2;
	
		}
		// ### TESI ### paragrafo "Calcolo dell'istogramma 2hop"
 		//memorizzo l'equazione 16
		vertex.getValue().u.add(new BigIntegerWritable(u11)); // 12° e ultimo elemento aggiunto ad u
		
		
		if (logger.isDebugEnabled()) {
			logger.debug("vertex.u[0]="+ecounts.n1_double+ "   ( vertex.u[0]=ecounts_n1_double )"); // TODO verificare se questi debbano essere effettivamente double
			logger.debug("vertex.u[1]="+ecounts.n2c_double+ "   ( vertex.u[1]=ecounts_n2c_double )");
			logger.debug("vertex.u[2]="+ecounts.n2e_double+ "   ( vertex.u[2]=ecounts_n2e_double )");
			logger.debug("vertex.u[3]="+ecounts.n3_double+ "   ( vertex.u[3]=ecounts_n3_double )");
			logger.debug("vertex.u[4]="+ecounts.n1_n2c+ "   ( vertex.u[4]=ecounts_n1_n2c )");
			logger.debug("vertex.u[5]="+ecounts.n1_n2e+ "   ( vertex.u[5]=ecounts_n1_n2e )");
			logger.debug("vertex.u[6]="+ecounts.n1_n3+ "   ( vertex.u[6]=ecounts_n1_n3 )");
		    logger.debug("vertex.u[7]="+ecounts.n2c_n2e+ "   ( vertex.u[7]=ecounts_n2c_n2e )");
			logger.debug("vertex.u[8]="+ecounts.n2c_n3+ "   ( vertex.u[8]=ecounts_n2c_n3 )");
			logger.debug("vertex.u[9]="+ecounts.n2e_n3+ "   ( vertex.u[9]=ecounts_n2e_n3 )");
			logger.debug("vertex.u[10]="+u10+ "   ( vertex.u[10]=(num_disc-ecounts_n1)*sourceNeighboorhod.size() )");
			logger.debug("vertex.u[11]="+u11+"   ( u11=(ecounts_twohop[i].count * (ecounts_twohop[i].count - 1)) / 2    for i=1,...,ecounts_twohop.size )");
			logger.debug("( ecounts_twohop is the sum of all gather_twohop - see above logs )"+C.N);
		}
		
	}



	
	

	private GlobalCounts1 buildGlobalCount1(Vertex<IntWritable, VertexData, EdgeData> vertex) {
		
		
		GlobalCounts1 gc1 = new GlobalCounts1();
		
		
		gc1.num_disc = vertex.getValue().num_disc;
		gc1.num_empty = vertex.getValue().num_empty;
		gc1.num_wedges_c=vertex.getValue().num_wedges_c;
		gc1.num_wedges_e=vertex.getValue().num_wedges_e;
		gc1.num_triangles=vertex.getValue().num_triangles;
		gc1.average_num_triple=vertex.getValue().average_num_triple;
		gc1.global_num_triple=vertex.getValue().global_num_triple;
				
		//gc1.u= vertex.getValue().u;
		
		// STOP 3PROFILE
		// SALTARE QUESTO FOR SE IL NUOVO PARAMETRO E' =3
		for(int i=0;i<vertex.getValue().u.size();i++) {
			    gc1.u.set(i,vertex.getValue().u.get(i));
		}
		
		// gc1.eqn10const will be set in Worker_Superstep1 after execution of GAS 3
		return gc1;
	}
	


	
	private IntHashSetWritable getIntersection(IntHashSetWritable A, IntHashSetWritable B) {	
		
		if (A.size() < B.size()) {
			return _getIntersection(A, B);
		} else {
			return _getIntersection(B, A);
		}
		
	}
	
	
	private IntHashSetWritable _getIntersection(IntHashSetWritable A, IntHashSetWritable B) {
		IntHashSetWritable intersection = new IntHashSetWritable();
	    for(IntWritable elem : A) {
	        if(B.contains(elem)) {
	            intersection.add(elem);
	        }
	    }
	    return intersection;
	    
	}

}
