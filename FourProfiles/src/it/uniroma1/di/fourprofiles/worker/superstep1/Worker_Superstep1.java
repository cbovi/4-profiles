package it.uniroma1.di.fourprofiles.worker.superstep1;

import java.io.IOException;

import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;
//import org.apache.logging.log4j.LogManager;//LOG4J2
//import org.apache.logging.log4j.Logger;//LOG4J2
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.memory.vertex.VertexDataMem;
import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts1;
import it.uniroma1.di.fourprofiles.data.writable.msg.SuperstepMessage;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global3Profiles;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global4Profiles;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;
import it.uniroma1.di.fourprofiles.worker.superstep1.gas2.Worker_Superstep1_GAS2;
import it.uniroma1.di.fourprofiles.worker.superstep1.gas3.Worker_Superstep1_GAS3;
import it.uniroma1.di.fourprofiles.worker.superstep1.persaggr1.GlobalCounts1Aggregator;



public class Worker_Superstep1 
	extends BasicComputation<
		IntWritable, // vertex id data type
		VertexData,  // vertex data type
		EdgeData,    // edge data type
		MapWritable   // message data type 
	> {  

	private final static Logger logger = Logger.getLogger(Worker_Superstep1.class);
	//private static final Logger logger = LogManager.getLogger(Superstep1.class);//LOG4J2
	
	
	
	@Override
	public void compute(
			            Vertex<IntWritable, VertexData, EdgeData> vertex,
						Iterable<MapWritable> listOfMaps)
			throws IOException {
		
		int customInfoLogLevel = Integer.parseInt(getConf().get(C.CUSTOM_INFO_LOG_LEVEL_PARAMETER));
		C.CUSTOM_INFO_LOG_LEVEL = customInfoLogLevel; 
		
		StringBuilder numberOfMessages = new StringBuilder();
		MapWritable mapOfTargetNeighborhoods=Util.convertListOfMapsInASingleMap(listOfMaps, numberOfMessages);

		//#########################################################################		
		if (logger.isInfoEnabled() && customInfoLogLevel >= C.CUSTOM_INFO_LOG_LEVEL_1)
			logger.info("\n\n\nSUPERSTEP 1: vertex="+vertex.getId()+" - edges="+vertex.getNumEdges() + " - received msg="+numberOfMessages);
		//#########################################################################		
		
		if (logger.isDebugEnabled()) {
			    logger.debug("BEGIN SUPERSTEP "+this.getSuperstep()+" OF VERTEX "+vertex.getId()+C.SN2);
			    logger.debug("Number of received messages: "+Util.countMessages(listOfMaps)+C.SN);
			    
			    int i=0;
			    
				for(Writable key:mapOfTargetNeighborhoods.keySet()) {
						
						IntWritable sourceKey=(IntWritable)key;
						
						String msg = Util.logNeighborhood(
											sourceKey, 
											(IntHashSetWritable)mapOfTargetNeighborhoods.get(sourceKey));
						String n = (i==vertex.getNumEdges()-1)?C.N:C.E;
						i++;
						logger.debug("RECEIVED MSG \""+msg+"\" FROM VERTEX "+sourceKey+" TO VERTEX "+vertex.getId()+n);
				}
		}
		
// START PREPARING DATA THAT WILL BE USED BY GAS 2 AND GAS 3 //////////////////////////////////////		
		VertexDataMem vertexM = new VertexDataMem(
				vertex, 
				mapOfTargetNeighborhoods,
				this.getTotalNumVertices(),
				this.getTotalNumEdges(),
				this.getSuperstep());

// END PREPARING DATA THAT WILL BE USED BY GAS 2 AND GAS 3 ////////////////////////////////////////
    	
    	

    	
    	
// START GAS 2 ////////////////////////////////////////////////////////////////////////////////////
    	
		if (logger.isDebugEnabled()) logger.debug("BEGIN GAS 2 OF VERTEX "+vertex.getId());  
        
		if (logger.isDebugEnabled()) logger.debug("Execution of "+Worker_Superstep1_GAS2.class.getSimpleName()+" ...   (see log file 04)");
       
		boolean is3Global = Boolean.parseBoolean(
				getConf().get(C.THREE_GLOBAL_PARAMETER));
		
		//Questo set deve essere fatto prima della chiamamta del metodo aggregate
		if (is3Global==true) C.GLOBAL_3=1; else C.GLOBAL_3=0;
		
		Worker_Superstep1_GAS2 gas2 = new Worker_Superstep1_GAS2();
    	
        
		/*
		 * ### NOTES ### 
		 * 1)
		 * This method calculate:
		 * - u vector (first 12 elements, last 5 elements will be calculated by Worker_Superstep2_GAS4 only if "-ca globalFromLocal=true" parameter)
		 * - num_disc, num_empty, num_wedges_c, num_wedges_e, num_triangles scalars
		 * 
		 * All these values will be aggregated by global_counts1Aggregator 
		 * (together with the eq10const calculated by Worker_Superstep1_GAS3)
	 	 *
	 	 * 2) 
	 	 * I due cicli for sugli edges eseguiti in Worker_Superstep1_GAS2 e Worker_Superstep1_GAS3 non possono essere "fusi" in un unico
	 	 * ciclo for poiché nel primo for viene calcolato conn_neighbors che verrà poi utilizzato nel secondo ciclo for per calcolare
	 	 * eqn10_const_sum
	 	 */    
		GlobalCounts1 gc1 = gas2.execute_gas2(
				vertex, 
				vertexM,
				is3Global);

		if (logger.isDebugEnabled()) logger.debug("END GAS 2 OF VERTEX "+vertex.getId()+C.N3);
		
// END GAS 2 /////////////////////////////////////////////////////////////////////////////////////		
	
		

		
		boolean is4Global = Boolean.parseBoolean(
				getConf().get(C.FOUR_GLOBAL_PARAMETER));
		
		boolean is4GlobalFromLocal = Boolean.parseBoolean(
				getConf().get(C.FOUR_GLOBAL_FROM_LOCAL_PARAMETER));
		
			
		
		if (logger.isDebugEnabled()) {
			logger.debug("PARAMETERS:");
			logger.debug("   -ca 3global="+is3Global);
			logger.debug("   -ca 4global="+is4Global);
			logger.debug("   -ca 4globalFromLocal="+is4GlobalFromLocal);
			logger.debug("RULES:");
			logger.debug("   -ca 3global=true  =>  GAS 3 can be skipped");
			logger.debug("   -ca 4global=true  =>  GAS 3 must be executed");
			logger.debug("   -ca 4globalFromLocal=true  =>  GAS 3 can be skipped"+C.N);
		}
		
		// STOP 3PROFILE
		// MODIFICARE QUESTO IF SE IL NUOVO PARAMETRO E' =4 
		// SE E' UGUALE A 3 NON BISIGNA FARE IL GAS3
		if (is4Global == true) {
			
// START GAS 3 ////////////////////////////////////////////////////////////////////////////////////


			
			
			if (logger.isDebugEnabled()) logger.debug("BEGIN GAS 3 OF VERTEX "+vertex.getId());
			//if (logger.isInfoEnabled()) logger.info("PROCESSING SUPERSTEP "+this.getSuperstep()+" (GAS 3) OF VERTEX "+vertex.getId());
			
			if (logger.isDebugEnabled()) logger.debug("Execution of "+Worker_Superstep1_GAS3.class.getSimpleName()+" ...   (see log file 05)");
			Worker_Superstep1_GAS3 gas3 = new Worker_Superstep1_GAS3();
			
			/*
			 * This method calculates eq10_const_sum.  
			 * 
			 * eq10_const_sum will be aggregated by global_counts1Aggregator to calculate toInvert[9] in 
			 * Master_Superstep2_Global4Profiles
			 * (global_counts1Aggregator aggregates values calculated by Worker_Superstep1_GAS2 too)
		     */
			// ### TESI ### EQUAZIONE 14
			gc1.eqn10const = gas3.execute_gas3(
					vertex, 
					vertexM);
			
			
				
			if (logger.isDebugEnabled()) logger.debug("END GAS 3 OF VERTEX "+vertex.getId()+C.N3);

			
// END GAS 3 ////////////////////////////////////////////////////////////////////////////////////			
			
		}

		
// START AGGREGATION //////////////////////////////////////////////////////////////////////////////
		if (is4Global == true || is3Global == true
		
		//#BROADCAST
		|| (is4GlobalFromLocal==true)
				
				) { 	
			if (logger.isDebugEnabled()) {
				logger.debug("BEGIN AGGREGATION OF VALUES OF VERTEX "+vertex.getId()+C.N);
				
				logger.debug("Execution of "+GlobalCounts1Aggregator.class.getSimpleName()+".aggregate() for aggregation of:");
				logger.debug("- u vector (first 12 elements), num_disc, num_empty, num_wedges_c, num_wedges_e, num_triangles (calculated in "+
						Worker_Superstep1_GAS2.class.getSimpleName()+")");
				logger.debug("- eqn10const (calculated in "+Worker_Superstep1_GAS3.class.getSimpleName()+") ...   (see log file 06)"+C.N);
				logger.debug("(aggregated values will be used by "+Master_Superstep2_Global3Profiles.class.getSimpleName()+" and "+
						Master_Superstep2_Global4Profiles.class.getSimpleName()+" in next superstep)"+C.N);
			}
			
			// this invokes aggregate method of GlobalCounts1Aggregator class through giraph	
			this.aggregate(C.GLOBAL_COUNT1, gc1);
	        
			if (logger.isDebugEnabled()) logger.debug("END AGGREGATION OF VALUES OF VERTEX "+vertex.getId()+C.N4);
		}
// END AGGREGATION ////////////////////////////////////////////////////////////////////////////////
		

		
		
		
		
		
		
		if (is4GlobalFromLocal == true || is3Global == true) {
			
			if (logger.isDebugEnabled()) logger.debug("GAS 3 SKIPPED"+C.N3);
		
		}
		
		
		
		
		if (is4GlobalFromLocal == true) {
       
// START SENDING MESSAGE //////////////////////////////////////////////////////////////////////////
			
			sendMessage(vertex, vertexM);

// END SENDING MESSAGE ////////////////////////////////////////////////////////////////////////////
		}
		else
		{
			vertex.voteToHalt();
		}

		//#########################################################################		
		if (logger.isInfoEnabled() && customInfoLogLevel >= C.CUSTOM_INFO_LOG_LEVEL_2)
			logMem();
		//#########################################################################		
		
		if (logger.isDebugEnabled()) logger.debug("END SUPERSTEP "+this.getSuperstep()+" OF VERTEX "+vertex.getId()+C.SN4);

	}



	private void sendMessage(Vertex<IntWritable, VertexData, EdgeData> vertex, VertexDataMem vertexM) {
		/*
		### GRAPHLAB ###
		
		edge.data().eqn10_const=0;
		
		foreach(graphlab::vertex_id_type v,interlist) {
			our_cset->insert(v);
		}
		
		for(size_t k=0;k<srclist.conn_neighbors.size();k++) {
			size_t flag1=0;
			flag1 = our_cset->count(srclist.conn_neighbors.at(k).first) + our_cset->count(srclist.conn_neighbors.at(k).second);
			if(flag1==2)
			edge.data().eqn10_const++;
		}
		*/	
		SuperstepMessage message = new SuperstepMessage(/*vertex.getId(),*/
														vertex.getValue().num_triangles, 
														vertex.getValue().num_wedges_e,
														vertexM.conn_neighbors
														);
       
 	    if (logger.isDebugEnabled()) {
	 	    logger.debug("SEND MSG:");
	 	    logger.debug("   num_triangles="+message.num_triangles);
	 	    logger.debug("   num_wedges_e="+message.num_wedges_e);
	 	    logger.debug("   conn_neighbors="+message.conn_neighbors.toString());
	 	    logger.debug("FROM VERTEX "+vertex.getId()+ " TO VERTEXES "+vertexM.sourceNeighborhoodStr+C.N);
 	    }
 	    
 	    MapWritable messageMap = new MapWritable();
 	    
 	    messageMap.put(vertex.getId(), message);
	    
		sendMessageToAllEdges(vertex, messageMap);
	}

	
	//############################LOG TEMPORANEO#############################################
	private void logMem() {
		logger.info("MEMORY:");
		logger.info("total memory (-Xmx) = "+Runtime.getRuntime().totalMemory()/1024);
		logger.info("max memory = "+Runtime.getRuntime().maxMemory()/1024);
		logger.info("free memory = "+Runtime.getRuntime().freeMemory()/1024 + "   (Current allocated free memory, is the current allocated space ready for new objects.)");
		long usedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		logger.info("used memory = "+usedMemory/1024);
		long totalFreeMemory = Runtime.getRuntime().maxMemory() - usedMemory;
		logger.info("total free memory = "+totalFreeMemory/1024);
	}
	//############################LOG TEMPORANEO#############################################
	
}



