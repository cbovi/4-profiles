package it.uniroma1.di.fourprofiles.worker.superstep1.gas3;

import java.math.BigInteger;
import java.util.Iterator;

import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
//import org.apache.logging.log4j.LogManager;//LOG4J2
//import org.apache.logging.log4j.Logger;//LOG4J2
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDs;
import it.uniroma1.di.fourprofiles.data.memory.vertex.VertexDataMem;
import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global4Profiles;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;
import it.uniroma1.di.fourprofiles.worker.superstep1.gas2.Worker_Superstep1_GAS2;
import it.uniroma1.di.fourprofiles.worker.superstep1.persaggr1.GlobalCounts1Aggregator;

public class Worker_Superstep1_GAS3 {

	private final static Logger logger = Logger.getLogger(Worker_Superstep1_GAS3.class);
	//private static final Logger logger = LogManager.getLogger(Superstep1_GAS3.class);//LOG4J2
	
	/**
	 * This method calculates eq10_const_sum.  
	 * 
	 * eq10_const_sum will be aggregated by global_counts1Aggregator to calculate toInvert[9] in 
	 * Master_Superstep2_Global4Profiles
	 * (global_counts1Aggregator aggregates values calculated by Worker_Superstep1_GAS2 too)
     */
	public BigIntegerWritable execute_gas3(
			Vertex<IntWritable, VertexData, EdgeData> sourceVertex,
			VertexDataMem sourceVertexM) {
		
		
		if (logger.isDebugEnabled()) logBeginGAS3(sourceVertex, sourceVertexM);
		
        
		BigInteger eqn10_const_sum = BigInteger.ZERO;

    	/* ### NOTES ###
        I due cicli for sugli edges eseguiti in Worker_Superstep1_GAS2 e Worker_Superstep1_GAS3 non possono essere "fusi" in un unico
        ciclo for poiché nel primo for viene calcolato conn_neighbors che verrà poi utilizzato nel secondo ciclo for */
	
	    for(Writable key:sourceVertexM.mapOfTargetNeighborhoods.keySet()){
	    	
	    	IntWritable targetVertexId = (IntWritable)key;
	    	if (logger.isDebugEnabled()) logger.debug("START PROCESSING EDGE "+sourceVertex.getId()+" -> "+targetVertexId + C.N);
	    	
			BigInteger eqn10_const = BigInteger.ZERO; 
	     
		    IntHashSetWritable targetNeighborhood = (IntHashSetWritable)sourceVertexM.mapOfTargetNeighborhoods.get(key);
			IntHashSetWritable intersection = sourceVertexM.mapOfIntersections.get(targetVertexId);

		     
		    if (logger.isDebugEnabled()) {
			     logger.debug("##### STARTING CALCULUS OF eq10const FOR EDGE "+sourceVertex.getId()+" -> "+targetVertexId +" #####");
			     logger.debug("EDGE "+sourceVertex.getId()+" -> "+targetVertexId);
			     logger.debug("source = "+sourceVertex.getId());
			     logger.debug("target = "+targetVertexId);
			     logger.debug("source neighborhood = "+Util.logNeighborhood(sourceVertex.getId(), sourceVertexM.sourceNeighborhood));
			     logger.debug("target neighborhood = "+Util.logNeighborhood(targetVertexId, targetNeighborhood));
			     logger.debug("intersection between source and target neighborhood = "+intersection);
			     
	 
			        
			     
			     
			     // conn_neighbors (calcolato in Worker_Superstep1_GAS2.java)
			     // insieme di twoids costruiti scorrendo l'intersezione tra il vicinato del sorgente e il vicinato del destinatario,
			     // nello specifico in ogni twoids il primo è sempre il sorgente e il secondo è sempre uno dei vertici dell'intersezione avente
			     // id maggiore del sorgente
				 logger.debug("source.conn_neighbors="+sourceVertexM.conn_neighbors.toString());
		    }
		     
			if (!sourceVertexM.conn_neighbors.isEmpty()) {
				 eqn10_const = calculate_eqn10_const(sourceVertexM, eqn10_const, intersection);
			}

			 
		    // il valore ottenuto di eqn10_const per l'arco corrente viene sommato al valore di eqn10_const 
		    // ottenuto per tutti gli archi incidenti al vertice
			eqn10_const_sum=eqn10_const_sum.add(eqn10_const);      
		    
			if (logger.isDebugEnabled()) {
			    logger.debug(C.N);
			    logger.debug("eqn10_const="+eqn10_const +" for EDGE "+sourceVertex.getId()+" -> "+targetVertexId+C.N);
			    
			    logger.debug("updated eq10_const_sum for ALL EDGES OF VERTEX "+sourceVertex.getId()+" = "+eqn10_const_sum);
			    
			    logger.debug("##### FINISH CALCULUS OF eq10const FOR EDGE "+sourceVertex.getId()+" -> "+targetVertexId +" #####"+C.N);
	
			    
			    logger.debug("END PROCESSING EDGE "+sourceVertex.getId()+" -> "+targetVertexId + C.N4);
			}
		}
	    
	    if (logger.isDebugEnabled()) { 
		    logger.debug("final eq10_const_sum for ALL EDGES OF VERTEX "+sourceVertex.getId()+" = "+eqn10_const_sum);
		    logger.debug("(eq10_const_sum will be aggregated by "+GlobalCounts1Aggregator.class.getSimpleName()+" to calculate toInvert[9] in "+Master_Superstep2_Global4Profiles.class.getSimpleName()+C.N);
		    
		    logger.debug("END GAS 3 OF VERTEX "+sourceVertex.getId() + " (SUPERSTEP "+sourceVertexM.superstep+")"+C.SN4);
	    }
	    
	    sourceVertex.voteToHalt();
	    
	    return new BigIntegerWritable(eqn10_const_sum);
	}

	private BigInteger calculate_eqn10_const(VertexDataMem sourceVertexM, BigInteger eqn10_const,
			IntHashSetWritable intersection) {
		if (logger.isDebugEnabled()) logger.debug("iterating on source_conn_neighbors ..."); 
		 

		 Iterator<TwoIDs> iter = sourceVertexM.conn_neighbors.iterator();
		 long k=0;
		 while (iter.hasNext()) {
			 TwoIDs twoids_k = iter.next();
		 
		 /*for(int k=0; k<vertex.getValue().conn_neighbors.size();k++){
			  twoids k_twoids = vertex.getValue().conn_neighbors.get(k);*/
		      long flag1=0;
		      if (logger.isDebugEnabled()) {
		          logger.debug("");
		          logger.debug("   index "+k+" of source.conn_neighbors:");
		          logger.debug("   source.conn_neighbors["+k+"] = ("+twoids_k.first.get()+","+twoids_k.second.get()+")");
		          logger.debug("   source.conn_neighbors["+k+"].first = "+twoids_k.first.get());
		          logger.debug("   source.conn_neighbors["+k+"].second = "+twoids_k.second.get());
		      }
		      
		      
		      
		      

		      long countOfFirst = intersection.contains(twoids_k.first/*.get()*/)?1:0;
		      /*
		       * ### NOTES ###
		       * It is not necessary to invoke .get() on twoids_k.first since contains internally uses equals method of 
		       * org.apache.hadoop.io.IntWritable class so defined:
		        
		       
			  	  // Returns true iff <code>o</code> is a IntWritable with the same value.
				  @Override
				  public boolean equals(Object o) {
				    if (!(o instanceof IntWritable))
				      return false;
				    IntWritable other = (IntWritable)o;
				    return this.value == other.value;
				  }
		       */
		      
		      long countOfSecond = intersection.contains(twoids_k.second/*.get()*/)?1:0;
		      
		      flag1 = countOfFirst + countOfSecond;
		      
		      if (logger.isDebugEnabled()) {
		          logger.debug("   count of first \""+twoids_k.first.get()+"\" in intersection "+intersection+" = "+countOfFirst);
		          logger.debug("   count of second \""+twoids_k.second.get()+"\" in intersection "+intersection+" = "+countOfSecond);
		          logger.debug("   flag1 = "+flag1+"   (flag1 = count of first "+countOfFirst+" + count of second "+countOfSecond+")");
		      }
		      
		      if (flag1==2) {
		    	  eqn10_const=eqn10_const.add(BigInteger.valueOf(1));
		    	  if (logger.isDebugEnabled()) logger.debug("   since flag1 == 2 increments eqn10_const to = "+eqn10_const);
		      }
		      else {
		    	  if (logger.isDebugEnabled()) logger.debug("   since flag1 != 2 no increments of eqn10_conts that remains equals to "+eqn10_const);
		      }
		      
		      k++;
		}
		return eqn10_const;
	}

	private void logBeginGAS3(Vertex<IntWritable, VertexData, EdgeData> sourceVertex, VertexDataMem sourceVertexM) {
		logger.debug("BEGIN GAS 3 OF VERTEX "+sourceVertex.getId() + " (SUPERSTEP "+sourceVertexM.superstep+")"+C.SN);
		
		logger.debug("IN GAS 3 WILL BE CALCULATED:"+C.N);
		
		logger.debug("- eq10const from intersections and conn_neighbors calculated in GAS 2"+C.N);
		logger.debug("  eq10const will be used to calculate toInvert[9] in "+Master_Superstep2_Global4Profiles.class.getSimpleName());
		logger.debug("  (toInvert[9] will be used to calculate global 4-profiles together with some of first 12 elements of u vector;");
		logger.debug("  u vector has already been calculated in "+Worker_Superstep1_GAS2.class.getSimpleName()+")"+C.N4);
				
		
		
		
		int h=1;
		logger.debug("EDGES OF VERTEX "+sourceVertex.getId()+" TO BE PROCESSED:");
		for (IntWritable targetVertexId:sourceVertexM.sourceNeighborhood) {
			logger.debug("  "+sourceVertex.getId()+" -> "+targetVertexId+((h==sourceVertexM.sourceNeighborhood.size())?C.N2:C.E));
			h++;
		}
	}

}
