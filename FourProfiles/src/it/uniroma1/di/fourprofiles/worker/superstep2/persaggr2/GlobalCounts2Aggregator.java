package it.uniroma1.di.fourprofiles.worker.superstep2.persaggr2;

import org.apache.giraph.aggregators.BasicAggregator;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts2;
import it.uniroma1.di.fourprofiles.util.C;

public class GlobalCounts2Aggregator extends BasicAggregator<GlobalCounts2>  {

	private final static Logger logger = Logger.getLogger(GlobalCounts2Aggregator.class);
	//private static final Logger logger = LogManager.getLogger(global_counts2Aggregator.class);	
	


	/**
	 * 
	 * Il framework GIRAPH invoca il metodo createInitialValue sia dal master sia dai workers TODO verificare dai log
	 */
	@Override
	public GlobalCounts2 createInitialValue() {
	
		GlobalCounts2 gc2 = new GlobalCounts2();
		
		if (logger.isDebugEnabled()) {
			logger.debug("BEGIN INITIAL AGGREGATED VALUES OF GLOBAL COUNT 2 (gc2):");
			logger.debug("gc2.n4local="+gc2.n4local);
			logger.debug("END INITIAL AGGREGATED VALUES OF GLOBAL COUNT 2 (gc2)"+C.N4);
		}
		
		return gc2;
	}


	/**
	 * Il metodo aggregate viene invocato, attraverso il framework GIRAPH, dal Worker nel metodo TODO
	 */
	@Override
	public void aggregate(GlobalCounts2 input) {
		
		
		//#######################################################################################
	    //contiene il valore aggregato fino a questo momento
		GlobalCounts2 partial_gc2 = this.getAggregatedValue();
		//#######################################################################################
		
		if (logger.isDebugEnabled()) {
			logger.debug("PREVIOUS AGGREGATED VALUE OF GLOBAL COUNT 2: gc2.n4local="+partial_gc2.n4local);
			logger.debug("INPUT OF aggregate() METHOD: gc2.n4local="+input.n4local);
		}
		//Aggregazione su tutti i vertici
		for(int i=0; i<C.USIZE_17; i++) {
			         
			partial_gc2.n4local.set(i, new BigIntegerWritable(
					partial_gc2.n4local.get(i).get().add(
							input.n4local.get(i).get())));
			                    
		}
		
		if (logger.isDebugEnabled()) logger.debug("UPDATED AGGREGATED VALUE OF GLOBAL COUNT 2: gc2.n4local="+partial_gc2.n4local+C.N4);
		
		/* Not useful
		//#########################################################################		
		if (logger.isInfoEnabled() && U.CUSTOM_INFO_LOG_LEVEL >= U.CUSTOM_INFO_LOG_LEVEL_1)
			logger.info("aggregate gc2.n4local="+partial_gc2.n4local);
		//#########################################################################		
		*/
		
		
		///////////////////////////////////////////////////////////////////////////////////
		//aggiorna il valore aggrgato fino a questo momento	
		this.setAggregatedValue(partial_gc2);
		///////////////////////////////////////////////////////////////////////////////////
	}
	
	
	

}