package it.uniroma1.di.fourprofiles.worker.superstep2.gas4;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Iterator;

import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDs;
import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDsHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts2;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.data.writable.msg.SuperstepMessage;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.master.superstep3.Master_Superstep3_Global4ProfilesFromLocal;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;
import it.uniroma1.di.fourprofiles.worker.superstep2.persaggr2.GlobalCounts2Aggregator;

public class Worker_Superstep2_GAS4 extends BasicComputation
    <IntWritable, VertexData, EdgeData, MapWritable>{ 

	private final static Logger logger = Logger.getLogger(Worker_Superstep2_GAS4.class);
	//private static final Logger logger = LogManager.getLogger(Superstep2_GAS4.class);
	
	
	
	class ECounts {
		BigInteger h8v=BigInteger.ZERO;
		BigInteger h10v=BigInteger.ZERO;
		BigInteger n3g= BigInteger.ZERO;
		BigInteger n2eg=BigInteger.ZERO;
		
	}
	
	
	@Override
	public void compute(
			Vertex<IntWritable, VertexData, EdgeData> sourceVertex, 
			Iterable<MapWritable> listOfMaps)
			throws IOException {
		
		int customInfoLogLevel = Integer.parseInt(getConf().get(C.CUSTOM_INFO_LOG_LEVEL_PARAMETER));

		StringBuilder numberOfMessages = new StringBuilder();
		MapWritable mapOfMessages = Util.convertListOfMapsInASingleMap(listOfMaps,numberOfMessages);
		
		//#########################################################################
		if (logger.isInfoEnabled() && customInfoLogLevel >= C.CUSTOM_INFO_LOG_LEVEL_1)
			logger.info("\n\n\nSUPERSTEP 2: vertex="+sourceVertex.getId()+" - edges="+sourceVertex.getNumEdges() + " - received msg="+numberOfMessages);
		//#########################################################################
				
		if (logger.isDebugEnabled()) logBeginGAS4(sourceVertex, mapOfMessages, listOfMaps);    
		//if (logger.isInfoEnabled()) logger.info("PROCESSING SUPERSTEP "+this.getSuperstep()+" (GAS 4) OF VERTEX "+sourceVertex.getId());
		
		
		ECounts ecounts = new ECounts();
		
		
		IntHashSetWritable sourceNeighboorhod = new IntHashSetWritable();
		Util.getNeighborhood(sourceVertex, sourceNeighboorhod);
		
		
		
		if (logger.isDebugEnabled()) {
	    	logger.debug("EDGES OF VERTEX "+sourceVertex.getId()+" TO BE PROCESSED:");
	    	for (Edge<IntWritable,EdgeData> edge:sourceVertex.getEdges()) {
	        	logger.debug("  "+sourceVertex.getId()+" -> "+edge.getTargetVertexId());
	        }
	    	
			
	    	logger.debug(C.N2);
		}
        
		for (Writable key:mapOfMessages.keySet())	{	
		   
        	calculate_ecounts_h8v_h10v(sourceVertex, mapOfMessages, ecounts, sourceNeighboorhod, key);
	         
		}
		
		
		if (logger.isDebugEnabled()) logger.debug("ALL EDGES OF VERTEX "+sourceVertex.getId()+" HAS BEEN PROCESSED"+C.N4);
			
			
			
			
			
		if (logger.isDebugEnabled()) logger.debug("START CALCULUS OF u AND n4local VECTORS:"+C.N);
		
		/*
		 *  Calculate last 5 elements of u vector.
		 *  First 12 elements of u vector has been calculated by Worker_Superstep2_GAS4.calculateUVector().
	 	 *	
	 	 *  u vector will be used to calculate n4local vector.
	 	 *  
	 	 *  NOTE:
	 	 *  u vector is memorized in sourceVertex parameter
	 	 */
		
		//System.out.println("Qui chiamo GET BROADCAST");
		
		BigIntegerArrayListWritable n3Global=this.getBroadcast("n3Global");
		//System.out.println("Valore di n3Global "+n3Global.get(0).get());
		
		calculateUVector(sourceVertex, ecounts.n3g, ecounts.n2eg, ecounts.h8v, ecounts.h10v);
		
		/*
		 * Calculate n4Local vector
		 * 
		 * NOTE:
		 * n4Local vector is memorized in sourceVertex parameter
		 */
		calculateN4LocalVector(sourceVertex, n3Global);
		
		  
		if (logger.isDebugEnabled()) logger.debug("END CALCULUS OF u AND n4local VECTORS"+C.N2);

		
		
		if (logger.isDebugEnabled()) logger.debug("END GAS 4 OF VERTEX "+sourceVertex.getId()+C.N2);


		
		
		
		if (logger.isDebugEnabled()) logger.debug("BEGIN AGGREGATION OF VERTEX "+sourceVertex.getId()+C.N);
		
		aggregateN4LocalVector(sourceVertex);

		if (logger.isDebugEnabled()) logger.debug("END AGGREGATION OF VERTEX "+sourceVertex.getId()+C.N2);
        
		
        
        
		if (logger.isDebugEnabled()) logger.debug("VERTEX "+sourceVertex.getId()+" VOTES HALT"+C.N2);
		
		
        sourceVertex.voteToHalt();
		  
		  
		
        if (logger.isDebugEnabled()) logger.debug("END SUPERSTEP "+this.getSuperstep()+" OF VERTEX "+sourceVertex.getId()+C.SN4);
		
		//#########################################################################        
		if (logger.isInfoEnabled() && customInfoLogLevel >= C.CUSTOM_INFO_LOG_LEVEL_2)
			logMem();
		//#########################################################################		
	}



	private void calculate_ecounts_h8v_h10v(Vertex<IntWritable, VertexData, EdgeData> sourceVertex,
			MapWritable mapOfMessages, ECounts ecounts, IntHashSetWritable sourceNeighboorhod, Writable key) {
		IntWritable targetVertexId = (IntWritable)key;
		EdgeData edgeData = sourceVertex.getEdgeValue(targetVertexId);
		
		SuperstepMessage message = (SuperstepMessage)mapOfMessages.get(key);
		
		if (logger.isDebugEnabled()) {
			logger.debug("START PROCESSING EDGE "+sourceVertex.getId()+" -> "+targetVertexId + C.N);
			
			logger.debug("@@@@@ 4-PROFILES PAPER - top left of pag. 486 - LOCAL 3-PROFILES @@@@@");
			logger.debug("edge.n3 = "+edgeData.n3.get()+"   ( calculated by Worker_Superstep1_GAS2.calculateEdgeCounts() )");
			logger.debug("edge.n2c = "+edgeData.n2c.get()+"   ( calculated by Worker_Superstep1_GAS2.calculateEdgeCounts() )"+C.N);
			
			
			
			
			logger.debug("MSG FROM TARGET "+targetVertexId+".num_triangles = "+message.num_triangles.get()+
					"   ( calculated by Worker_Superstep1_GAS2.calculateScalars() )");
			logger.debug("MSG FROM TARGET "+targetVertexId+".num_wedges_e = "+message.num_wedges_e.get()+
					"   ( calculated by Worker_Superstep1_GAS2.calculateScalars() )"+C.N4);
			
			
			/* ### GRAPHLAB ###
			gather.n3g = edge.target().data().num_triangles - edge.data().n3;
		    gather.n2eg = edge.target().data().num_wedges_e - edge.data().n2c;
			*/
			
			
			logger.debug("##### START CALCULUS OF ecounts_n3g and ecounts_n2eg: #####"+C.N);
			
			logger.debug("gather variables are related to current EDGE "+sourceVertex.getId()+" -> "+targetVertexId);
			logger.debug("ecounts variables are related to ALL EDGES of VERTEX "+sourceVertex.getId()+C.N);
		}
		
		//## TESI ### paragrafo "Calcolo delle Equazioni Edge Pivot" 
		//Equazione 12
		BigInteger gather_n3g = message.num_triangles.get().subtract(edgeData.n3.get());
		ecounts.n3g=ecounts.n3g.add(gather_n3g);
		
		//## TESI ### paragrafo "Calcolo delle Equazioni Edge Pivot" 
		//Equazione 13
		BigInteger gather_n2eg = message.num_wedges_e.get().subtract(edgeData.n2c.get()); 
		ecounts.n2eg=ecounts.n2eg.add(gather_n2eg);
		
		if (logger.isDebugEnabled()) {
			logger.debug("gather_n3g = "+gather_n3g + "   ( gather_n3g = MSG FROM VERTEX "+targetVertexId+".num_triangles - edge.n3 )");
			logger.debug("ecounts_n3g = "+ecounts.n3g + "   ( ecounts_n3g+=gather_n3g )"+C.N);

			logger.debug("gather_n2eg = "+gather_n2eg + "   ( gather_n2eg = MSG FROM VERTEX "+targetVertexId+".num_wedges_e - edge.n2c )");
			logger.debug("ecounts_n2eg = "+ecounts.n2eg + "   ( ecounts_n2eg+=gather_n2eg )"+C.N);	        
		    
			logger.debug("##### END CALCULUS OF ecounts_n3g and ecounts_n2eg: #####"+C.N3);
		}
		 /* ### GRAPHLAB ###
		  
		  for(size_t k=0;k<edge.target().data().conn_neighbors.size();k++) {
		      size_t flag1=0;
		      flag1 = our_cset->count(edge.target().data().conn_neighbors.at(k).first) +
		          our_cset->count(edge.target().data().conn_neighbors.at(k).second);
		      if(flag1==0)
		        gather.h8v += 1;
		      //simply add case for H10 here
		      else if(flag1 == 2)
		        gather.h10v += 1;
		  }
		  */

		 BigInteger gather_h8v=BigInteger.ZERO;
		 BigInteger gather_h10v=BigInteger.ZERO;
		 TwoIDsHashSetWritable msg_from_target_conn_neighbors = message.conn_neighbors; 
		 
		 
		 if (logger.isDebugEnabled()) {
		     logger.debug("##### START CALCULUS OF gather_h8v and gather_h10v: #####");
		     logger.debug("@@@@@ 4-PROFILES PAPER - pag. 486 paragraph 2.2 (4) @@@@@");
		     
		     logger.debug("gather_h8v="+gather_h8v);
		     logger.debug("gather_h10v="+gather_h10v);
		     
		         
		     logger.debug("MSG FROM TARGET "+targetVertexId+".conn_neighbors = "+msg_from_target_conn_neighbors+
		    		 "   ( calculated by Worker_Superstep1_GAS2.calculateConn_neighbors() )");
		     logger.debug("iterating on MSG FROM TARGET "+targetVertexId+".conn_neighbors ...");
		 }
		 
		 Iterator<TwoIDs> iter = msg_from_target_conn_neighbors.iterator();
		 long k = 0;
		 while (iter.hasNext()) {
			  TwoIDs twoids_k = iter.next();
			  
			  if (logger.isDebugEnabled()) {
		    	  logger.debug("");
		    	  logger.debug("   index "+k+":");
		          logger.debug("   MSG FROM TARGET "+targetVertexId+".conn_neighbors["+k+"] = ("+twoids_k.first.get()+","+twoids_k.second.get()+")");
		          logger.debug("   MSG FROM TARGET "+targetVertexId+".conn_neighbors["+k+"].first = "+twoids_k.first.get());
		          logger.debug("   MSG FROM TARGET "+targetVertexId+".conn_neighbors["+k+"].second = "+twoids_k.second.get());
			  }
			  
		      long flag1=0;
		      
		      
		      
		      
		      long countOfFirst = sourceNeighboorhod.contains(twoids_k.first/*.get()*/)?1:0;
		      /*
		       * ### NOTES ###
		       * It is not necessary to invoke .get() on twoids_k.first since contains internally uses equals method of 
		       * org.apache.hadoop.io.IntWritable class so defined:
		        
		       
			  	  // Returns true iff <code>o</code> is a IntWritable with the same value.
				  @Override
				  public boolean equals(Object o) {
				    if (!(o instanceof IntWritable))
				      return false;
				    IntWritable other = (IntWritable)o;
				    return this.value == other.value;
				  }
		       */

		      long countOfSecond = sourceNeighboorhod.contains(twoids_k.second/*.get()*/)?1:0;
		      
		      
		      flag1 = countOfFirst + countOfSecond;
		      
		      if (logger.isDebugEnabled()) {
		          logger.debug("   count of first \""+twoids_k.first.get()+"\" in sourceNeighboorhod "+sourceNeighboorhod+" = "+countOfFirst);
		          logger.debug("   count of second \""+twoids_k.second.get()+"\" in sourceNeighboorhod "+sourceNeighboorhod+" = "+countOfSecond);
		          logger.debug("   flag1 = "+flag1+"   (flag1 = count of first "+countOfFirst+" + count of second "+countOfSecond+")");
		      }
		      
		      if (flag1 == 0) {
		    	  gather_h8v=gather_h8v.add(BigInteger.ONE);
		    	  if (logger.isDebugEnabled()) logger.debug("   since flag1 == 0 increments gather_h8v to = "+gather_h8v);
		      }
		      else if (flag1 == 2) {
		    	  gather_h10v=gather_h10v.add(BigInteger.ONE);
		    	  if (logger.isDebugEnabled()) logger.debug("   since flag1 == 2 increments gather_h10v to = "+gather_h10v);
		      }
		      else {
		    	  if (logger.isDebugEnabled()) logger.debug("   since flag1 != 0 and flag1 != 2 no increments of gather_h8v and gather_h10v that respectively remain equals to "+gather_h8v+" and "+gather_h10v);
		      }
		      k++;
		 }
		 // ### TESI ### equazione 14)
		 ecounts.h8v=ecounts.h8v.add(gather_h8v);
		// ### TESI ### equazione 15)
		 ecounts.h10v=ecounts.h10v.add(gather_h10v);
		 
		 if (logger.isDebugEnabled()) {
		     logger.debug("##### END CALCULUS OF gather_h8v and gather_h10v #####"+C.N);
		     
		     
		     logger.debug("END PROCESSING EDGE "+sourceVertex.getId()+" -> "+targetVertexId + C.N2);
		 }
	}



	private void logBeginGAS4(
					Vertex<IntWritable, VertexData, EdgeData> sourceVertex,
					MapWritable mapOfMessages,
					Iterable<MapWritable> messages) {
		logger.debug("BEGIN SUPERSTEP "+this.getSuperstep()+" OF VERTEX "+sourceVertex.getId()+C.SN);
		logger.debug("BEGIN GAS 4 OF VERTEX "+sourceVertex.getId()+C.N);
		
		logger.debug("IN GAS 4 WILL BE CALCULATED LOCAL 4 PROFILES OF VERTEX "+sourceVertex.getId()+" USING FROM EVERY EDGE SOURCE -> TARGET FOLLOWING VALUES:");
		logger.debug("- edge.n3   ( calculated by Worker_Superstep1_GAS2.calculateEdgeCounts() )");
		logger.debug("- edge.n2c   ( calculated by Worker_Superstep1_GAS2.calculateEdgeCounts() )");
		logger.debug("- MSG FROM TARGET.num_triangles   ( calculated by Worker_Superstep1_GAS2.calculateScalars() )");
		logger.debug("- MSG FROM TARGET.num_wedges_e   ( calculated by Worker_Superstep1_GAS2.calculateScalars() )");
		logger.debug("- MSG FROM TARGET.conn_neighbors   ( calculated by Worker_Superstep1_GAS2.calculateConn_neighbors() )"+C.N3);

		logger.debug("Number of received messages:"+Util.countMessages(messages)+C.N);
		
		for(Writable key:mapOfMessages.keySet()) {
			SuperstepMessage message=(SuperstepMessage)mapOfMessages.get(key);
			logger.debug("RECEIVED MSG:");
			logger.debug("   msg.num_triangles = "+message.num_triangles);
		    logger.debug("   msg.num_wedges_e = "+message.num_wedges_e);
		    logger.debug("   msg.conn_neighbors = "+message.conn_neighbors.toString());
		    logger.debug("FROM VERTEX "+(IntWritable)key+ " TO VERTEX "+sourceVertex.getId()+C.N);
		
		}
	}



	/**
	 *  Calculate last 5 elements of u vector
	 *  (first 12 elements of u vector has been calculated by Worker_Superstep2_GAS4.calculateUVector())
	 */
	private void calculateUVector(Vertex<IntWritable, VertexData, EdgeData> vertex, BigInteger ecounts_n3g,
			BigInteger ecounts_n2eg, BigInteger ecounts_h8v, BigInteger ecounts_h10v) {
		/**
		### GRAPHLAB ### 
		vertex.data().u[12] = ecounts.h10v/3.; // each clique at a vertex is counted three times once each for every incident edge
		vertex.data().u[12] = ecounts.h10v*2; // each clique at a vertex is counted three times once each for every incident edge, until fix matrix just scale this to 6x
	    vertex.data().u[13] = ecounts.n3g;
	    vertex.data().u[14] = ecounts.n2eg;
	    //solve for H_8 directly, 1 more equation
	    vertex.data().u[15] = ecounts.h8v;
	    */
		
		
		//logger.debug("CHECK size of vertex.u vector = "+vertex.getValue().u.size() + "   (size at this point must be 12)"+U.N2);
		
		if (logger.isDebugEnabled()) {
			logger.debug("@@@@@ 4-PROFILES PAPER - pag. 485 - Figure 3 (b) @@@@@");
			logger.debug("Calculate last 5 elements of u vector");
			logger.debug("(first 12 elements of u vector has been calculated by Worker_Superstep1_GAS2.calculateUVector())");
		}
		
		vertex.getValue().u.add(ecounts_h10v.multiply(C.TWO));
		vertex.getValue().u.add(ecounts_n3g);
		vertex.getValue().u.add(ecounts_n2eg);
		vertex.getValue().u.add(ecounts_h8v);
		
		
		
		
		
				
		/*
		### GRAPHLAB ###
		//then invert matrix and store local 4-profile
		void solve_local_4profile(graph_type::vertex_type& v) {

		  long nv = (long)total_vertices;
		  v.data().u[16] = (nv-1)*(nv-2)*(nv-3)/6.; //|V|-1 choose 3

		    
		    A*12 =
		    -12     -4    -12     -4     -6    -12     -6     -6     -4     -6      0      0      0      0      0      0     12
		     12      0      0    -24      0      0      0      0      0    -12    -12    -24      0     24     12    -12      0
		      0      0      0     24      0      0      0      0      0     12     12     24      0    -24    -12     12      0
		      0      0      0     24      0     12      0      0      0     12      0     24      0    -24    -12     24      0
		      0      0      0      0      6      0      0     -6      0      0      0     12      3     -6      0      6      0
		      0      0      0    -24      0      0      0      0      0    -12      0    -24      0     24     12    -24      0
		      0      0      0      0      0      0      0     12      0      0      0    -24     -6     12      0    -12      0
		      0      0      0      0      0      0      6      0      0     -6      0      0     -3      6      0     -6      0
		      0      0     12      0      0      0      0      0      0      0      0      0      0      0      0    -12      0
		      0      4      0      4      0      0      0      0     -2      0      0      0     -2      0      0      0      0
		      0      0      0      0      0      0      0      0      0      0      0     12      3     -6      0      6      0
		      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0     12      0
		      0      0      0    -12      0      0      0      0      6      0      0      0      6      0      0      0      0
		      0      0      0      0      0      0      0      0      0     12      0      0      6    -12      0     12      0
		      0      0      0      0      0      0      0      0      0      0      0      0     -3      6      0     -6      0
		      0      0      0     12      0      0      0      0      0      0      0      0     -6      0      0      0      0
		      0      0      0      0      0      0      0      0      0      0      0      0      2      0      0      0      0
		    

		  const uvec A0  = {{-6,-2,-6,-2,-3,-6,-3,-3,-2,-3,0,0,0,0,0,0,6}};
		  const uvec A1  = {{1,0,0,-2,0,0,0,0,0,-1,-1,-2,0,2,1,-1,0}};
		  const uvec A2  = {{0,0,0,2,0,0,0,0,0,1,1,2,0,-2,-1,1,0}};
		  const uvec A3  = {{0,0,0,2,0,1,0,0,0,1,0,2,0,-2,-1,2,0}};
		  const uvec A4  = {{0,0,0,0,2,0,0,-2,0,0,0,4,1,-2,0,2,0}};
		  const uvec A5  = {{0,0,0,-2,0,0,0,0,0,-1,0,-2,0,2,1,-2,0}};
		  const uvec A6  = {{0,0,0,0,0,0,0,2,0,0,0,-4,-1,2,0,-2,0}};
		  const uvec A7  = {{0,0,0,0,0,0,2,0,0,-2,0,0,-1,2,0,-2,0}};
		  const uvec A8  = {{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,-1,0}};
		  const uvec A9  = {{0,2,0,2,0,0,0,0,-1,0,0,0,-1,0,0,0,0}};
		  const uvec A10 = {{0,0,0,0,0,0,0,0,0,0,0,4,1,-2,0,2,0}};
		  const uvec A11 = {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}};
		  const uvec A12 = {{0,0,0,-2,0,0,0,0,1,0,0,0,1,0,0,0,0}};
		  const uvec A13 = {{0,0,0,0,0,0,0,0,0,2,0,0,1,-2,0,2,0}};
		  const uvec A14 = {{0,0,0,0,0,0,0,0,0,0,0,0,-1,2,0,-2,0}};
		  const uvec A15 = {{0,0,0,2,0,0,0,0,0,0,0,0,-1,0,0,0,0}};
		  const uvec A16 = {{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0}};
		}
		*/

		
		  long nv = this.getTotalNumVertices();
		  
		  /*
		  ### GRAPHLAB ###
		  v.data().u[16] = (nv-1)*(nv-2)*(nv-3)/6.; //|V|-1 choose 3
		  */
		  /*
		  ### PAPER ###
		  pag.487 (6) 
		  */
		  /*Si tratta del coefficiente binomiale ((nv-1) su 3): deve essere di tipo long poichè int potrebbe non
		   * essere sufficiente */
		  vertex.getValue().u.add((nv-1)*(nv-2)*(nv-3)/6);
		  
		  
		  if (logger.isDebugEnabled()) {
			  logger.debug("\n");
			  
			  Iterator<BigIntegerWritable> iter = vertex.getValue().u.iterator();
			  
			  int i=0;
			  while (iter.hasNext()) {
				  logger.debug("u["+i+"] = "+iter.next().get());
				  i++;
			  }
			  
			  logger.debug("( u vector will be used to calculate n4local vector )"+C.N4);
		  }
	}
	
	
	
	private void calculateN4LocalVector(Vertex<IntWritable, VertexData, EdgeData> vertex, 
										BigIntegerArrayListWritable n3Global) {
		
		  final long[] A0  = {-6,-2,-6,-2,-3,-6,-3,-3,-2,-3,0,0,0,0,0,0,6};
		  final long[] A1  = {1,0,0,-2,0,0,0,0,0,-1,-1,-2,0,2,1,-1,0};
		  final long[] A2  = {0,0,0,2,0,0,0,0,0,1,1,2,0,-2,-1,1,0};
		  final long[] A3  = {0,0,0,2,0,1,0,0,0,1,0,2,0,-2,-1,2,0};
		  final long[] A4  = {0,0,0,0,2,0,0,-2,0,0,0,4,1,-2,0,2,0};
		  final long[] A5  = {0,0,0,-2,0,0,0,0,0,-1,0,-2,0,2,1,-2,0};
		  final long[] A6  = {0,0,0,0,0,0,0,2,0,0,0,-4,-1,2,0,-2,0};
		  final long[] A7  = {0,0,0,0,0,0,2,0,0,-2,0,0,-1,2,0,-2,0};
		  final long[] A8  = {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,-1,0};
		  final long[] A9  = {0,2,0,2,0,0,0,0,-1,0,0,0,-1,0,0,0,0};
		  final long[] A10 = {0,0,0,0,0,0,0,0,0,0,0,4,1,-2,0,2,0};
		  final long[] A11 = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0};
		  final long[] A12 = {0,0,0,-2,0,0,0,0,1,0,0,0,1,0,0,0,0};
		  final long[] A13 = {0,0,0,0,0,0,0,0,0,2,0,0,1,-2,0,2,0};
		  final long[] A14 = {0,0,0,0,0,0,0,0,0,0,0,0,-1,2,0,-2,0};
		  final long[] A15 = {0,0,0,2,0,0,0,0,0,0,0,0,-1,0,0,0,0};
		  final long[] A16 = {0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0};
		  
		  
		  /*
		  ### GRAPHLAB ###
		  //write to vertex data in case saving local counts to file
		  v.data().n4local[0] = (v.data().u * A0) / 6.;
		  v.data().n4local[1] = (v.data().u * A1);
		  v.data().n4local[2] = (v.data().u * A2);
		  v.data().n4local[3] = (v.data().u * A3);
		  v.data().n4local[4] = (v.data().u * A4) / 4.;
		  v.data().n4local[5] = (v.data().u * A5);
		  v.data().n4local[6] = (v.data().u * A6) / 2.;
		  v.data().n4local[7] = (v.data().u * A7) / 4.;
		  v.data().n4local[8] = (v.data().u * A8);
		  v.data().n4local[9] = (v.data().u * A9) / 6.;
		  v.data().n4local[10] = (v.data().u * A10) / 4.;
		  v.data().n4local[11] = (v.data().u * A11);
		  v.data().n4local[12] = (v.data().u * A12) / 2.;
		  v.data().n4local[13] = (v.data().u * A13) / 2.;
		  v.data().n4local[14] = (v.data().u * A14) / 4.;
		  v.data().n4local[15] = (v.data().u * A15) / 2.;
		  v.data().n4local[16] = (v.data().u * A16) / 6.;
		   */
		  
		  //write to vertex data in case saving local counts to file
		  //.toArray() restituire un array[] di IntWritable
		  vertex.getValue().n4local = new BigIntegerArrayListWritable();
		  /*1*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A0).divide(C.SIX));
		  /*2*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A1));
		  /*3*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A2));
		  /*4*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A3));
		  /*5*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A4).divide(C.FOUR));
		  /*6*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A5));
		  /*7*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A6).divide(C.TWO));
		  /*8*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A7).divide(C.FOUR));
		  /*9*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A8));
		  /*10*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A9).divide(C.SIX));
		  /*11*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A10).divide(C.FOUR));
		  /*12*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A11));
		  /*13*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A12).divide(C.TWO));
		  /*14*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A13).divide(C.TWO));
		  /*15*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A14).divide(C.FOUR));
		  /*16*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A15).divide(C.TWO));
		  /*17*/vertex.getValue().n4local.add(Util.multiply(vertex.getValue().u.toArray(),A16).divide(C.SIX));
		  
		  /*18*/vertex.getValue().n4local.add(0);  
		  /*19*/vertex.getValue().n4local.add(0);  
		  /*20*/vertex.getValue().n4local.add(0);  

		  
		  //Configurazione F1 - disconneceted vertex
		  BigInteger F1=n3Global.get(1).get().subtract
				  						(vertex.getValue().num_disc.get()).subtract
				  						(vertex.getValue().n4local.get(2).get()).subtract
				  						(vertex.getValue().n4local.get(6).get()).subtract
				  						(vertex.getValue().n4local.get(12).get()).subtract
				  						(vertex.getValue().n4local.get(3).get()).subtract
				  						(vertex.getValue().n4local.get(7).get())
		  								;
		  
		  //Configurazione F3  - disconneceted vertex
		  BigInteger F3=n3Global.get(2).get().subtract
					(vertex.getValue().num_wedges_c.get()).subtract
					(vertex.getValue().num_wedges_e.get()).subtract
					(vertex.getValue().n4local.get(5).get()).subtract
					(vertex.getValue().n4local.get(8).get()).subtract
					(vertex.getValue().n4local.get(13).get()).subtract
					(vertex.getValue().n4local.get(10).get()).subtract
					(vertex.getValue().n4local.get(15).get())
					;

		  //Configurazione F5 - disconnected vertex
		  BigInteger F5=n3Global.get(3).get().subtract
					(vertex.getValue().num_triangles.get()).subtract
					(vertex.getValue().n4local.get(14).get()).subtract
					(vertex.getValue().n4local.get(11).get()).subtract
					(vertex.getValue().n4local.get(16).get())
					;

		  
		  //Update value F0
		  BigInteger F0 = vertex.getValue().n4local.get(0).get().subtract(F1).subtract(F3).subtract(F5);
		  vertex.getValue().n4local.set(0, new BigIntegerWritable(F0));
		  
		  //Update value F1,F3 e F5 - disconnected vertex
		  vertex.getValue().n4local.set(17, new BigIntegerWritable(F1));
		  vertex.getValue().n4local.set(18, new BigIntegerWritable(F3));
		  vertex.getValue().n4local.set(19, new BigIntegerWritable(F5));
		 
		  		  		  
		  if (logger.isDebugEnabled()) {
			  int i=0;
			  logger.debug("\n");
			  for (BigIntegerWritable i_n4local:vertex.getValue().n4local) {
				  String n = (i==vertex.getValue().n4local.size()-1)?C.N:C.E;
				  logger.debug("n4local["+i+"] = "+i_n4local+n);
				  i++;
			  }
		  }
	}

	
	
	

	private void aggregateN4LocalVector(Vertex<IntWritable, VertexData, EdgeData> vertex) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Execution of "+GlobalCounts2Aggregator.class.getSimpleName()+
					".aggregate() for aggregation of n4Local vector ...   (see log file 06)"+C.N);
			logger.debug("(aggregated n4Local vector will be used by "+Master_Superstep3_Global4ProfilesFromLocal.class.getSimpleName()+
					" in next superstep)"+C.N);
		}
		
		GlobalCounts2 gc2 = new GlobalCounts2();
        //gc2.n4local = vertex.getValue().n4local;
          
		for(int i=0;i<vertex.getValue().n4local.size();i++) {
		    gc2.n4local.set(i,vertex.getValue().n4local.get(i));
		}
		
		this.aggregate(C.GLOBAL_COUNT2, gc2);
	}
	
	
	
	//############################LOG TEMPORANEO#############################################
	private void logMem() {
		logger.info("MEMORY:");
		logger.info("total memory (-Xmx) = "+Runtime.getRuntime().totalMemory()/1024);
		logger.info("max memory = "+Runtime.getRuntime().maxMemory()/1024);
		logger.info("free memory = "+Runtime.getRuntime().freeMemory()/1024 + "   (Current allocated free memory, is the current allocated space ready for new objects.)");
		long usedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		logger.info("used memory = "+usedMemory/1024);
		long totalFreeMemory = Runtime.getRuntime().maxMemory() - usedMemory;
		logger.info("total free memory = "+totalFreeMemory/1024);
	}
	//############################LOG TEMPORANEO#############################################

	

}
