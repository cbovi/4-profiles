package it.uniroma1.di.fourprofiles.io.format;


import java.io.IOException;
import java.util.regex.Pattern;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.giraph.io.EdgeReader;
import org.apache.giraph.io.ReverseEdgeDuplicator;
import org.apache.giraph.io.formats.IntNullTextEdgeInputFormat;
import org.apache.giraph.io.formats.TextEdgeInputFormat;
import org.apache.giraph.utils.IntPair;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;

/**
 * Simple text-based {@link org.apache.giraph.io.EdgeInputFormat} for
 * unweighted graphs with int ids.
 *
 * Each line consists of: source_vertex, target_vertex
 * 
 * 
 * NOTA BENE:
 * Poiché ogni riga contenuta nei file di input di graphlab è di tipo tsv tab separated value, ossia
 * 
 * vertexSource		vertexTarget
 * 
 * giraph interpresa questa tipologia di file in input come un formato di input di tipo edge (perché
 * ossia riga del file di input rappresenta un edge).
 * 
 * 
 * Giraph mette a disposizione la classe IntNullTextEdgeInputFormat per fare il parsing dei files di input
 * di questo tipo. 
 * 
 * 
 * La classe LongEdgeData_TextEdgeInputFormat_ReverseEdgeDuplicator è stata ottenuta 
 * modificando la classe predefinita IntNullTextEdgeInputFormat di giraph
 * dove Null è stato sostituito con EdgeData, per fare in modo che contestualmente
 * alla lettura del file di input vengano creati per ogni coppia sourceId, targetId
 * (ossia per ogni edge), un'istanza di EdgeData con tutti i valori di default
 * stabiliti all'interno del costruttore di EdgeData stesso.
 * 
 */
public class IntEdgeData_TextEdgeInputFormat_ReverseEdgeDuplicator extends
    TextEdgeInputFormat<IntWritable, EdgeData> {
    	
    	
  //private static final Logger logger = LogManager.getLogger(IntEdge_dataTextEdgeInputFormat.class);  	
  private final static Logger logger = Logger.getLogger(IntEdgeData_TextEdgeInputFormat_ReverseEdgeDuplicator.class);
  
  
    	
  /** Splitter for endpoints */
  private static final Pattern SEPARATOR = Pattern.compile("[\t ]");

  
/*

IntEdge_dataTextEdgeInputFormat_ReverseEdgeDuplicator

@Override
public EdgeReader<LongWritable, LongWritable> createEdgeReader(InputSplit split, TaskAttemptContext context) throws IOException {
    String duplicator = this.getConf().get("io.edge.reverse.duplicator");
    boolean useDuplicator = (null != duplicator) ? Boolean.parseBoolean(duplicator) : false;
    EdgeReader<LongWritable, LongWritable> reader = (useDuplicator) ? new ReverseEdgeDuplicator<LongWritable, LongWritable>(new SBEdgeReader()) : new SBEdgeReader();
    return reader;
}

 */
  
  
  
  
  
  @Override
  public EdgeReader<IntWritable, EdgeData> createEdgeReader(
		  				InputSplit split, 
		  				TaskAttemptContext context) 
		  		throws IOException {
	  
	  
	  
	    String duplicator = this.getConf().get("io.edge.reverse.duplicator");
	    boolean useDuplicator = (null != duplicator) ? Boolean.parseBoolean(duplicator) : false;
	    
	    
	    EdgeReader<IntWritable, EdgeData> reader = 
	    		/*if*/ (useDuplicator) 
	    		?/*then*/ 
	    				// ReverseEdgeDuplicator è una classe predefinita di giraph
//####################################################################################################
	    				new ReverseEdgeDuplicator<IntWritable, EdgeData>(
	    						// IntNullTextEdgeReader è la inner class dichiarata di seguito
	    						new IntNullTextEdgeReader()) 
//####################################################################################################
	    		:/*else*/ 
	    			// IntNullTextEdgeReader è la inner class dichiarata di seguito
	    			new IntNullTextEdgeReader();
	    						
	    						
	    return reader;	  
	  
    //return new IntNullTextEdgeReader();
  }

  
  
  
  
  /**
   * {@link org.apache.giraph.io.EdgeReader} associated with
   * {@link IntNullTextEdgeInputFormat}.
   */
  public class IntNullTextEdgeReader extends
      TextEdgeReaderFromEachLineProcessed<IntPair> {
	  
	//questo metodo prende in input una alla volte le linee del file di input e crea per ogni linea due vertici
	//ed un arco (Giraph ha dentro di se la logica per invocare questo metodo)
    @Override
    protected IntPair preprocessLine(Text line) throws IOException {
    	
      String[] tokens = SEPARATOR.split(line.toString());
      
      return new IntPair(
    		  Integer.parseInt(tokens[0]),
    		  Integer.parseInt(tokens[1]));
    }

    
    @Override
    protected IntWritable getSourceVertexId(IntPair endpoints)
      throws IOException {
      return new IntWritable(endpoints.getFirst());
    }

    
    @Override
    protected IntWritable getTargetVertexId(IntPair endpoints)
      throws IOException {
      return new IntWritable(endpoints.getSecond());
    }

    @Override
    protected EdgeData getValue(IntPair endpoints) throws IOException {
        EdgeData ed=new EdgeData();
        return ed;
    }
  }
}

