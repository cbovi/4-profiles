package it.uniroma1.di.fourprofiles.util;

import java.math.BigInteger;

public class C {

	public static final int USIZE_17 = 17;
	public static final int USIZE_20 = 20;
	public static final int USIZE_11 = 11;
	public static final int USIZE_12 = 12;
	public static int GLOBAL_3=0;
	public static final String SAMPLE_ITER = "sample_iter";
	public static final String MIN_PROB = "min_prob";
	public static final String MAX_PROB = "max_prob";
	public static final String PROB_STEP = "prob_step";
	public static final String SAMPLE_PROB_KEEP = "sample_prob_keep";
	public static final String IVN = "isolatedVertexNumber";

	
	//public static final String PER_VERTEX = "per_vertex";
	public static final String FOUR_GLOBAL_PARAMETER = "4global";
	public static final String FOUR_GLOBAL_FROM_LOCAL_PARAMETER = "4globalFromLocal";
	public static final String THREE_GLOBAL_PARAMETER = "3global";
	public static final String GLOBAL_COUNT1 = "global_count1";
	public static final String GLOBAL_COUNT2 = "global_count2";
	public static final String CUSTOM_INFO_LOG_LEVEL_PARAMETER = "customInfoLogLevel";
	/**
	 * Enable log of vertex id, edges, received messages at supersteps 1 and 2
	 */
	public static final int CUSTOM_INFO_LOG_LEVEL_1 = 1;
	/**
	 * Enable log of memory usage by every vertex at the end of supersteps 1 and 2"
	 */
	public static final int CUSTOM_INFO_LOG_LEVEL_2 = 2;
	/**
	 * Used in GlobalCounts1Aggregator since following instruction is not available in that class:
	 * 		getConf().get(U.CUSTOM_INFO_LOG_LEVEL_PARAMETER)
	 */
	public static int CUSTOM_INFO_LOG_LEVEL = 0;
	public static final String E = "";
	public static final String C = ", ";
	public static final String N = "\n";
	public static final String N2 = N+N;
	public static final String N3 = N2+N;
	public static final String N4 = N2+N2;
	public static final String S = " <-----------------------------";
	public static final String SN = S+N;
	public static final String SN2 = S+N2;
	public static final String SN4 = S+N4;
	public static final String S2 = " <<<<<<<<<<<<<<";
	public static final String S2N = S2+N;
	public final static BigInteger FOUR=new BigInteger("4");
	public final static BigInteger TWO = new BigInteger("2");
	public final static BigInteger THREE = new BigInteger ("3");
	public static final BigInteger ONE = new BigInteger("1");
	public static final BigInteger SIX = new BigInteger("6");
	public static final BigInteger EIGHT = new BigInteger("8");
	public static final BigInteger TWENTYFOUR = new BigInteger("24");
	
	
	/*
	   ### OBSOLETE ###
	   Corrisponde alla directory di output predefinita di apache giraph.
	 
	public static final String OUTPUT_DIRECTORY = "output/";
	public static final String OUTPUT_GLOBAL_FROM_LOCAL_FILE = "globalFromLocal4Profiles.txt";
	public static final String OUTPUT_GLOBAL_FILE = "global4profiles.txt";
	*/	

}
