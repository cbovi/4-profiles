package it.uniroma1.di.fourprofiles.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.math.BigInteger;

import org.apache.giraph.edge.Edge;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global3Profiles;

public class Util {
	
	
	public final static Logger logger = Logger.getLogger(Util.class);
	

	
	public static String getNeighborhood(
			Vertex<IntWritable, VertexData, EdgeData> vertex, 
			IntHashSetWritable neighborhood) {
		
		
		String neighborhoodStr = "";
		for (Edge<IntWritable,EdgeData> edge // elemento della lista 
				:
			vertex.getEdges()) { //è la lista
			
			neighborhoodStr += edge.getTargetVertexId() + it.uniroma1.di.fourprofiles.util.C.C; // ", "
			
			
			/* ### EBOOK ### 
			From pag. 95 of "Large Scale Graph Processing Using Apache Giraph" book
			It is important to clone the vertex id before adding it to the list as Giraph provides 
			a temporary reference to all edges, in the computation method.
			Va clonato perchè al prossimo ciclo for verrebbe sovrascritto dal valore successivo. 
			 */
			/*
			neighborhood.add(
					WritableUtils.clone(
							edge.getTargetVertexId(), 
					getConf()));
			*/
			// This other way is simpler and probably faster than cloning
			// Invece di usare la lista vertex.getEdges() si riversa il vicinato in un hashset
			// perchè nel superstep successivo sarà più efficiente con un hashset calcolare le
			// intersezioni con gli altri vicinati. (contains è un metodo che ha solo l'hashset
			// e non le liste, contains sugli hashset ha una complessità computazionale O(1) 
			// invece per cercare un elemento in una lista la devi scorrere tutta e quindi la 
			// complessità sarebbe O(n) dove n è la lunghezza della lista )
			// Si invia un messaggio contenente il vicinato in una struttura dati già efficiente
			neighborhood.add(
					new IntWritable(edge.getTargetVertexId().get()));

	
		}
		
		
		if (!neighborhoodStr.isEmpty())
			neighborhoodStr = neighborhoodStr.substring(0, neighborhoodStr.length()-2); // to delete last ", "
		
		
		return neighborhoodStr;
	}
	
	
	
	
	
	public static void logInputGraph(String edgeInputPath, long nv, long ne) {
		
		System.out.println("\nINPUT GRAPH:");
		Master_Superstep2_Global3Profiles.logger.debug("INPUT GRAPH:");
		
		System.out.println("giraph.edge.input.dir="+edgeInputPath);
		Master_Superstep2_Global3Profiles.logger.debug("giraph.edge.input.dir="+edgeInputPath);
		
		System.out.println("Vertices="+nv);
		Master_Superstep2_Global3Profiles.logger.debug("Vertices="+nv);

		
		System.out.println("Directed Edges="+ne);
		Master_Superstep2_Global3Profiles.logger.debug("Directed Edges="+ne);
		
		System.out.println("Directed Edges/2=Undirected Edges="+ne/2);
		Master_Superstep2_Global3Profiles.logger.debug("Directed Edges/2=Undirected Edges="+ne/2+C.N2);
	}
	
	/* 
    ### GRAPHLAB ###
    La seguente riga non è stata tradotta evitando così di dover predisporre
    doppie istruzioni come nel codice originale in C++: 
 
		//comment the following line if you want to use integer counters
		#define  DOUBLE_COUNTERS
	
	
	Segue un esempio di doppia istruzione:
	
	#ifdef DOUBLE_COUNTERS
	double n[4] = {};
	#else
	size_t n[4] = {};
	#endif 
    */	


	
	public static String logNeighborhood(IntWritable iw, IntHashSetWritable ialw) {
		return iw.toString()+"->"+ialw.toString();
	}

	
	
	public static long multiply (long[] a, long[] b)  {
		
		long sum = 0;
	    for (int i = 0; i < it.uniroma1.di.fourprofiles.util.C.USIZE_11; i++) {
	        sum += a[i]
	        	   *
	        	   b[i];
	    }
	    
	    return sum;
	      
	}	
	
	
	public static BigInteger multiply (BigInteger[] a, long[] b)  {
		
		BigInteger sum = BigInteger.ZERO;
	    for (int i = 0; i < it.uniroma1.di.fourprofiles.util.C.USIZE_11; i++) {
	        sum = sum.add(a[i].multiply(BigInteger.valueOf(b[i])));
	        	  
	    }
	    
	    return sum;
	      
	}	
	
	
	/*
	 il seguente metodo è stato sostituito da 
	 public static BigInteger multiply (Object[] a, long[] b)
	 
	 
	public static long multiply (Object[] a, long[] b)  {
		
		long sum = 0;
	    for (int i = 0; i < USIZE_17; ++i){
	        sum += 
	        		  ((LongWritable)a[i]).get()
	        		  *
	        		  b[i];
	      }
	    return sum;
	      
	}
	*/
	public static BigInteger multiply (Object[] a, long[] b)  {
		
		BigInteger sum = BigInteger.ZERO;
	    for (int i = 0; i < it.uniroma1.di.fourprofiles.util.C.USIZE_17; ++i){
	        sum = sum.add(
	        		  ((BigIntegerWritable)a[i]).get().multiply(
	        		  
	        		  BigInteger.valueOf(b[i])));
	      }
	    return sum;
	      
	}
	

	public static String stackTraceToString(Throwable t)
	  {
	    ByteArrayOutputStream b = new ByteArrayOutputStream();
	    PrintStream p = new PrintStream(b);
	    t.printStackTrace(p);
	    p.flush();
	    return b.toString();
	  }

	
	
	
	public static String shortStackTraceToString(Throwable t)
	  {
	    StackTraceElement[] stackTrace = t.getStackTrace();
	    StringBuilder logMsg = new StringBuilder();
	    
	    int i = 1;
	    for (StackTraceElement s : stackTrace) {
	      i++;
	      logMsg.append(s.toString());
	      logMsg.append(" ");
	      if (i == 6)
	        break;
	    }
	    return logMsg.toString();
	  }

	
	
	
	public static String stackTraceToString()
	  {
	    StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
	    StringBuilder logMsg = new StringBuilder();
	    
	    for (StackTraceElement s : stackTrace) {
	      logMsg.append(s.toString());
	      logMsg.append("\n");
	    }
	    
	    return logMsg.toString();
	  }

	
	
	
	public static String shortStackTraceToString()
	  {
	    StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
	    StringBuilder logMsg = new StringBuilder();
	    
	    int i = 1;
	    for (StackTraceElement s : stackTrace) {
	      i++;
	      logMsg.append(s.toString());
	      logMsg.append(" ");
	      if (i == 6)
	        break;
	    }
	    return logMsg.toString();
	  }

	
	private static final long MB = 1024*1000;
	private static final String MBs = " MBs";
	
	public static void logRuntimeInfo(long superstep) {
		/* TODO
		Iterator<WorkerInfo> it = this.getWorkerInfoList().iterator();
		while (it.hasNext()) {
			
			WorkerInfo wi = it.next();
			wi.
		}
		*/
		
		/*
		System.out.println("env HADOOP_ROOT_LOGGER="+System.getenv("HADOOP_ROOT_LOGGER"));
		System.out.println("env $HADOOP_ROOT_LOGGER="+System.getenv("$HADOOP_ROOT_LOGGER"));
		System.out.println("property HADOOP_ROOT_LOGGER="+System.getProperty("HADOOP_ROOT_LOGGER"));
		System.out.println("property $HADOOP_ROOT_LOGGER="+System.getProperty("$HADOOP_ROOT_LOGGER"));
		System.out.println("logger.getLevel()="+logger.getLevel());
		*/
		
		
		/*
		System.out.println("LOG RUNTIME INFORMATIONS:");
		System.out.println("debug="+logger.isDebugEnabled());
		System.out.println("info="+logger.isInfoEnabled());
		System.out.println("trace="+logger.isTraceEnabled());
		*/
		
		System.out.println("\nSUPERSTEP NUMBER: " +superstep + C.S);
		
		// https://stackoverflow.com/questions/12807797/java-get-available-memory
		/* Total number of processors or cores available to the JVM */
		System.out.println("\nPROCESSORS:\nAvailable processors (cores): " + Runtime.getRuntime().availableProcessors());
		
		// https://stackoverflow.com/questions/3571203/what-are-runtime-getruntime-totalmemory-and-freememory
		System.out.println("\nMEMORY:");
		System.out.println("total memory (-Xmx) = "+Runtime.getRuntime().totalMemory()/MB + MBs);
		System.out.println("max memory = "+Runtime.getRuntime().maxMemory()/MB + MBs);
		System.out.println("free memory = "+Runtime.getRuntime().freeMemory()/MB + MBs + "   (Current allocated free memory, is the current allocated space ready for new objects.)");
		long usedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("used memory = "+usedMemory/MB + MBs);
		long totalFreeMemory = Runtime.getRuntime().maxMemory() - usedMemory;
		System.out.println("total free memory = "+totalFreeMemory/MB + MBs);
		
		
		
		
		System.out.println("\nFILE SYSTEM:");
		/* Get a list of all filesystem roots on this system */
		File[] roots = File.listRoots();

		/* For each filesystem root, print some info */
		for (File root : roots) {
		    System.out.println("File system root: " + root.getAbsolutePath());
		    System.out.println("Total space (bytes): " + root.getTotalSpace()/MB + MBs);
		    System.out.println("Free space (bytes): " + root.getFreeSpace()/MB + MBs);
		    System.out.println("Usable space (bytes): " + root.getUsableSpace()/MB + MBs);
		}
		
		
		System.out.println(C.N4);
		
		// java.lang.OutOfMemoryError: GC overhead limit exceeded
		// https://plumbr.io/outofmemoryerror/gc-overhead-limit-exceeded
	}


	public static MapWritable convertListOfMapsInASingleMap(
			Iterable<MapWritable> listOfMaps,
			StringBuilder numberOfMessages){
	
		MapWritable singleMap = new MapWritable();
	
		int c=0;
		for (MapWritable inputMap: listOfMaps){
	                c++;   				
					singleMap.putAll(inputMap);
		
		}
		numberOfMessages.append(c);
		
	    return singleMap;
	}
	
	public static int countMessages(Iterable<MapWritable> listOfMaps){
		int c=0;
		for (MapWritable inputMap: listOfMaps){
			c++;
		}
		return c;	
	}
	
	
}
