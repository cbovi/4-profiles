package it.uniroma1.di.fourprofiles.master;
import java.math.BigInteger;

import org.apache.giraph.master.DefaultMasterCompute;
//import org.apache.logging.log4j.LogManager;//Log4j2
//import org.apache.logging.log4j.Logger;//Log4j2
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts1;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts2;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global3Profiles;
import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global4Profiles;
import it.uniroma1.di.fourprofiles.master.superstep3.Master_Superstep3_Global4ProfilesFromLocal;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;
import it.uniroma1.di.fourprofiles.worker.superstep0.gas1.Worker_Superstep0_GAS1;
import it.uniroma1.di.fourprofiles.worker.superstep1.Worker_Superstep1;
import it.uniroma1.di.fourprofiles.worker.superstep1.gas2.Worker_Superstep1_GAS2;
import it.uniroma1.di.fourprofiles.worker.superstep1.gas3.Worker_Superstep1_GAS3;
import it.uniroma1.di.fourprofiles.worker.superstep1.persaggr1.GlobalCounts1Aggregator;
import it.uniroma1.di.fourprofiles.worker.superstep2.gas4.Worker_Superstep2_GAS4;
import it.uniroma1.di.fourprofiles.worker.superstep2.persaggr2.GlobalCounts2Aggregator;


/**
### EBOOK ###

1) from paragraph 4.1.2 "IMPLEMENTATION DETAILS" pag. 122 of "Large Scale Graph Processing Using Apache Giraph" book
Recall that implementing any graph algorithm in Giraph requires extending the basic 
abstract class BasicComputation and specifying the four parameters for modeling the 
input graph:VertexID,VertexData, EdgeData, and MessageData. 
These four parameters should be changed according to the input graph and the algorithm
specifications.

2) pag. 141 of "Large Scale Graph Processing Using Apache Giraph" book
Fig. 5.1, MasterCompute class is executed in the master
node at the beginning of each superstep and before starting the graph computation
at the worker nodes. Programmers can use this stage to change graph computation
classes, such as message combiners and vertex compute class, during runtime.
Moreover, this stage gives

3) pag. 144 of "Large Scale Graph Processing Using Apache Giraph" book
... where the random generator depends
on a custom generated seed. To ensure that all vertices use the same seed, programmers
may produce the seed on the master compute and then use broadcast to
send the seed value to all vertices.
 */
public class Master_FourProfiles extends DefaultMasterCompute {	

	//private static final Logger logger = LogManager.getLogger(FourProfiles.class);//Log4j2
	public final static Logger logger = Logger.getLogger(Master_FourProfiles.class);
	
	
	/* ### TODO ### 
	   Verificare se siano effettivamente necessari ogetti di tipo Text al posto di String
	   Using Hadoop's Text object for easier serialization/deserialization
	Text agg1;
	Text agg2;*/
	
	private Master_Superstep2_Global3Profiles master_superstep2_global3Profiles;
	private Master_Superstep2_Global4Profiles master_superstep2_global4Profiles;
	private Master_Superstep3_Global4ProfilesFromLocal master_superstep3;
	
	private boolean is4Global;
	private boolean is4GlobalFromLocal;
	private boolean is3Global;
    private long superstep;
    private long ivn;
    
    
    
	public Master_FourProfiles() {
		
	

		this.master_superstep2_global3Profiles = new Master_Superstep2_Global3Profiles();
		this.master_superstep2_global4Profiles = new Master_Superstep2_Global4Profiles();
		this.master_superstep3 = new Master_Superstep3_Global4ProfilesFromLocal();
		
		/*
		 ### TODO ###
		 vedi sopra
		 
		 this.agg1 = new Text(IFourProfiles.GLOBAL_COUNT);
		 this.agg2 = new Text(IFourProfiles.GLOBAL_COUNT2);
		*/
	}

	
	
	/*
	   ### NOTES ###
	   
	   1) Examples taken from
	   C:\TESI_CRISTINA\03 - EBOOKS\APACHE GIRAPH\01_GiraphBookSourceCodes-master\GiraphBookSourceCodes-master\
	   chapter04_05\src\main\java\bookExamples\ch5\masterCompute\AggregatorMasterCompute.java
	   
	   2)
	    There are
		two types of aggregators: nonpersistent and persistent aggregators. The nonpersistent
		aggregators will be reset to their default initial value at the end of the superstep before
		applying any value changes from workers of the current superstep. The persistent
		aggregators, on the other hand, will not reset the aggregator value and it will apply
		any changes requested by theworkers of the current superstep on the latest aggregator
		value. Since the aggregator value is made available to both master and worker nodes,
		Giraph will keep synchronizing its value across workers even if no worker in the
		next superstep requires to access the aggregator value.
		
	   3) Il metodo initialize viene usato per:
		Initialization phase, used to initialize aggregator/Reduce/Broadcast
		or to initialize other objects
	 */
	@Override
	public void initialize() throws InstantiationException, IllegalAccessException {
		
		/*
		   ### NOTES ###
		   
		   1) 
		   FROM http://giraph.apache.org/aggregators.html
		   You can register aggregators either in MasterCompute.initialize() - in that case the registered aggregator will be available through whole 
		   application, or you can do it in MasterCompute.compute() - the aggregator will then be available in current and each of the following 
		   supersteps.
		   2)
		   Gli aggregators non vengono registrati nel metodo initialize poichÃ© non occorrono dal superstep 0
		   ma nei superstep successivi e quindi impiegherebbero inutilmente risorse. Pertanto gli aggregator
		   vengono registrati nel metodo compute.
		 
		  //this.registerPersistentAggregator(IFourProfiles.GLOBAL_COUNT, global_countsAggregator.class);
		  //this.registerPersistentAggregator(IFourProfiles.GLOBAL_COUNT2, global_counts2Aggregator.class);
		*/
	}

  
	@Override
	public final void compute () {  
		this.superstep = getSuperstep ();
		
		logger.info(C.N);
		logger.info("START SUPERSTEP "+superstep+" OF MASTER"+C.S);

		Util.logRuntimeInfo(this.superstep);
		
		if (superstep == 0) {
			
			getCustomArguments();
			
			runSuperstep0();
			
		} else if (superstep == 1) {
			
			logger.info("Vertices="+this.getTotalNumVertices());
			logger.info("Isolated Vertices="+this.ivn);
			logger.info("Directed Edges="+this.getTotalNumEdges());
			logger.info("Undirected Edges=Directed Edges/2="+this.getTotalNumEdges()/2+C.N4);
			
			System.out.println("Vertices="+this.getTotalNumVertices());
			System.out.println("Isolated Vertices="+this.ivn);
			System.out.println("Directed Edges="+this.getTotalNumEdges());
			System.out.println("Undirected Edges=Directed Edges/2="+this.getTotalNumEdges()/2+C.N4);
			
			runSuperstep1();
			
		} else if (superstep == 2) {
			runSuperstep2();
			      
		}
		else if (superstep==3) {
			runSuperstep3();
			
		}
		
		
	}



	private void getCustomArguments() {
		/**
		### OBSOLETE ###
		
		Valori di esempio:	  
			  -ca sample_iter=10
			  -ca min_prob=0.5
			  -ca max_prob=1
			  -ca prob_step 0.1

		-ca sample_iter=10 -ca min_prob=0.5 -ca max_prob=1 -ca prob_step=0.1 -ca sample_prob_keep=0.5 

	    int sample_iter = Integer.parseInt(getConf().get(IMaster_FourProfiles.SAMPLE_ITER));
	    long min_prob = Double.parseDouble(getConf().get(IMaster_FourProfiles.MIN_PROB));
	    long max_prob = Double.parseDouble(getConf().get(IMaster_FourProfiles.MAX_PROB));
	    long prob_step = Double.parseDouble(getConf().get(IMaster_FourProfiles.PROB_STEP));
	    long sample_prob_keep = Double.parseDouble(getConf().get(IMaster_FourProfiles.SAMPLE_PROB_KEEP));
	    */
	    
		
		
		
		
		/**
		  ### NOTES ### 
		  Nel programma Graphlab il parametro Ã¨ denominato "-per_vertex=OUTPUT_FILE_NAME".
		  Se "-per_vertex" Ã¨ diverso dalla stringa vuota, allora significa che
          bisogna calcolare il LOCAL anzichÃ© il GLOBAL.          
          Nel programma Graphlab questo parametro da anche il nome al file di output.
          
          Invece in questo programma Ã¨ possibile usare due parametri distinti:
          - ca global=true
          - ca globalFromLocal=true
          */

		this.is4Global = Boolean.parseBoolean(
				getConf().get(C.FOUR_GLOBAL_PARAMETER));
		
		this.is4GlobalFromLocal = Boolean.parseBoolean(
				getConf().get(C.FOUR_GLOBAL_FROM_LOCAL_PARAMETER));
		
		this.is3Global = Boolean.parseBoolean(
				getConf().get(C.THREE_GLOBAL_PARAMETER));
		
		this.ivn = Long.parseLong(getConf().get(C.IVN));
		
		//TODO: 
		//class org.apache.giraph.conf.AllOptions
		/*
		System.out.println("giraph.nettyClientThreads = "+getConf().get("giraph.nettyClientThreads"));
		System.out.println("giraph.nettyServerThreads = "+getConf().get("giraph.nettyServerThreads"));
		System.out.println("giraph.serverReceiveBufferSize = "+getConf().get("giraph.serverReceiveBufferSize"));
		System.out.println("giraph.clientSendBufferSize = "+getConf().get("giraph.clientSendBufferSize"));
		*/	
		
		if (this.is4Global == false && this.is4GlobalFromLocal == false && this.is3Global == false) {
			String errMsg = C.N2+"\"-ca 4global=true\" or \"-ca 4globalFromLocal=true\" or \"-ca 3global=true\"  parameter must be set;";
			System.err.println(errMsg);
			logger.error(errMsg);
			haltComputation();
		}
		
		
		
		logger.info("CUSTOM ARGUMENTS:");
		logger.info("   -ca 4global="+this.is4Global);
		logger.info("   -ca 4globalFromLocal="+this.is4GlobalFromLocal);
		logger.info("   -ca 3global="+this.is3Global+C.N2);
		/*
		  ### OBSOLETE ###
		  logger.debug("sample_iter="+sample_iter);
		  logger.debug("min_prob="+min_prob);
		  logger.debug("max_prob="+max_prob);
		  logger.debug("prob_step="+prob_step);
		  logger.debug("sample_prob_keep="+sample_prob_keep);
		*/
	}



	private void runSuperstep0() {
		
		  /*
		    ### NOTES ### 
		    I due metodi getTotalNumvertexes() e getTotalNumEdges() sono documentati qui:
		    https://giraph.apache.org/apidocs/org/apache/giraph/graph/Computation.html
		      
		      TODO VERIFICARE che anche il metodo getTotalNumVertices invocato dai workers restituisca in output il numero totale di vertici
		                      su tutte le partizioni in cui Ã¨ stato suddiviso il grafo di input
		    
		                      
			  - long getTotalNumvertexes()
				Gets the total number of vertexes in all the partitions that existed in the previous superstep. If it
				is the first superstep, it will return â€“1.
			
			  - long getTotalNumEdges()
			    Gets the total number of edges in all partitions that existed in the previous superstep. If it is the
			    first superstep, it will return â€“1.
		
		    
		    Inutile eseguire a questo punto i metodi getTotalNumvertexes() e getTotalNumEdges() poichÃ© restituiscono entrambi 0
			non essendo stato ancora eseguito il parsing del grafo di input.
		    
		    logger.debug("total num of vertices ="+this.getTotalNumVertices());
		    logger.debug("total num of edges ="+this.getTotalNumEdges()/2); // ### OBSOLETE ### (before sampling) 
		  */
		
		
		
		if (logger.isDebugEnabled()) logger.debug("set execution of "+Worker_Superstep0_GAS1.class.getSimpleName()+" ...   (see log file 02)");
		setComputation (Worker_Superstep0_GAS1.class);
		
		logger.info("END SUPERSTEP "+this.superstep+" OF MASTER"+C.SN);
		logger.info("NOW WORKERS BEGIN SUPERSTEP "+this.superstep+" ..."+C.SN4);
		
	}



	
	private void runSuperstep1() {
		
		if (this.is4Global == true || this.is3Global == true
		
			//#BROADCAST		
			|| (this.is4GlobalFromLocal=true))
		
		{
			
			if (logger.isDebugEnabled()) logger.debug("registration of persistent aggregator "+GlobalCounts1Aggregator.class.getSimpleName()+
					" that will be used by workers in superstep 1 and by master in superstep 2 to calculate global 3-profiles and global 4-profiles"+C.N);
			/*
			### NOTES ###
			FROM http://giraph.apache.org/aggregators.html
			You can register aggregators either in MasterCompute.initialize() - in that case the registered aggregator will be available through whole 
			application, or you can do it in MasterCompute.compute() - the aggregator will then be available in current and each of the following 
			supersteps.
			 */
			try {this.registerPersistentAggregator(C.GLOBAL_COUNT1, GlobalCounts1Aggregator.class);}
			catch (Throwable t) {logger.error(Util.stackTraceToString(t));this.haltComputation();throw new RuntimeException(t);}
		
			if (logger.isDebugEnabled()) logger.debug("set execution of "+Worker_Superstep1.class.getSimpleName()+" (that in turn calls "+Worker_Superstep1_GAS2.class.getSimpleName()+", "+
					Worker_Superstep1_GAS3.class.getSimpleName()+" and "+GlobalCounts1Aggregator.class.getSimpleName()+
					".aggregate()) ...   (see respectively log files 03, 04, 05 and 06)");
		
		}
		
		
		
		if (this.is4GlobalFromLocal == true) {
		
			if (logger.isDebugEnabled()) logger.debug("set execution of "+Worker_Superstep1.class.getSimpleName()+" (that in turn calls "+
					Worker_Superstep1_GAS2.class.getSimpleName()+") ...   (see respectively log files 03, and 04)");
		}
		

		
		/*
		 * execution of Worker_Superstep1_GAS2 is skipped if isGlobalFromLocal == true
		 */
		setComputation (Worker_Superstep1.class); 
		
		
		
		logger.info("END SUPERSTEP "+this.superstep+" OF MASTER"+C.SN);
		logger.info("NOW WORKERS BEGIN SUPERSTEP "+this.superstep+" ..."+C.SN4);
		
		/*
		 ### OBSOLETE ### 
		NOTA BENE Il seguente tentativo di raccogliere i valori aggregati nello step corrente non Ã¨ andato a buon fine
		in quanto viene restituita una variabile con valore 0
		
		global_counts gc = this.getAggregatedValue(IFourProfiles.GLOBAL_COUNT);
		logger.debug("\n\nGLOBAL COUNT gc:");
		logger.debug("gc.num_disc="+gc.num_disc);
		*/
	}



	
	private void runSuperstep2() {
		
		
		// Il numero delle partizioni
		// System.out.println("\n\ngiraph.userPartitionCount="+getConf().get("giraph.userPartitionCount"));
		
		
		// Le variabili indicate nella mail https://www.mail-archive.com/user@giraph.apache.org/msg02925.html
		// sono obsolete, infatti la loro stampa restituisce null
		// System.out.println("\n\ngiraph.maxNumberOfOpenRequests="+getConf().get("giraph.maxNumberOfOpenRequests"));
		// System.out.println("\n\ngiraph.waitForRequestsConfirmation="+getConf().get("giraph.waitForRequestsConfirmation"));
		

		BigIntegerArrayListWritable n3Global = null;
		
		// IF da eseguire solo per il calcolo GLOBALE dei 4-PROFILES
		if (this.is4Global == true || this.is3Global == true 
		
		//BROADCAST
		|| (this.is4GlobalFromLocal=true))
			
				{
			/*
			 ### GRAPHLAB ###
			 global_counts = graph.map_reduce_vertices<vertex_data_type>(get_vertex_data);
			 */
			
			/* 
			### EBOOK ###
			FROM BOOK â€œ02 Practical Graph Analytics with Apache Giraphâ€� pag. 55
			Computing Global Functions with Aggregators
			...During each superstep, these global functions aggregate the values, and the results are available to
			the vertices during the following superstep...
			
			### NOTES ###
			NOTA BENE: L'AGGREGATOR "GLOBAL_COUNT1" E' STATO REGISTRATO NEL PRECEDENTE STEP 1 CON IL METODO registerPersistentAggregator
			ED I VALORI AGGREGATI (SOMMATI) SONO DISPONIBILI SOLO A PARTIRE DAL SUPERSTEP SUCCESSIVO 2 CON IL METODO getAggregatedValue
			 */
			if (logger.isDebugEnabled()) {
				logger.debug("getting aggregated values calculated by workers in previous superstep ...   (see last part of log file 06)");
				logger.debug("aggregated values are:");
				logger.debug("- u vector, num_disc, num_empty, num_wedges_c, num_wedges_e, num_triangles (calculated in "+
							Worker_Superstep1_GAS2.class.getSimpleName()+")");
				logger.debug("- eqn10const (calculated in "+ Worker_Superstep1_GAS3.class.getSimpleName()+")"+C.N2);
			}
			else if (logger.isInfoEnabled()) {
				logger.info("getting aggregated values ..."+C.N2);
			}
			
			
			
			//#######################################################################
			//con questo metodo il master riceve il valore aggregato che tutti i workers hanno calcolato
			//nel superstep precedente
			GlobalCounts1 gc1 = this.getAggregatedValue(C.GLOBAL_COUNT1);
			
			//#######################################################################
			
			if (logger.isDebugEnabled()) logger.debug("execution of "+Master_Superstep2_Global3Profiles.class.getSimpleName()+
					" to calculate global 3-profiles from aggregated values (num_disc, num_empty, num_wedges_c, num_wedges_e, num_triangles) calculated by workers in previous superstep ...   (see log file 07)"+C.N);
			
			
			n3Global=master_superstep2_global3Profiles.calculateGlobal3Profiles(
					gc1, 
					// https://giraph.apache.org/giraph-core/apidocs/constant-values.html
					getConf().get("giraph.edge.input.dir"),
					this.getTotalNumVertices(),
					this.getTotalNumEdges(),
					this.ivn);
			
			
			BigIntegerArrayListWritable isolatedVertex = new BigIntegerArrayListWritable();
			
			isolatedVertex = calculateIsolatedVertex(n3Global, this.ivn, this.getTotalNumEdges(), this.getTotalNumVertices());
			
			
			if (ivn>0) {
				
						String n3Global0 = "n3Global[0]="+n3Global.get(0).get().add(isolatedVertex.get(4).get());
						String n3Global1 = "n3Global[1]="+n3Global.get(1).get().add(isolatedVertex.get(5).get());
						String n3Global2 = "n3Global[2]="+n3Global.get(2).get();
						String n3Global3 = "n3Global[3]="+n3Global.get(3).get();
				
						System.out.println(n3Global0);
						logger.info(n3Global0+"   ( gc1.num_empty/3 )");
						System.out.println(n3Global1);
						logger.info(n3Global1+"   ( gc1.num_disc/3 )");
						System.out.println(n3Global2);
						logger.info(n3Global2+"   ( (gc1.num_wedges_c+gc1.num_wedges_e)/3 )");
						System.out.println(n3Global3);
						logger.info(n3Global3+"   ( gc1.num_triangles/3 )"+C.N2);
			}			

			
			
			if (this.is4Global == true) {
				if (logger.isDebugEnabled()) logger.debug("execution of "+Master_Superstep2_Global4Profiles.class.getSimpleName()+
					" to calculate global 4-profiles from aggregated values (u vector and eqn10const) calculated by workers in previous superstep ...   (see log file 07)"+C.N);
			
				master_superstep2_global4Profiles.calculate4Profiles(gc1, this.getTotalNumVertices(), isolatedVertex, this.ivn);	
			
			}
			
			//#BROADCAST
			if (is4GlobalFromLocal==false) {
			
				logger.info (">>> HALT COMPUTATION <<<");
				this.haltComputation();
			}
			
			logger.info("END SUPERSTEP "+this.superstep+" OF MASTER"+C.SN4);
			

		} 
		
		
		
		
		if (this.is4GlobalFromLocal == true) {
			
			if (logger.isDebugEnabled()) logger.debug("registration of persistent aggregator "+ GlobalCounts2Aggregator.class.getSimpleName() + 
					" that will be used by workers in superstep 2 and by master in superstep 3 to calculate global 4-profiles from local 4-profiles"+C.N);
			/*
			### NOTES ###
			FROM http://giraph.apache.org/aggregators.html
			You can register aggregators either in MasterCompute.initialize() - in that case the registered aggregator will be available through whole 
			application, or you can do it in MasterCompute.compute() - the aggregator will then be available in current and each of the following 
			supersteps.
			 */
			try {this.registerPersistentAggregator(C.GLOBAL_COUNT2, GlobalCounts2Aggregator.class);}
			catch (Throwable t) {logger.error(Util.stackTraceToString(t));this.haltComputation();throw new RuntimeException(t);}
			

			
			if (logger.isDebugEnabled()) logger.debug("set execution of "+Worker_Superstep2_GAS4.class.getSimpleName()+" and "+GlobalCounts2Aggregator.class.getSimpleName()+
					" ...   (see respectively log files 08 and 09)");
			
			//#BROADCAST
			this.broadcast("n3Global", n3Global);
				
			
			setComputation (Worker_Superstep2_GAS4.class);
			
			
			logger.info("END SUPERSTEP "+this.superstep+" OF MASTER"+C.SN);
			logger.info("NOW WORKERS BEGIN SUPERSTEP "+this.superstep+" ..."+C.SN4);
		}
		
		
	}

	
	
	
	private void runSuperstep3() {
		
		if (this.is4GlobalFromLocal == true) {
			/*
			### NOTES ###
			L'AGGREGATOR "GLOBAL_COUNT2" E' STATO REGISTRATO NEL PRECEDENTE STEP 2 CON IL METODO registerPersistentAggregator
			ED I VALORI AGGREGATI (SOMMATI) SONO DISPONIBILI SOLO A PARTIRE DAL SUPERSTEP SUCCESSIVO 3 CON IL METODO getAggregatedValue.
			
			IL SUPERSTEP 3 VIENE ESEGUITO SOLAMENTE PER RECUPERARE I VALORI AGGREGATI DAL "GLOBAL_COUNT2", INFATTI IN FONDO AL METODO
			compute() DELLA CLASSE Worker_Superstep2_GAS4 OGNI VERTICE ESEGUE voteToHalt() ED INOLTRE IN FONDO A QUESTO IF VIENE ESEGUITO
			IL METODO haltComputation
			*/
			if (logger.isDebugEnabled()) {
				logger.debug("getting aggregated values of n4local vector calculated by "+Worker_Superstep2_GAS4.class.getSimpleName()+
				" in previous superstep ...   (see log file 09)"+C.N);
			}
			else if (logger.isInfoEnabled()) {
				logger.info("getting aggregated values ..."+C.N2);
			}

			
			
			//##############ww#########################################################
			//con questo metodo il master riceve il valore aggregato che tutti i workers hanno calcolato
			//nel superstep precedente
			GlobalCounts2 gc2 = this.getAggregatedValue(C.GLOBAL_COUNT2);
			
			//#######################################################################
			
			
			if (logger.isDebugEnabled()) logger.debug("execution of "+Master_Superstep3_Global4ProfilesFromLocal.class.getSimpleName()+
					" to calculate global 4 profiles from aggregated local 4-profiles calculated by workers in previous superstep ...   (see log file 10)"+C.N);
			
			// https://giraph.apache.org/giraph-core/apidocs/constant-values.html
			String edgeInputPath = getConf().get("giraph.edge.input.dir");
			logger.info("giraph.edge.input.dir="+edgeInputPath);
			System.out.print("giraph.edge.input.dir="+edgeInputPath);
			
			master_superstep3.calculateGlobal4ProfilesFromLocal(
					gc2, 
					// https://giraph.apache.org/giraph-core/apidocs/constant-values.html
					getConf().get("giraph.edge.input.dir"),					
					this.getTotalNumVertices(), 
					this.getTotalNumEdges());
			
			
			
			
			logger.info (">>> HALT COMPUTATION <<<"+C.N);
			this.haltComputation();

			logger.info("END SUPERSTEP "+this.superstep+" OF MASTER"+C.SN4);
		}


		
	}  

	
	private BigIntegerArrayListWritable calculateIsolatedVertex(BigIntegerArrayListWritable n3Global,
			long ivn, long ne, long nv) {
	BigIntegerArrayListWritable confIsolatedVertexVector = new BigIntegerArrayListWritable();
    
	BigInteger ivnBig = BigInteger.valueOf(ivn);
	BigInteger coeffBin_ivn_2=new BigInteger("0");
	BigInteger coeffBin_ivn_3=new BigInteger("0");
	BigInteger coeffBin_ivn_4=new BigInteger("0");
	BigInteger coeffBinTotalEdges=new BigInteger("0");
	BigInteger noEdges=new BigInteger("0");
	
	ne=ne/2;
	
	coeffBin_ivn_2=ivnBig.multiply((ivnBig).subtract(C.ONE)).divide(C.TWO);

	coeffBin_ivn_3=ivnBig.
						  multiply((ivnBig).subtract(C.ONE)).
						  multiply((ivnBig).subtract(C.TWO)).divide(C.SIX);
	
	coeffBin_ivn_4=ivnBig.
			  multiply((ivnBig).subtract(C.ONE)).
			  multiply((ivnBig).subtract(C.TWO)).
			  multiply((ivnBig).subtract(C.THREE)).divide(C.TWENTYFOUR);

	
	coeffBinTotalEdges=BigInteger.valueOf(nv).multiply((BigInteger.valueOf(nv)).subtract(C.ONE)).divide(C.TWO);
	
	noEdges=coeffBinTotalEdges.subtract(BigInteger.valueOf(ne));
	
		
	//configurazione n.0 4 vertici non connessi tra loro
	confIsolatedVertexVector.add(
			 				(n3Global.get(0).get().multiply(ivnBig)).
					 		add(noEdges.multiply(coeffBin_ivn_2)).
					 		add(BigInteger.valueOf(nv).multiply(coeffBin_ivn_3).add(coeffBin_ivn_4)));
	
	
	//configurazione n.1 - Un arco e un vertice libero
	confIsolatedVertexVector.add(
			 				(n3Global.get(1).get().multiply(ivnBig)).
			 				add(BigInteger.valueOf(ne).multiply(coeffBin_ivn_2)));
	
	//configurazione n.2 - triangolo aperto
	confIsolatedVertexVector.add(n3Global.get(2).get().multiply(ivnBig));
	
	
	//configurazione n.3 - triangolo 
    confIsolatedVertexVector.add(n3Global.get(3).get().multiply(ivnBig));
    
    
    // configurazione n3global per il caso n0 - calcolo solo per i vertici isolati.
    // questo valore sarà aggiunto in fase di stampa al valore di n3global per il caso 0
    // viene fatto questo per non modificare il valore di n3global utilizzato per il calcolo di n4global
    
    confIsolatedVertexVector.add(
			(BigInteger.valueOf(nv).multiply(coeffBin_ivn_2)).
	 		add(noEdges.multiply(ivnBig)).
	 		add(coeffBin_ivn_3));
    
    // configurazione n3global per il caso n1 - calcolo solo per i vertici isolati.
    // questo valore sarà aggiunto in fase di stampa al valore di n3global per il caso 1
    // viene fatto questo per non modificare il valore di n3global utilizzato per il calcolo di n4global
    
    confIsolatedVertexVector.add(
			(BigInteger.valueOf(ne).multiply(ivnBig)));
    
    
    
    System.out.println("\nNumero vertici isolati: "+confIsolatedVertexVector.get(0).get());
    System.out.println("\nNumero archi e vertice libero: "+confIsolatedVertexVector.get(1).get());
    System.out.println("\nNumero triangoli aperti: "+confIsolatedVertexVector.get(2).get());
    System.out.println("\nNumero triangoli chiusi: "+confIsolatedVertexVector.get(3).get());
    System.out.println("\nNumero n3global n0: "+confIsolatedVertexVector.get(4).get());
    System.out.println("\nNumero n3global n1: "+confIsolatedVertexVector.get(5).get());

    
    
	
    return confIsolatedVertexVector;
}	

	/*
	 ### TODO ###
	 verificare se questi metodi effettivamente occorrono
	 
	
	public void readFields(DataInput arg0) throws IOException {
		// To deserialize this class fields (global variables) if any
		logger.debug("MASTER readFields");
		agg1.readFields(arg0);
		agg2.readFields(arg0);
		

	}

	public void write(DataOutput arg0) throws IOException {
		// To serialize this class fields (global variables) if any
		logger.debug("MASTER write");
		agg1.write(arg0);
		agg2.write(arg0);
		
	}
	*/
	

}




