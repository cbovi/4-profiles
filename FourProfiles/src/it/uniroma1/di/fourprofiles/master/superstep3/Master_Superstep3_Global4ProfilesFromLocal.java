package it.uniroma1.di.fourprofiles.master.superstep3;

import java.math.BigInteger;

import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts2;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;

public class Master_Superstep3_Global4ProfilesFromLocal {

	private final static Logger logger = Logger.getLogger(Master_Superstep3_Global4ProfilesFromLocal.class);
	
	public void calculateGlobal4ProfilesFromLocal(GlobalCounts2 gc2, String edgeInputPath, long nv, long ne) {
		
		logger.info("BEGIN AGGREGATED VALUES RECEIVED FROM WORKERS - GLOBAL COUNT 2 (gc2):");
		logger.info("gc2.n4local="+gc2.n4local);
		logger.info("END AGGREGATED VALUES RECEIVED FROM WORKERS - GLOBAL COUNT 2 (gc2)"+C.N4);
		
		/*
		   ### GRAPHLAB ###
		   
		     double n4final_gfroml[11] = {};
		     n4final_gfroml[0] = global_counts2.n4local[0]/4.;
		     n4final_gfroml[1] = global_counts2.n4local[1]/2.;
		     n4final_gfroml[2] = global_counts2.n4local[2]/4.;
		     n4final_gfroml[3] = global_counts2.n4local[4];
		     n4final_gfroml[4] = global_counts2.n4local[6]/2.;
		     n4final_gfroml[5] = global_counts2.n4local[7]/3.;
		     n4final_gfroml[6] = global_counts2.n4local[9];
		     n4final_gfroml[7] = global_counts2.n4local[10]/4.;
		     n4final_gfroml[8] = global_counts2.n4local[11];
		     n4final_gfroml[9] = global_counts2.n4local[14]/2.;
		     n4final_gfroml[10] = global_counts2.n4local[16]/4.;	
	    */

		BigIntegerArrayListWritable n4GlobalFromLocal = new BigIntegerArrayListWritable();
		
		//LongArrayListWritable n4GlobalFromLocal = new LongArrayListWritable();
		
		n4GlobalFromLocal.add(gc2.n4local.get(0).get().divide(C.FOUR));
		n4GlobalFromLocal.add(gc2.n4local.get(1).get().divide(C.TWO));
		n4GlobalFromLocal.add(gc2.n4local.get(2).get().divide(C.FOUR));
		n4GlobalFromLocal.add(gc2.n4local.get(4).get());
		n4GlobalFromLocal.add(gc2.n4local.get(6).get().divide(C.TWO));
		n4GlobalFromLocal.add(gc2.n4local.get(7).get().divide(C.THREE));
		n4GlobalFromLocal.add(gc2.n4local.get(9).get());
		n4GlobalFromLocal.add(gc2.n4local.get(10).get().divide(C.FOUR));
		n4GlobalFromLocal.add(gc2.n4local.get(11).get());
		n4GlobalFromLocal.add(gc2.n4local.get(14).get().divide(C.TWO));
		n4GlobalFromLocal.add(gc2.n4local.get(16).get().divide(C.FOUR));
		
		
	
		/*
		 * ### PAPER ###
		 * pag 487 in alto a destra
		 
		n4GlobalFromLocal.add(gc2.n4local.get(0).get()/4);
		n4GlobalFromLocal.add(gc2.n4local.get(1).get()/2);
		n4GlobalFromLocal.add(gc2.n4local.get(2).get()/4);
		n4GlobalFromLocal.add(gc2.n4local.get(4).get());
		n4GlobalFromLocal.add(gc2.n4local.get(6).get()/2);
		n4GlobalFromLocal.add(gc2.n4local.get(7).get()/3);
		n4GlobalFromLocal.add(gc2.n4local.get(9).get());
		n4GlobalFromLocal.add(gc2.n4local.get(10).get()/4);
		n4GlobalFromLocal.add(gc2.n4local.get(11).get());
		n4GlobalFromLocal.add(gc2.n4local.get(14).get()/2);
		n4GlobalFromLocal.add(gc2.n4local.get(16).get()/4);
		*/
		
				
		Util.logInputGraph(edgeInputPath, nv, ne);
			
		BigInteger numV = BigInteger.valueOf(nv);
		BigInteger numV1 = numV.subtract(C.ONE); 
		BigInteger numV2 = numV.subtract(C.TWO);
		BigInteger numV3 = numV.subtract(C.THREE); 

		BigInteger denom = (numV.multiply(numV1).multiply(numV2).multiply(numV3)).divide(C.TWENTYFOUR);
					
		System.out.println("\n\nBINOMIAL COEFFICIENT (|V| choose 4) : "+denom);

		
		BigInteger sumTotal4Profiles = new BigInteger("0");
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(0).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(1).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(2).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(3).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(4).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(5).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(6).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(7).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(8).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(9).get());
		sumTotal4Profiles=sumTotal4Profiles.add(n4GlobalFromLocal.get(10).get());

		  	  	   
   	    System.out.println("TOTAL SUM OF GLOBAL COUNT 4-PROFILES: "+sumTotal4Profiles);

		
		
		System.out.println("\nGLOBAL COUNT FROM LOCAL 4-PROFILES:");
		logger.info("GLOBAL COUNT FROM LOCAL 4-PROFILES:");
		
		logger.info("@@@@@ 4-PROFILES PAPER - pag. 487 top right @@@@@");
		String n4GlobalFromLocal0 = "n4GlobalFromLocal[0]="+n4GlobalFromLocal.get(0);
		String n4GlobalFromLocal1 = "n4GlobalFromLocal[1]="+n4GlobalFromLocal.get(1);
		String n4GlobalFromLocal2 = "n4GlobalFromLocal[2]="+n4GlobalFromLocal.get(2);
		String n4GlobalFromLocal3 = "n4GlobalFromLocal[3]="+n4GlobalFromLocal.get(3);
		String n4GlobalFromLocal4 = "n4GlobalFromLocal[4]="+n4GlobalFromLocal.get(4);
		String n4GlobalFromLocal5 = "n4GlobalFromLocal[5]="+n4GlobalFromLocal.get(5);
		String n4GlobalFromLocal6 = "n4GlobalFromLocal[6]="+n4GlobalFromLocal.get(6);
		String n4GlobalFromLocal7 = "n4GlobalFromLocal[7]="+n4GlobalFromLocal.get(7);
		String n4GlobalFromLocal8 = "n4GlobalFromLocal[8]="+n4GlobalFromLocal.get(8);
		String n4GlobalFromLocal9 = "n4GlobalFromLocal[9]="+n4GlobalFromLocal.get(9);
		String n4GlobalFromLocal10 = "n4GlobalFromLocal[10]="+n4GlobalFromLocal.get(10);
		
		System.out.println(n4GlobalFromLocal0);
		logger.info(n4GlobalFromLocal0+"   ( n4GlobalFromLocal[0] = gc2.n4local[0]/4 )");
		System.out.println(n4GlobalFromLocal1);
		logger.info(n4GlobalFromLocal1+"   ( n4GlobalFromLocal[1] = gc2.n4local[1]/2 )");
		System.out.println(n4GlobalFromLocal2);
		logger.info(n4GlobalFromLocal2+"   ( n4GlobalFromLocal[2] = gc2.n4local[2]/4 )");
		System.out.println(n4GlobalFromLocal3);
		logger.info(n4GlobalFromLocal3+"   ( n4GlobalFromLocal[3] = gc2.n4local[4] )");
		System.out.println(n4GlobalFromLocal4);
		logger.info(n4GlobalFromLocal4+"   ( n4GlobalFromLocal[4] = gc2.n4local[6]/2 )");
		System.out.println(n4GlobalFromLocal5);
		logger.info(n4GlobalFromLocal5+"   ( n4GlobalFromLocal[5] = gc2.n4local[7]/3 )");
		System.out.println(n4GlobalFromLocal6);
		logger.info(n4GlobalFromLocal6+"   ( n4GlobalFromLocal[6] = gc2.n4local[9] )");
		System.out.println(n4GlobalFromLocal7);
		logger.info(n4GlobalFromLocal7+"   ( n4GlobalFromLocal[7] = gc2.n4local[10]/4 )");
		System.out.println(n4GlobalFromLocal8);
		logger.info(n4GlobalFromLocal8+"   ( n4GlobalFromLocal[8] = gc2.n4local[11] )");
		System.out.println(n4GlobalFromLocal9);
		logger.info(n4GlobalFromLocal9+"   ( n4GlobalFromLocal[9] = gc2.n4local[14]/2 )");
		System.out.println(n4GlobalFromLocal10);
		logger.info(n4GlobalFromLocal10+"   ( n4GlobalFromLocal[10] = gc2.n4local[16]/4 )");
		


		
		
		
		/*
		for (int i=0; i<n4GlobalFromLocal.size(); i++) {
			  String str="n4GlobalFromLocal["+i+"]="+n4GlobalFromLocal.get(i).get();
			  System.out.println(str);
			  logger.debug(str);
		}
		*/
		
		/*
		### OBSOLETE ###
		EasyWriter outFile = new EasyWriter(IMaster_FourProfiles.OUTPUT_DIRECTORY+IMaster_FourProfiles.OUTPUT_GLOBAL_FROM_LOCAL_FILE);
		if (outFile.bad()) {
			System.err.println("Can't create "+IMaster_FourProfiles.OUTPUT_DIRECTORY+IMaster_FourProfiles.OUTPUT_GLOBAL_FILE);
			System.exit(1);
		}
		  
		for (int i=0; i<n4finalgfroml.size(); i++) {
		  String str="n4finalgfroml["+i+"]="+n4finalgfroml.get(i).get();
		  logger.debug(str);
		  
		  if (!outFile.bad()) {
			  outFile.println(str);
		  }
		}
		 
		outFile.close();
		*/
		

	}
	
}
