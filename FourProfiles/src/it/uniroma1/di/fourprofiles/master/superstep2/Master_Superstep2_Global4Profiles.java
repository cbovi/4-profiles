package it.uniroma1.di.fourprofiles.master.superstep2;

import java.math.BigInteger;

import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts1;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;

public class Master_Superstep2_Global4Profiles {

	
	private final static Logger logger = Logger.getLogger(Master_Superstep2_Global4Profiles.class);
	
	
	public void calculate4Profiles(GlobalCounts1 gc1, long nv, BigIntegerArrayListWritable isolatedVertex, long ivn) {

		BigInteger[] toInvert = calculateToInvertVector(gc1, nv, ivn);
		
		BigIntegerArrayListWritable n4Global = calculateN4GlobalVector(toInvert, isolatedVertex);
		  
		logN4GlobalVector(n4Global);
		
	}


	
	private BigInteger[] calculateToInvertVector(GlobalCounts1 gc1, long nv, long ivn) {
		/*
		 ### GRAPHLAB ###
		      uvec toInvert;
		      toInvert[0] = global_counts.u[0];
		      toInvert[1] = global_counts.u[1];
		      toInvert[2] = global_counts.u[4];
		      toInvert[3] = global_counts.u[6];
		      toInvert[4] = global_counts.u[7];
		      toInvert[5] = global_counts.u[8];
		      toInvert[6] = global_counts.u[10];
		      toInvert[7] = global_counts.u[3];
		      toInvert[8] = global_counts.u[11];
		 */
		

		BigInteger[] toInvert = new BigInteger[C.USIZE_11];
		toInvert[0] = gc1.u.get(0).get();
		toInvert[1] = gc1.u.get(1).get();
		toInvert[2] = gc1.u.get(4).get();
		toInvert[3] = gc1.u.get(6).get();
		toInvert[4] = gc1.u.get(7).get();
		toInvert[5] = gc1.u.get(8).get();
		toInvert[6] = gc1.u.get(10).get();
		toInvert[7] = gc1.u.get(3).get();
		toInvert[8] = gc1.u.get(11).get();
		
		
		
		/*
		 ### GRAPHLAB ###
		 toInvert[9] = graph.map_reduce_edges<long>(get_eqn10_const)*4.;
		 
		 ### NOTES ###
		 E' stato aggiunto /2 alla seguente istruzione secondo quanto riportato nel metodo gather della classe eq10_count:
         
         		For the accumulating src, if target < member of list then push (target,member). Avoids double counts,
         		if inequality is other way, this will be done at the other vertex
		 */
		
		
		toInvert[9] = gc1.eqn10const.get().multiply(C.TWO); //moltiplica per 4 e divide per 2 (la divisione è stata
		                                                  //aggiunta nell'implementazione di Giraph)


		/*
		### GRAPHLAB ###
		long n4final[11] = {}; // This is for accumulating global 4-profiles.
		long nv = (long)graph.num_vertices();
		long denom = (nv*(nv-1)*(nv-2)*(nv-3))/24.; //normalize by |V| choose 4
		toInvert[10] = denom;		
		*/
	
					
		long totalV=nv+ivn;
		BigInteger numV = BigInteger.valueOf(totalV);
		BigInteger numV1 = numV.subtract(C.ONE); 
		BigInteger numV2 = numV.subtract(C.TWO);
		BigInteger numV3 = numV.subtract(C.THREE); 

	    BigInteger CB = (numV.multiply(numV1).multiply(numV2).multiply(numV3)).divide(C.TWENTYFOUR);	
					
		numV = BigInteger.valueOf(nv);
		numV1 = numV.subtract(C.ONE); 
		numV2 = numV.subtract(C.TWO);
		numV3 = numV.subtract(C.THREE); 

		BigInteger denom = (numV.multiply(numV1).multiply(numV2).multiply(numV3)).divide(C.TWENTYFOUR);
		//long denom = (nv*(nv-1)*(nv-2)*(nv-3))/24; 
		//toInvert[10] = BigInteger.valueOf(denom);
		
					
		System.out.println("\n\nBINOMIAL COEFFICIENT (|V| choose 4) : "+CB);
		
		toInvert[10] = denom;
		return toInvert;
	}
	
	
	
	private BigIntegerArrayListWritable calculateN4GlobalVector(BigInteger[] toInvert, BigIntegerArrayListWritable isolatedVertex) {
		/*
		  ### GRAPHLAB ###
		 
		  uvec A0 = {{-12,-8,-12,-4,-6,-7,6,-4,6,1,24}};
		  uvec A1 = {{2,0,0,0,2,1,-2,2,-4,-1,0}};
		  uvec A2 = {{0,0,0,0,-2,-1,2,-2,4,1,0}};
		  uvec A3 = {{0,0,1,0,-1,0,0,-2,2,1,0}};
		  uvec A4 = {{0,0,0,0,1,0,0,2,-2,-1,0}};
		  uvec A5 = {{0,0,0,1,0,-1,0,2,0,-1,0}};
		  uvec A6 = {{0,2,0,0,0,-1,0,2,0,-1,0}};
		  uvec A7 = {{0,0,0,0,0,0,0,-2,2,1,0}};
		  uvec A8 = {{0,0,0,0,0,1,0,-2,0,1,0}};
		  uvec A9 = {{0,0,0,0,0,0,0,2,0,-1,0}};
		  uvec A10 = {{0,0,0,0,0,0,0,0,0,1,0}};

		  // Operate Ai's on global_counts.u
		  n4final[0] = (toInvert * A0) / 24.;
		  n4final[1] = (toInvert * A1) / 4.;
		  n4final[2] = (toInvert * A2) / 8.;
		  n4final[3] = (toInvert * A3) / 2.;
		  n4final[4] = (toInvert * A4) / 2.;
		  n4final[5] = (toInvert * A5) / 6.;
		  n4final[6] = (toInvert * A6) / 6.;
		  n4final[7] = (toInvert * A7) / 8.;
		  n4final[8] = (toInvert * A8) / 2.;
		  n4final[9] = (toInvert * A9) / 4.;
		  n4final[10] = (toInvert * A10) / 24.;
		 */
		
		  long A0[] = {-12,-8,-12,-4,-6,-7,6,-4,6,1,24};
		  long A1[] = {2,0,0,0,2,1,-2,2,-4,-1,0};
		  long A2[] = {0,0,0,0,-2,-1,2,-2,4,1,0};
		  long A3[] = {0,0,1,0,-1,0,0,-2,2,1,0};
		  long A4[] = {0,0,0,0,1,0,0,2,-2,-1,0};
		  long A5[] = {0,0,0,1,0,-1,0,2,0,-1,0};
		  long A6[] = {0,2,0,0,0,-1,0,2,0,-1,0};
		  long A7[] = {0,0,0,0,0,0,0,-2,2,1,0};
		  long A8[] = {0,0,0,0,0,1,0,-2,0,1,0};
		  long A9[] = {0,0,0,0,0,0,0,2,0,-1,0};
		  long A10[] = {0,0,0,0,0,0,0,0,0,1,0};

		  BigIntegerArrayListWritable n4Global = new BigIntegerArrayListWritable();
		  
		  n4Global.add( Util.multiply(toInvert,A0).divide(C.TWENTYFOUR));
		  n4Global.add( Util.multiply(toInvert,A1).divide(C.FOUR));
		  n4Global.add( Util.multiply(toInvert,A2).divide(C.EIGHT));
		  n4Global.add( Util.multiply(toInvert,A3).divide(C.TWO));
		  n4Global.add( Util.multiply(toInvert,A4).divide(C.TWO));
		  n4Global.add( Util.multiply(toInvert,A5).divide(C.SIX));
		  n4Global.add( Util.multiply(toInvert,A6).divide(C.SIX));
		  n4Global.add( Util.multiply(toInvert,A7).divide(C.EIGHT));
		  n4Global.add( Util.multiply(toInvert,A8).divide(C.TWO));
		  n4Global.add( Util.multiply(toInvert,A9).divide(C.FOUR));
		  n4Global.add( Util.multiply(toInvert,A10).divide(C.TWENTYFOUR));
		
		  //aggregazione con le configurazioni dei vertici isolati
		  
		  BigInteger conf0=n4Global.get(0).get().add(isolatedVertex.get(0).get());
		  BigInteger conf1=n4Global.get(1).get().add(isolatedVertex.get(1).get());
		  BigInteger conf3=n4Global.get(3).get().add(isolatedVertex.get(2).get());
		  BigInteger conf5=n4Global.get(5).get().add(isolatedVertex.get(3).get());
		  
		  n4Global.set(0, new BigIntegerWritable(conf0));
		  n4Global.set(1, new BigIntegerWritable(conf1));
		  n4Global.set(3, new BigIntegerWritable(conf3));
		  n4Global.set(5, new BigIntegerWritable(conf5));		  
		  
		  BigInteger sumTotal4Profiles = new BigInteger("0");
		  for (int i=0;i<=10;i++) {
			  sumTotal4Profiles=sumTotal4Profiles.add(n4Global.get(i).get());
			  
		  }
		  	  	   
		  System.out.println("TOTAL SUM OF GLOBAL COUNT 4-PROFILES: "+sumTotal4Profiles);
		  
		  return n4Global;
	}



	private void logN4GlobalVector(BigIntegerArrayListWritable n4Global) {
		  
		  System.out.println("\nGLOBAL COUNT OF 4-PROFILES:");
		  logger.info("GLOBAL COUNT OF 4-PROFILES:");
		  for (int i=0; i<n4Global.size(); i++) {
			  String str = "n4Global["+i+"]="+n4Global.get(i).get();
			  System.out.println(str);
			  logger.info(str);
		  }
		  
		  
		  /*
		  ### OBSOLETE ###
		  
		  EasyWriter outFile = new EasyWriter(IMaster_FourProfiles.OUTPUT_DIRECTORY+IMaster_FourProfiles.OUTPUT_GLOBAL_FILE);
		  if (outFile.bad()) {
		      logger.error("Can't create "+IMaster_FourProfiles.OUTPUT_GLOBAL_FILE);
		      System.exit(1);
		  }
		  
		  
		  for (int i=0; i<n4final.size(); i++) {
			  String str = "n4final["+i+"]="+n4final.get(i).get();
			  logger.debug(str);
			  
			  if (!outFile.bad()) {
				  outFile.println(str);
			  }
		  }
		  
		  outFile.close();
		  */		  
	}
	

}
