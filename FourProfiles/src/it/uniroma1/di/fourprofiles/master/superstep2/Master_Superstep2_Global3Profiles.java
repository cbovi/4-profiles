package it.uniroma1.di.fourprofiles.master.superstep2;

import java.math.BigInteger;

import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.global.GlobalCounts1;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.util.C;
import it.uniroma1.di.fourprofiles.util.Util;

public class Master_Superstep2_Global3Profiles {

	public final static Logger logger = Logger.getLogger(Master_Superstep2_Global3Profiles.class);
	
	

	public BigIntegerArrayListWritable calculateGlobal3Profiles(GlobalCounts1 gc1, String edgeInputPath, long nv, long ne, long ivn) {
		
		logAggregatedValues(gc1);
		
		BigIntegerArrayListWritable n3Global = calculateN3Global(gc1);
		
		
		
		//BigIntegerArrayListWritable isolatedVertexConf = calculateIsolatedVertex(n3Global, ivn, ne, nv);

		
		double averageClusteringCoeff = calculateAverageClusteringCoeff(gc1, nv);
		
		double globalClusteringCoeff = calculateGlobalClusteringCoeff(gc1);
		
		
		
		logN3GlobalVector(edgeInputPath, nv, ne, n3Global, averageClusteringCoeff, globalClusteringCoeff, ivn);
		


		/*
		 ### OBSOLETE ###
		 // in Java Math.pow 
		 n3est[0] = n[3]/pow(sample_prob_keep, 3);
		 n3est[1] = n[2]/pow(sample_prob_keep, 2) - 3*(1-sample_prob_keep)*(n[3])/pow(sample_prob_keep, 3);
		 n3est[2] = n[1]/sample_prob_keep - 2*(1-sample_prob_keep)*n[2]/pow(sample_prob_keep, 2) + 3*pow(1-sample_prob_keep,2)*n[3]/pow(sample_prob_keep, 3);
		 n3est[3] = n[0]-(1-sample_prob_keep)*n[1]/sample_prob_keep + pow(1-sample_prob_keep,2)*n[2]/pow(sample_prob_keep, 2) - pow(1-sample_prob_keep,3)*n[3]/pow(sample_prob_keep, 3);
		 */
		/*
		n3est.add(new LongWritable(n.get(3).get()/Math.pow(sample_prob_keep, 3)));
		n3est.add(new LongWritable(
				n.get(2).get() / Math.pow(sample_prob_keep, 2) - 3*(1-sample_prob_keep) * n.get(3).get() / Math.pow(sample_prob_keep, 3)
				));
		n3est.add(new LongWritable(
				n.get(1).get() / sample_prob_keep - 2*(1-sample_prob_keep) * n.get(2).get() / Math.pow(sample_prob_keep, 2) + 3*Math.pow(1-sample_prob_keep, 2) * n.get(3).get() / Math.pow(sample_prob_keep, 3) 
				));
		n3est.add(new LongWritable(
				n.get(0).get() - (1-sample_prob_keep) * n.get(1).get() / sample_prob_keep + Math.pow(1-sample_prob_keep, 2) * n.get(2).get() / Math.pow(sample_prob_keep, 2) - Math.pow(1-sample_prob_keep, 3) * n.get(3).get() / Math.pow(sample_prob_keep,  3)
				));
		
		logger.debug("n3est[0]="+n3est.get(0).get());
		logger.debug("n3est[1]="+n3est.get(1).get());
		logger.debug("n3est[2]="+n3est.get(2).get());
		logger.debug("n3est[3]="+n3est.get(3).get());
		*/
		
		return n3Global;
	}


	private void logAggregatedValues(GlobalCounts1 gc1) {
		logger.info("BEGIN AGGREGATED VALUES RECEIVED FROM WORKERS - GLOBAL COUNT 1 (gc1):"+C.N);
		logger.info("gc1.num_disc="+gc1.num_disc);
		logger.info("gc1.num_empty="+gc1.num_empty);
		logger.info("gc1.num_triangles="+gc1.num_triangles);
		logger.info("gc1.num_wedges_c="+gc1.num_wedges_c);
		logger.info("gc1.num_wedges_e="+gc1.num_wedges_e);
		logger.info("gc1.num_triple="+gc1.average_num_triple);
		logger.info("gc1.u="+gc1.u);
		logger.info("gc1.eqn10const="+gc1.eqn10const+C.N);
		logger.info("END AGGREGATED VALUES RECEIVED FROM WORKERS - GLOBAL COUNT 1"+C.N4);
	} 
	
	


	private BigIntegerArrayListWritable calculateN3Global(GlobalCounts1 gc1) {
		BigIntegerArrayListWritable n3Global = new BigIntegerArrayListWritable();
	    
		/*
		 ### GRAPHLAB ###
		n[0] = global_counts.num_empty/3;
		n[1] = global_counts.num_disc/3;
		n[2] = (global_counts.num_wedges_c + global_counts.num_wedges_e)/3;
		n[3] = global_counts.num_triangles/3;
		*/
		
		// ### TESI ### paragrafo "Passaggio dal local 3-profile al global 3-profile"
		n3Global.add(new BigIntegerWritable(gc1.num_empty.get().divide(C.THREE)));
		n3Global.add(new BigIntegerWritable(gc1.num_disc.get().divide(C.THREE)));
		n3Global.add(new BigIntegerWritable((gc1.num_wedges_c.get().add(gc1.num_wedges_e.get())).divide(C.THREE)));
		n3Global.add(new BigIntegerWritable(gc1.num_triangles.get().divide(C.THREE)));
		return n3Global;
	}

/*
	private BigIntegerArrayListWritable calculateIsolatedVertex(BigIntegerArrayListWritable n3Global,
			long ivn, long ne, long nv) {
	BigIntegerArrayListWritable confIsolatedVertexVector = new BigIntegerArrayListWritable();
    
	BigInteger ivnBig = BigInteger.valueOf(ivn);
	BigInteger coeffBin_ivn_2=new BigInteger("0");
	BigInteger coeffBin_ivn_3=new BigInteger("0");
	BigInteger coeffBin_ivn_4=new BigInteger("0");
	BigInteger coeffBinTotalEdges=new BigInteger("0");
	BigInteger noEdges=new BigInteger("0");
	
	ne=ne/2;
	
	coeffBin_ivn_2=ivnBig.multiply((ivnBig).subtract(C.ONE)).divide(C.TWO);

	coeffBin_ivn_3=ivnBig.
						  multiply((ivnBig).subtract(C.ONE)).
						  multiply((ivnBig).subtract(C.TWO)).divide(C.SIX);
	
	coeffBin_ivn_4=ivnBig.
			  multiply((ivnBig).subtract(C.ONE)).
			  multiply((ivnBig).subtract(C.TWO)).
			  multiply((ivnBig).subtract(C.THREE)).divide(C.TWENTYFOUR);

	
	coeffBinTotalEdges=BigInteger.valueOf(ne).multiply((BigInteger.valueOf(ne)).subtract(C.ONE)).divide(C.TWO);
	
	noEdges=coeffBinTotalEdges.subtract(BigInteger.valueOf(ne));
	
	//configurazione n.0 4 vertici non connessi tra loro
	confIsolatedVertexVector.add(
			 				(n3Global.get(0).get().multiply(ivnBig)).
					 		add(noEdges.multiply(coeffBin_ivn_2)).
					 		add(BigInteger.valueOf(nv).multiply(coeffBin_ivn_3).add(coeffBin_ivn_4)));
	
	
	//configurazione n.1 - Un arco e un vertice libero
	confIsolatedVertexVector.add(
			 				(n3Global.get(1).get().multiply(ivnBig)).
			 				add(BigInteger.valueOf(ne).multiply(coeffBin_ivn_2)));
	
	//configurazione n.2 - triangolo aperto
	confIsolatedVertexVector.add(n3Global.get(2).get().multiply(ivnBig));
	
	
	//configurazione n.3 - triangolo 
    confIsolatedVertexVector.add(n3Global.get(3).get().multiply(ivnBig));
	
    /*
    System.out.println("\nVertici isolati: "+ivnBig);
    
    System.out.println("\nNumero vertici isolati: "+confIsolatedVertexVector.get(0).get());
    System.out.println("\nNumero archi e vertice libero: "+confIsolatedVertexVector.get(1).get());
    System.out.println("\nNumero triangoli aperti: "+confIsolatedVertexVector.get(2).get());
    System.out.println("\nNumero triangoli chiusi: "+confIsolatedVertexVector.get(3).get());
    */
/*	
    return confIsolatedVertexVector;
}	
*/
	
	
	/*
1)
http://pages.di.unipi.it/ricci/19-03-10-NetworkAnalysis.pdf
pag. 26

una tripla in v è un sottografo di esattamente tre vertici e due archi, dove i due archi incidono in v

2)
http://pages.di.unipi.it/ricci/20-03-2007-SmallWorlds.pdf
pag.37

Coefficiente di clusterizzazione di un  grafo
C= (3*numero di triangoli nella rete) / numero di triple connesse
fattore 3 misura il fatto che ogni triangolo contribuisce a 3 triple connesse
	 */
	private double calculateAverageClusteringCoeff(GlobalCounts1 gc1, long nv) {
		
		BigInteger n3 = (gc1.num_triangles.get().divide(C.THREE));
		double triple_connesse = gc1.average_num_triple.get();
				
		//CHECK
		//System.out.println("\nNumero Triangoli: "+n3.doubleValue());
		//System.out.println("Numero Triple Connesse: "+triple_connesse);

		double averageClusteringCoeff = triple_connesse/(double)nv;
		
		return averageClusteringCoeff;
	
	}
	
	
	private double calculateGlobalClusteringCoeff(GlobalCounts1 gc1) {
		
		BigInteger n3 = (gc1.num_triangles.get().divide(C.THREE));
		
		double globalClusteringCoeff = 
				C.THREE.multiply(n3).doubleValue()/
				gc1.global_num_triple.get().doubleValue();
		
		/*
		double global1ClusteringCoeff = 
				U.THREE.multiply(n3).doubleValue()/
				(gc1.num_wedges_c.get().doubleValue()+U.THREE.multiply(n3).doubleValue());
		*/
		
		return globalClusteringCoeff;
	}
	
	private void logN3GlobalVector(
			String edgeInputPath,
			long nv, 
			long ne, 
			BigIntegerArrayListWritable n3Global, 
			double averageClusterCoeff,
			double globalClusterCoeff,
			long ivn) {
		
		Util.logInputGraph(edgeInputPath, nv, ne);
		
		System.out.println("\nAVERAGE CLUSTERING COEFFICIENT:");
		logger.info("AVERAGE CLUSTERING COEFFICIENT:");
		System.out.println(averageClusterCoeff);
		logger.info(averageClusterCoeff);
		
		System.out.println("\nGLOBAL CLUSTERING COEFFICIENT:");
		logger.info("GLOBAL CLUSTERING COEFFICIENT:");
		System.out.println(globalClusterCoeff);
		logger.info(globalClusterCoeff);		
		
		System.out.println("\n\nGLOBAL COUNT OF 3-PROFILES:");
		logger.info("GLOBAL COUNT OF 3-PROFILES:");
		
		if (ivn==0) {
		
					String n3Global0 = "n3Global[0]="+n3Global.get(0).get();
					String n3Global1 = "n3Global[1]="+n3Global.get(1).get();
					String n3Global2 = "n3Global[2]="+n3Global.get(2).get();
					String n3Global3 = "n3Global[3]="+n3Global.get(3).get();
			
					System.out.println(n3Global0);
					logger.info(n3Global0+"   ( gc1.num_empty/3 )");
					System.out.println(n3Global1);
					logger.info(n3Global1+"   ( gc1.num_disc/3 )");
					System.out.println(n3Global2);
					logger.info(n3Global2+"   ( (gc1.num_wedges_c+gc1.num_wedges_e)/3 )");
					System.out.println(n3Global3);
					logger.info(n3Global3+"   ( gc1.num_triangles/3 )"+C.N2);
		}			
		/*
		for (int i=0; i<n3Global.size(); i++) {
			String str = "n3Global["+i+"]="+n3Global.get(i).get();
			System.out.println(str);
			String n= (i==n3Global.size()-1)?U.N2:U.E;
			logger.debug(str + n);
		}
		*/
	}
	

}
