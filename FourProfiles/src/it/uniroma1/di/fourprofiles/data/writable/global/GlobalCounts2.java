package it.uniroma1.di.fourprofiles.data.writable.global;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.util.C;

public class GlobalCounts2 implements Writable{

	public BigIntegerArrayListWritable n4local;
	
	
	public GlobalCounts2() {
		n4local = new BigIntegerArrayListWritable();
		for (int i=0;i<C.USIZE_20;i++) {
			n4local.add(new BigIntegerWritable());
		}
	
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		n4local.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		n4local.readFields(in);
	}
	

}
