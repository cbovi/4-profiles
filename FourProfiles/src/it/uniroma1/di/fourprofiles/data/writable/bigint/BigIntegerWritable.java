package it.uniroma1.di.fourprofiles.data.writable.bigint;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.math.BigInteger;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.master.superstep2.Master_Superstep2_Global3Profiles;
import it.uniroma1.di.fourprofiles.util.Util;


public class BigIntegerWritable implements WritableComparable<BigIntegerWritable> {

	public final static Logger logger = Logger.getLogger(BigIntegerWritable.class);
	
    private BigInteger value;

    public BigIntegerWritable() {
        value = BigInteger.ZERO;
    }

    public BigIntegerWritable(BigInteger bi) {
        value = bi;
    }
    
    public BigIntegerWritable(long bi) {
        value = new BigInteger(String.valueOf(bi));
    }

    public void set(BigInteger value) {
        this.value = value;
    }

    public BigInteger get() {
        return value;
    }

    @Override
    public int compareTo(BigIntegerWritable o) {
        return value.compareTo(o.get());
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BigIntegerWritable)) {
            return false;
        }
        return compareTo((BigIntegerWritable)o) == 0;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    
    @Override
    public void write(DataOutput out) throws IOException {

// 1° SOLUZIONE    	
    	//new Text(value.toString()).write(out);

    	
// 2° SOLUZIONE OTTIMIZZATA    	
    	String str = value.toString();
    	int l = Text.utf8Length(str);
    	
    	/*
    	if (l>Text.DEFAULT_MAX_LEN) {
    		logger.info("l="+l+" - value="+str+" - Text.DEFAULT_MAX_LEN="+Text.DEFAULT_MAX_LEN);
    	}
    	*/
    	
    	out.writeInt(l);
    	// result always contains the lenght of value.toString()
    	int result = Text.writeString(out, value.toString(), l);
    	
    	
    	/*
    	if (str.startsWith("-")) {
    		
    		try {
    			throw new RuntimeException();
    		}
    		catch (Throwable t) {
    			
    			String s = U.stackTraceToString(t);
    			
    			if (!(s.indexOf("calculateEdgeValuesForLocal3Profiles") != -1 && str.equals("-1"))) {
    				logger.info("WRITE result="+result+" - l="+l+" - value="+value.toString());
    				logger.info(s);
    			}
    			
    			
    		}    	
    	
    	}
    	*/
    	
    	
    	
// 3° SOLUZIONE presa dal progetto pig che però non funziona con outOfCorePartitions=true e/o outOfCoreMessages=true    	
    	//writeChararray(out, value.toString());
    	
    	//pig.apache.org
        //bis.writeDatum(out, value, BIGINTEGER);
    }
    
    
    
    @Override
    public void readFields(DataInput in) throws IOException {
// 1° SOLUZIONE    	
    	/*
    	Text t = new Text();
    	t.readFields(in);
    	value = new BigInteger(t.toString());
    	*/

    	
// 2° SOLUZIONE
    	int l = in.readInt();
    	String str = Text.readString(in, l);
    	value = new BigInteger(str);
    	
    	/*
    	if (str.startsWith("-") || value.toString().startsWith("-")) {
    		
    		try {
    			throw new RuntimeException();
    		}
    		catch (Throwable t) {
    			String s = U.stackTraceToString(t);
    			
    			if (!(s.indexOf("calculateEdgeValuesForLocal3Profiles") != -1 && str.equals("-1"))) {
    				logger.info("READFIELDS l="+l+" - value="+value+" - str="+str);
    				logger.info(s);
    			}
    		}
    	}
    	*/
    	
    	
    	
    	
    	
// 3° SOLUZIONE    	
    	//String s = null;
    	//try {
    		//s = readChararray(in);
    		//value = new BigInteger( s );
    	/*}
    	catch (Throwable nfe) {
    		
    		//System.out.println("\n\n\n\n\nERROR ***"+s+"***\n");
    		//System.out.println(U.stackTraceToString(nfe));
    		//System.out.println("\n\n\n\n\n\n");
    		
    		value = BigInteger.ZERO;
    	}*/
    	
    	//pig.apache.org
        //value = (BigInteger)bis.readDatum(in);
    }

    
    
    
    //TODO verificare la correttezza del seguente Comparator in seguito all'adozione di Text
    
    /** A Comparator optimized for BigIntegerWritable. */
    //TODO consider trying to do something a big more optimizied
    public static class Comparator extends WritableComparator {
        private BigIntegerWritable thisValue = new BigIntegerWritable();
        private BigIntegerWritable thatValue = new BigIntegerWritable();

        public Comparator() {
            super(BigIntegerWritable.class);
        }

        @Override
        public int compare(byte[] b1, int s1, int l1,
                           byte[] b2, int s2, int l2) {
            try {
                thisValue.readFields(new DataInputStream(new ByteArrayInputStream(b1,s1,l1)));
                thatValue.readFields(new DataInputStream(new ByteArrayInputStream(b2,s2,l2)));
            } catch (IOException e) {
                throw new RuntimeException("Unable to read field from byte array: " + e);
            }
            return thisValue.compareTo(thatValue);
        }
    }

    // register this comparator
    static {
        WritableComparator.define(BigIntegerWritable.class, new Comparator());
    }
    
    
    
/* Istruzioni non più necessarie da quanto è stato utilizzato Text
  
   Istruzioni prese dalle seguenti classi del progetto pig.apache.org:
   - pig-0.17.0-src/src/org/apache/pig/backend/hadoop/BigIntegerWritable.java
   - pig-0.17.0-src/src/org/apache/pig/data/utils/SedesHelper.java <- qui si trovano i metodi writeChararray e readChararray
   - pig-0.17.0-src/src/org/apache/pig/data/BinInterSedes.java
   
  
    public static final int UNSIGNED_SHORT_MAX = 65535;

    public static final String UTF8 = "UTF-8";
    
    public static final byte CHARARRAY = 14;
    public static final byte SMALLCHARARRAY = 15;
    
    public static void writeChararray(DataOutput out, String s) throws IOException {
        // a char can take up to 3 bytes in the modified utf8 encoding
        // used by DataOutput.writeUTF, so use UNSIGNED_SHORT_MAX/3
        byte[] utfBytes = s.getBytes(UTF8);
        int length = utfBytes.length;
        if (length < UNSIGNED_SHORT_MAX) {
            out.writeByte(SMALLCHARARRAY);
            out.writeShort(length);
        } else {
            out.writeByte(CHARARRAY);
            out.writeInt(length);
        }
        out.write(utfBytes);
    }


    public static String readChararray(DataInput in) throws IOException {
    	
    	byte type = in.readByte();
    	
        int size;
        if (type == SMALLCHARARRAY) {
            size = in.readUnsignedShort();
        } else {
            size = in.readInt();
        }
        byte[] buf = new byte[size];
        in.readFully(buf);
        return new String(buf, UTF8);
    }
*/    
}
