package it.uniroma1.di.fourprofiles.data.writable.global;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;
import it.uniroma1.di.fourprofiles.util.C;

public class GlobalCounts1 implements Writable {

	
	public BigIntegerWritable num_empty;
	public BigIntegerWritable num_disc;
	public BigIntegerWritable num_wedges_c;
	public BigIntegerWritable num_wedges_e;
	public BigIntegerWritable num_triangles;
	public DoubleWritable average_num_triple;
	public BigIntegerWritable global_num_triple;
	public BigIntegerArrayListWritable u;
    public BigIntegerWritable eqn10const;
    
    
    
    public GlobalCounts1() {
    	
    	/* ### PAPER ###
    	   Figura 3 (a) pag. 485 
    	 */
		num_empty = new BigIntegerWritable();
		num_disc = new BigIntegerWritable();
		num_wedges_c = new BigIntegerWritable();
		num_wedges_e = new BigIntegerWritable();
		num_triangles = new BigIntegerWritable();
		average_num_triple = new DoubleWritable();
		global_num_triple = new BigIntegerWritable();
				
		u=new BigIntegerArrayListWritable();
		
		
		/* ### PAPER ###
		   Figure 3 (b) pag. 485 e 1 (b) pag. 484
			  for (int i=0;i<USIZE;i++) {
			    vertex.data().u[i]=0;
			  }
			  
		   ### NOTES ###
		   Il vettore u deve essere inizializzato a 0 in modo da consentire l'aggregazione nel metodo global_counts1Aggregator.aggregate
		*/
		for (int i=0;i<C.USIZE_12;i++) {
			u.add(new BigIntegerWritable());
		}
		
		eqn10const = new BigIntegerWritable();
		
		
	}
    
    
	@Override
	public void write(DataOutput out) throws IOException {
		num_empty.write(out);
		num_disc.write(out);
        num_wedges_c.write(out);
        num_wedges_e.write(out);
        num_triangles.write(out);
        average_num_triple.write(out);
        global_num_triple.write(out);
        eqn10const.write(out);
        u.write(out);        
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		
		num_empty.readFields(in);
		num_disc.readFields(in);
        num_wedges_c.readFields(in);
        num_wedges_e.readFields(in);
        num_triangles.readFields(in);
        average_num_triple.readFields(in);
        global_num_triple.readFields(in);
        eqn10const.readFields(in);
		u.readFields(in);
	}


	

}
