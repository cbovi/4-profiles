package it.uniroma1.di.fourprofiles.data.writable.bigint;

import java.math.BigInteger;

import org.apache.hadoop.io.Text;

public class BigIntegerTest {

	public static void main(String[] args) {
		
		BigInteger bi = new BigInteger("123456789012345678901234567890");
		System.out.println(bi.toString());
		Text t = new Text(bi.toString());
		System.out.println(t.toString());
		
		
		System.out.println("ZERO="+BigInteger.ZERO);
		
	
	}
}
