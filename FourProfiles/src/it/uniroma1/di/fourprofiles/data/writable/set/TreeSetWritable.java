package it.uniroma1.di.fourprofiles.data.writable.set;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.util.ReflectionUtils;

/**
 * A Writable for ListArray containing instances of a class.
 *
 * @param <M> Message data
 */
public abstract class TreeSetWritable<M extends Writable> extends TreeSet<M>
  implements Writable, Configurable {
  /** Defining a layout version for a serializable class. */
  private static final long serialVersionUID = 1L;
  /** Used for instantiation */
  private Class<M> refClass = null;

  /** Configuration */
  private Configuration conf;

  /**
   * Using the default constructor requires that the user implement
   * setClass(), guaranteed to be invoked prior to instantiation in
   * readFields()
   */
  public TreeSetWritable() {
  }

  /**
   * Constructor with another {@link BigListWritable}.
   *
   * @param arrayListWritable Array list to be used internally.
   */
  public TreeSetWritable(TreeSetWritable<M> arrayListWritable) {
    super(arrayListWritable);
  }

  /**
   * This constructor allows setting the refClass during construction.
   *
   * @param refClass internal type class
   */
  public TreeSetWritable(Class<M> refClass) {
    super();
    this.refClass = refClass;
  }

  /**
   * This is a one-time operation to set the class type
   *
   * @param refClass internal type class
   */
  public void setClass(Class<M> refClass) {
    if (this.refClass != null) {
      throw new RuntimeException(
          "setClass: refClass is already set to " +
              this.refClass.getName());
    }
    this.refClass = refClass;
  }

  /**
   * Subclasses must set the class type appropriately and can use
   * setClass(Class<M> refClass) to do it.
   */
  public abstract void setClass();

  @Override
  public void readFields(DataInput in) throws IOException {
    if (this.refClass == null) {
      setClass();
    }

    clear();                              // clear list before storing values
    long numValues = in.readLong();            // read number of values
    
    //TODO check if exist a similar ensureCapacity method for HashSet
    //ensureCapacity(numValues);
    for (int i = 0; i < numValues; i++) {
      M value = ReflectionUtils.newInstance(refClass, conf);
      value.readFields(in);                // read a value
      add(value);                          // store it in values
    }
  }

  @Override
  public void write(DataOutput out) throws IOException {
    long numValues = size();
    out.writeLong(numValues);                 // write number of values
    
    Iterator<M> iter = this.iterator();
    while (iter.hasNext()) {
    	iter.next().write(out);
    }
    
    /*
    for (int i = 0; i < numValues; i++) {
      get(i).write(out);
    }
    */
  }

  @Override
  public final Configuration getConf() {
    return conf;
  }

  @Override
  public final void setConf(Configuration conf) {
    this.conf = conf;
  }
}

