package it.uniroma1.di.fourprofiles.data.writable.set;

import org.apache.hadoop.io.IntWritable;


public class IntHashSetWritable extends /*TreeSet*/HashSetWritable<IntWritable> {


  private static final long serialVersionUID = -5384314207777296208L;
 
 
  /** Default constructor for reflection */
  public IntHashSetWritable() {
    super();
  }
 

  public IntHashSetWritable(/*TreeSet*/HashSetWritable<IntWritable> a) {
	    super(a);
  }

  
  /** Set storage type for this ArrayListWritable */
  @Override
  @SuppressWarnings("unchecked")
  public void setClass() {
    setClass(IntWritable.class);
  }
 

}
