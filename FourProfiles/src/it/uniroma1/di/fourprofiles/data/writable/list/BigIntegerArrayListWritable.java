package it.uniroma1.di.fourprofiles.data.writable.list;

import java.math.BigInteger;

import org.apache.giraph.utils.ArrayListWritable;
import org.apache.hadoop.io.LongWritable;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;


public class BigIntegerArrayListWritable extends ArrayListWritable<BigIntegerWritable> {


 private static final long serialVersionUID = -5384314207777296208L;
 
 
/** Default constructor for reflection */
 public BigIntegerArrayListWritable() {
   super();
 }
 /** Set storage type for this ArrayListWritable */
 @Override
 @SuppressWarnings("unchecked")
 public void setClass() {
   setClass(BigIntegerWritable.class);
 }
 

 public boolean add(long d) {
	return super.add(
			new BigIntegerWritable(
					BigInteger.valueOf(d)));
 }
 
 public boolean add(BigInteger d) {
		return super.add(
				new BigIntegerWritable(d));
	 }

 /*
 public BigIntegerWritable set(int index, long d) {
	 return super.set(index, new BigIntegerWritable(BigInteger.valueOf(d)));
 }
 */
 
 public BigIntegerWritable get(int index) {
	 return super.get(index);
 }

}
