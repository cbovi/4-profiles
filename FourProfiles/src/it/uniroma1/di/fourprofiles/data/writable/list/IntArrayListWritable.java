package it.uniroma1.di.fourprofiles.data.writable.list;

import org.apache.giraph.utils.ArrayListWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;


public class IntArrayListWritable extends /*Array*//*BigList*//*HashSet*/ArrayListWritable<IntWritable> {


 private static final long serialVersionUID = -5384314207777296208L;
 
 
/** Default constructor for reflection */
 public IntArrayListWritable() {
   super();
 }
 /** Set storage type for this ArrayListWritable */
 @Override
 @SuppressWarnings("unchecked")
 public void setClass() {
   setClass(IntWritable.class);
 }
 
 
 public boolean add(int d) {
	return super.add(new IntWritable(d));
 }
 
 public IntWritable set(int i, int d) {
	 return super.set(i, new IntWritable(d));
 }
 
 public IntWritable get(int i) {
	 return super.get(i);
 }

}
