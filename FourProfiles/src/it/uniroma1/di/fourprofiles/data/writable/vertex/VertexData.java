package it.uniroma1.di.fourprofiles.data.writable.vertex;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Writable;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.list.BigIntegerArrayListWritable;


/**
 * 
 * 
 * 
 * ### NOTES ##
 * - https://stackoverflow.com/questions/7994438/how-to-customize-writable-class-in-hadoop
 * 
 */
public class VertexData implements Writable {
	

	
	/**
	 * Scalars used to calculte global 3-profiles in Master_Superstep2_Global3Profiles
	 * 
	 * 
	 * ### NOTES ###
	 * These values are saved on this class instead of VertexDataMem class only because they must be print by output giraph
	 * that by default uses toString() method 
	 */
	public BigIntegerWritable num_triangles;
	public BigIntegerWritable num_wedges_e;
	public BigIntegerWritable num_wedges_c;
	public BigIntegerWritable num_disc;
	public BigIntegerWritable num_empty;
	public BigIntegerWritable num_disc_e;
	public DoubleWritable average_num_triple;
	public BigIntegerWritable global_num_triple;
	
	//MODIFICA_VICINATO  
	//public String sourceNeighborhoodStr;
	
	/**
	 * Source neighborhood
	 * 
	 * ### GRAPHLAB ###
	 * The corresponding implementation in Graphlab is the struct vid_set having vid_vec member.
	 * vid_vec is ordered by assign method of struct vid_set, in java the ordering is not executed because it is directly used an unordered hashset.
	 * (anyway to obtain a ordered hashset in Java could be used TreeSet but obviously it has a greater cost of execution)
	 */
	//MODIFICA_VICINATO
	//public LongHashSetWritable sourceNeighborhood;
	
	
	
	
	/**
	   ### NOTES ###
	   Nella classe Worker_Superstep1_GAS2 vengono calcolati per ogni vertice i primi 12 elementi del vettore u.
	   Nel calcolo GLOBALE dei 4 profiles questi 12 elementi verranno aggregati da global_counts1Aggregator e alcuni di questi verranno
	   infine utilizzati in Master_Superstep2_Global4Profiles per calcolare il valore GLOBALE dei 4 profiles.
	   Nel calcolo LOCALE dei 4 profiles a questi 12 elementi verranno aggiunti altri 5 elementi nella classe classe Worker_Superstep2_GAS4
	   e infine il vettore u verrà utilizzato per calcolare il vettore n4local per ogni vertice. Infine per calcolare il valore GLOBALE dei
	   4 profiles a partire dal valore LOCALE, il vettore n4local verrà aggregato tramite il global_counts2Aggregator e il valore aggregato
	   verrà utilizzato nella classe Master_Superstep3_Global4ProfilesFromLocal per ottenere il valore GLOBALE dal LOCALE dei 4-profiles.
	   
	   Poiché i primi 12 elementi di u vengono calcolati in Worker_Superstep1_GAS2 e i restanti 5 elementi di u vengono calcolati in
	   Worker_Superstep2_GAS4, è necessario salvare il vettore u sul vertice.
	 */
	public BigIntegerArrayListWritable u;
	
	/**
	 * Local 4-Profiles.
	 * 
	 * E' necessario salvare il vettore n4local nel vertice perché dovrà essere stampato da giraph dentro la cartella output
	 */
	public BigIntegerArrayListWritable n4local;

	
	

	public VertexData() {
		
		// TODO: verificare il motivo per cui senza queste inizializzazioni si verifica un NullPointerException
		num_triangles = new BigIntegerWritable();
		num_wedges_e = new BigIntegerWritable();
		num_wedges_c = new BigIntegerWritable();
		num_disc = new BigIntegerWritable();
		num_empty = new BigIntegerWritable();
		num_disc_e=new BigIntegerWritable();
		average_num_triple=new DoubleWritable();
		global_num_triple=new BigIntegerWritable();
		//MODIFICA_VICINATO
		//sourceNeighborhoodStr="";
		//sourceNeighborhood =new LongHashSetWritable();
		
		u = new BigIntegerArrayListWritable();
		n4local = new BigIntegerArrayListWritable();
		
		
	}
	
	
	
	
	
	@Override
	public void write(DataOutput out) throws IOException {

		num_triangles.write(out);
		num_wedges_e.write(out);
		num_wedges_c.write(out);
		num_disc.write(out);
		num_empty.write(out);
		num_disc_e.write(out);
		average_num_triple.write(out);
		global_num_triple.write(out);		
		
		//MODIFICA_VICINATO
		//sourceNeighborhoodStr.write(out);
		//sourceNeighborhood.write(out);
		
		u.write(out);
		n4local.write(out);

	}


	@Override
	public void readFields(DataInput in) throws IOException {
		
		num_triangles.readFields(in);
		num_wedges_e.readFields(in);
		num_wedges_c.readFields(in);
		num_disc.readFields(in);
		num_empty.readFields(in);
		num_disc_e.readFields(in);
		average_num_triple.readFields(in);
		global_num_triple.readFields(in);
		
		//MODIFICA_VICINATO
		//sourceNeighborhoodStr.readFields(in);
		//sourceNeighborhood.readFields(in);
		
		u.readFields(in);
		n4local.readFields(in);
	}


	
	private static final String TAB = "\t";
	

	/* ### GRAPHLAB ###
	// A saver which saves a file where each line is a vid / # triangles pair
	struct save_profile_count{
	  std::string save_vertex(graph_type::vertex_type v) {
	
	    std::string str = graphlab::tostr(v.id());
	    str += "\t" + graphlab::tostr(v.data().num_empty) + "\t" +
	           graphlab::tostr(v.data().num_disc) + "\t" +
	           graphlab::tostr(v.data().num_wedges_c) + "\t" +
	           graphlab::tostr(v.data().num_wedges_e) + "\t"Long +
	           graphlab::tostr(v.data().num_triangles);
	    for (int i=0; i<USIZE; i++){
	      str += "\t" + graphlab::tostr(v.data().n4local[i]);
	      // str += "\t" + graphlab::tostr(v.data().u[i]);
	    }
	    str += "\n";
	    return str;
	  }
	
	  std::string save_edge(graph_type::edge_type e) {
	    return "";
	  }
	};
	 */	
	/**
	 * This method is used by giraph output
	 */
	@Override
	public String toString() {
		
		// @@@@@ 4-PROFILES PAPER - pag. 485 - Figure 3 (a) @@@@@
		// @@@@@ 3-PROFILES PAPER - paragraph 4.1 - (12) @@@@@
		// firstPart---->local 3 profile
				
		String firstPart = // n0
							this.num_empty.get() + TAB +
						   //n1d
						    (this.num_disc.get().subtract(this.num_disc_e.get())) + TAB +
						   //n1e
						     this.num_disc_e.get() + TAB +
						   //n2c
						     this.num_wedges_c.get() + TAB +
						   //n2e
						     this.num_wedges_e.get() + TAB +
						   //n3
						     this.num_triangles.get();
		
		// secondPart viene costruito solo quando globalFromLocal=true
		// local 4-profiles: n4local is calculated only if "-ca globalFromLocal=true" argument is set
		String secondPart = "";
		for (BigIntegerWritable n4local_i:this.n4local) {
			secondPart += TAB + n4local_i.get();
		}
		
		
		return firstPart + secondPart;
	}
	
	

}

/*
### GRAPHLAB ###

struct vertex_data_type {
  // A list of all its neighbors
  vid_vector vid_set;
  // The number of triangles this vertex is involved in.
#ifdef DOUBLE_COUNTERS
  double num_triangles;
  double num_wedges_e;
  double num_wedges_c;
  double num_disc;
  double num_empty;
#else
  size_t num_triangles;
  size_t num_wedges_e;
  size_t num_wedges_c;
  size_t num_disc;
  size_t num_empty;
#endif
  uvec u;

  std::vector<twoids> conn_neighbors;
  uvec n4local;


  // TRADOTTO CON IL METODO sum (OSBOLETE)
  vertex_data_type& operator+=(const vertex_data_type& other) {
    num_triangles += other.num_triangles;
    num_wedges_e += other.num_wedges_e;
    num_wedges_c += other.num_wedges_c;
    num_disc += other.num_disc;
    num_empty += other.num_empty;
    for (int i=0; i<USIZE; i++){
      u[i] += other.u[i];
      n4local[i] += other.n4local[i];
    }
    return *this;
  }
	

  void save(graphlab::oarchive &oarc) const {
    oarc << conn_neighbors;
    oarc << vid_set << num_triangles << num_wedges_e << num_wedges_c << num_disc << num_empty << u << n4local;
  }
  

  void load(graphlab::iarchive &iarc) {
    iarc >> conn_neighbors;
    iarc >> vid_set >> num_triangles >> num_wedges_e >> num_wedges_c >> num_disc >>num_empty>> u >> n4local;
  }
};
*/
