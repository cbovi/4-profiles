package it.uniroma1.di.fourprofiles.data.writable.edge;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;

public class EdgeData implements Writable {

	public BigIntegerWritable n3;
	public BigIntegerWritable n2c;
	
	
	/*
	 * It is not necessary to save following n2e and n1 members in hadoop file system
	 * because they are only used internally by Worker_Superstep1_GAS2 class, i.e. only in superstep 1.
	 * 
	 * Therefore n2e and n1 members are not used in write and readFields methods.
	 */
	public BigIntegerWritable n2e;
	public BigIntegerWritable n1;
	//public BigIntegerWritable triple;
	
	
	/*
	 ### OBSOLETE ###
	 
	 public BooleanWritable sample_indicator; 
	*/
	
	
	
	
	/*
	  ### GRAPHLAB ###
	  
		void sample_edge(graph_type::edge_type& edge) {
		  // pag. 486 in alto a sx
		  edge.data().n3 = 0;
		  edge.data().n2e = 0;
		  edge.data().n2c = 0;
		  edge.data().n1 = 0;
		  edge.data().eqn10_const = 0;
		
		  // TESI vengono scartati tutti gli archi la cui probabilità random è maggiore della probabilità sample_prob_keep
		  if(graphlab::random::rand01() < sample_prob_keep)
		    edge.data().sample_indicator = 1;
		  else
		    edge.data().sample_indicator = 0;
		}
	 */
	
	public EdgeData() {
		/*
		 ### PAPER ###
		 pag. 486 in alto a sx
		 
		### NOTES ###
		Non occorre inizializzare a zero n3 e n2c perchè verranno inizializzati direttamente in Worker_Superstep1_GAS2
		

		*/
		// TODO: verificare il motivo per cui senza queste inizializzazioni si verifica un NullPointerException
		this.n3 = new BigIntegerWritable();
		this.n2c = new BigIntegerWritable();
		
		this.n2e = new BigIntegerWritable();
		this.n1 = new BigIntegerWritable();
		//this.triple = new BigIntegerWritable();
		/*
		 OBSOLETE
		 
		java.util.Random r = new java.util.Random();
		
		TODO come passare questo parametro? forse devo usare il preCompute di giraph
	    TODO forse anche questa istruzione va tradotta dentro preComputer di GIRAPH
	         per deciderlo bisogna verificare se dentro questo metodo si ha accesso a tutti
	         gli archi, altrimenti la primissima volta che gli archi vengono trattati si
	         calcola il boolean indicator e poi, attraverso un flag, se il boolean indicator
	         è già calcolato non lo si calcola di nuovo
		
		NOTA il metodo Math.nextDouble() di seguito documentato non è disponibile nell'open jdk,
		     pertanto ho usato Math.random() TODO investigare se sia corretto usare questo metodo
		     https://docs.oracle.com/javase/7/docs/api/java/util/Random.html#nextDouble()
		  
		if (Math.random() < sample_prob_keep0.5 true)
			this.sample_indicator = new BooleanWritable(true);
		else
			this.sample_indicator = new BooleanWritable(false);
		*/
	}





	@Override
	public void write(DataOutput out) throws IOException {
		
		/*
		 ### OBSOLETE ###
		 sample_indicator.write(out);
		*/
		
		
		n3.write(out);
		n2c.write(out);
		n2e.write(out);
		n1.write(out);
		//triple.write(out);
		
	}
	
	
	
	@Override
	public void readFields(DataInput in) throws IOException {
		
		/*
		 ### OBSOLETE ###
	     sample_indicator.readFields(in);
		*/
		
		
		n3.readFields(in);
		n2c.readFields(in);
		n2e.readFields(in);
		n1.readFields(in);
		//triple.readFields(in);
	}

}


/*
### GRAPHLAB ###

struct edge_data_type {

#ifdef DOUBLE_COUNTERS
  double n3;
  double n2e;
  double n2c;
  double n1;
  double eqn10_const;
#else
  size_t n3;
  size_t n2e;
  size_t n2c;
  size_t n1;
  long int eqn10_const;
#endif

  bool sample_indicator;
  
  void save(graphlab::oarchive &oarc) const {
    oarc << n1 << n2e << n2c << n3 << sample_indicator << eqn10_const;
  }
  
  void load(graphlab::iarchive &iarc) {
    iarc >> n1 >> n2e >> n2c >> n3 >> sample_indicator >> eqn10_const;
  }
};

*/
