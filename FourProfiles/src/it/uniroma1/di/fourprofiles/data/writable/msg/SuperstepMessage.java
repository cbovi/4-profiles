package it.uniroma1.di.fourprofiles.data.writable.msg;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Writable;

import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDsHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.bigint.BigIntegerWritable;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;

//Classe costruita come indicato nell'esempio di stackoverflow
//https://stackoverflow.com/questions/17371861/apache-giraph-sendmessage

public class SuperstepMessage implements Writable, Configurable{
	
	private Configuration conf;
	//public LongWritable vertexId;
	//public LongHashSetWritable neighborhood;
	public BigIntegerWritable num_triangles;
	public BigIntegerWritable num_wedges_e;
	public TwoIDsHashSetWritable conn_neighbors;
	
	

	public SuperstepMessage() {
		//this.vertexId=new LongWritable(0);
		//this.neighborhood=new LongHashSetWritable();
		this.num_triangles=new BigIntegerWritable();
		this.num_wedges_e=new BigIntegerWritable();
		this.conn_neighbors=new TwoIDsHashSetWritable();
	}
	
	public SuperstepMessage(/*LongWritable id*/BigIntegerWritable num_triangle,BigIntegerWritable nume_wedges_e,TwoIDsHashSetWritable conn_neighbors) {
		//this.vertexId=id;
		//this.neighborhood=new LongHashSetWritable();
		this.num_triangles=num_triangle;
		this.num_wedges_e=nume_wedges_e;
		this.conn_neighbors=conn_neighbors;
	}
	
	public SuperstepMessage(/*LongWritable id*/ IntHashSetWritable neighborhood) {
		//this.vertexId=id;
		//this.neighborhood=neighborhood;
		this.num_triangles=new BigIntegerWritable();
		this.num_wedges_e=new BigIntegerWritable();
		this.conn_neighbors=new TwoIDsHashSetWritable();
	}
	
	

	@Override
	public Configuration getConf() {
		return this.conf;
	}

	@Override
	public void setConf(Configuration conf) {
		this.conf=conf;
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		//vertexId.readFields(in);
		//neighborhood.readFields(in);
		num_triangles.readFields(in);
		num_wedges_e.readFields(in);
		conn_neighbors.readFields(in);
		
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		//vertexId.write(out);
		//neighborhood.write(out);
		num_triangles.write(out);
		num_wedges_e.write(out);
		conn_neighbors.write(out);
		
	}
	
	public String toString() {
		
		return "num_triangles="+num_triangles+", num_wedges_e="+num_wedges_e+", conn_neighbors="+conn_neighbors.toString();
	}
	

}

