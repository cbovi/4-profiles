package it.uniroma1.di.fourprofiles.data.memory.clique;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

/**
 * It is used by Clique counting. 
 * 
 * It is used in VertexDataMem class to declare:
 *        
 *        public twoidsHashSetWritable conn_neighbors;
 * 
 * ### NOTES ###
 * Members are writable only for compatibility with data coming from hadoop file system.
 * This class is never memorized in hadoop file system.     
 */
public class TwoIDs implements Writable {
	
	public IntWritable first;
	public IntWritable second;
	
	
	public TwoIDs() {
		first = new IntWritable();
		second = new IntWritable();
	}

	@Override
	public String toString() {
		return "("+first.get()+", "+second.get()+")";
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		first.write(out);
		second.write(out);
		
	}
	@Override
	public void readFields(DataInput in) throws IOException {
		first.readFields(in);
		second.readFields(in);	
	}
	
}



/*
### GRAPHLAB ###

struct twoids{
   graphlab::vertex_id_type first;
   graphlab::vertex_id_type second;
 void save(graphlab::oarchive &oarc) const {
    oarc << first << second; }
  void load(graphlab::iarchive &iarc) {
    iarc >> first>>second; }
};

*/
