package it.uniroma1.di.fourprofiles.data.memory.clique;

import java.util.Iterator;

import it.uniroma1.di.fourprofiles.data.writable.set.HashSetWritable;


public class TwoIDsHashSetWritable extends /*Array*//*BigList*//*HashSet*/HashSetWritable<TwoIDs> {


  private static final long serialVersionUID = -5384314207777296208L;
 
 
  /** Default constructor for reflection */
  public TwoIDsHashSetWritable() {
    super();
  }
 
  
  /** Set storage type for this ArrayListWritable */
  @Override
  @SuppressWarnings("unchecked")
  public void setClass() {
    setClass(TwoIDs.class);
  }
  
  
  @Override
  public String toString() {
	  StringBuilder sb = new StringBuilder("[");
	  
	  Iterator<TwoIDs> iter = this.iterator();
	  long i=0;
	  while (iter.hasNext()) {
		  sb.append(iter.next().toString());
		  if (i != this.size()-1)
			  sb.append(", ");
		  i++;
	  }
	  
	  /*
	  for (int i=0; i<this.size(); i++) {
		  sb.append(this.get(i).toString());
		  if (i != this.size()-1)
			  sb.append(", ");
	  }
	  */
	  
	  sb.append("]");
	  return sb.toString();
  }
 

}

