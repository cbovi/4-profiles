package it.uniroma1.di.fourprofiles.data.memory.twohop;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.hadoop.io.IntWritable;

import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;

/**
 * Implementation of two hop histogram.
 * 
 * Two hop a list of pairs (vertex, count) where:
 * - vertex belongs to difference between source neighborhood and all target neighborhoods (excluding source)
 * - count is number of times that vertex occurs in the difference
 *
 * Two hop will be used to calculate u[11] in calculateUVector method of Worker_Superstep1_GAS2 class
 *
 *
 * ### PAPER ###
 * APPENDIX
 * A. IMPLEMENTATION DETAILS
 * To improve the practical performance of 4-Prof-Dist
 * (see Algorithm 1 for pseudocode), we handle low and high
 * degree vertices differently. 
 * 
 * THIS PART IS RELATED TO VertexDataMem CLASS IN WHICH VERTEX DEGREE (I.E. NEIGHBORHOOD) IS IMPLEMENTED AS LongHashSetWritable:
 * As in GraphLab PowerGraph's
 * standard triangle counting, cuckoo hash tables are used if
 * the vertex degree is above a threshold. 
 * 
 * THIS PART IS REALTED TO TwoHop CLASS IN WHICH TWO HOP IS IMPLEMENTED AS HashMap
 * Now, we also threshold
 * vertices to determine whether the 2-hop histogram in
 * Section 2.3 will be either a vector or an unordered map.
 * 
 * This is because sorting and merging operations on a vector
 * scale poorly with increasing degree size, while an unordered
 * map has constant lookup time. We found that this
 * approach successfully trades off processing time and memory
 * consumption.
 *
 *
 * ### GRAPHLAB ###
 * In Graphlab program the Two Hop Histogram is implemented by:
 * - struct nbh_count
 * 		corresponding to this class
 * 
 * - struct idcount
 * 		corresponding to pair <LongWritable,Long> used to declare map_vertex_count 
 * 
 */
public class TwoHop {
	
	
	public Map<IntWritable,Integer> map_vertex_count;
	
	
	
	public static final Integer INITIAL_VALUE = new Integer(1);
	
	
	/*
	   ### GRAPHLAB ###
	   In Graphlab unordered_map type is used for memorize two hop histogram:
	   		
	   		boost::unordered_map<graphlab::vertex_id_type,size_t> idc_map;
	 
	   The corresponding java type is HashMap.
	   	
	   Follows some documentation:
	   
	   1) unordered_map in C++/Graphlab
	   FROM http://www.cplusplus.com/reference/unordered_map/unordered_map/
	   Internally, the elements in the unordered_map are not sorted in any particular order with respect to either their key or mapped values, 
	   but organized into buckets depending on their hash values to allow for fast access to individual elements directly by their key values 
	   (with a constant average time complexity on average).
	   
	   2) HashMap in JAVA
	   FROM https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html
	   This class makes no guarantees as to the order of the map;
	   This implementation provides constant-time performance for the basic operations (get and put), assuming the hash function disperses the 
	   elements properly among the buckets.
	   
	   3) Furthermore this links explains that unordered_map corresponds to HashMap 
	   https://www.tangiblesoftwaresolutions.com/articles/java_equivalent_to_cplus_unordered_map.html
	*/
	
	public TwoHop() {
		
		this.map_vertex_count=new HashMap<IntWritable,Integer>();
		
	}
	

	
	/**
	 * 	
	 * ### GRAPHLAB ### 
	 * In GraphLab implementation of 4-profiles calculateTwoHopHistogram method corresponds to nbh_intersect_complement method
	 */	
	public static TwoHop calculateTwoHop(
			final IntHashSetWritable targetNeighborhood,
			final IntHashSetWritable sourceNeighborhood,
			IntWritable sourceVertexId) {
		
		/* 
		   A copy of the targetNeighborhood is made in order to leave the targetNeighborhood unaltered
		   so it can be used it for later calculations. 
		   On this copy will be executed the removeAll method that stores the difference between target neighborhood
		   and source neighborhood in differenceBetweenTargetAndSource.
		   
		   differenceBetweenTargetAndSource.removeAll(sourceNeighborhood);
		  
		  
		   (targetNeighborhood has obtained from vertexM.mapOfTargetNeighborhoods in Worker_Superstep1_GAS2) */
		IntHashSetWritable differenceBetweenTargetAndSource = new IntHashSetWritable(targetNeighborhood);
		
		TwoHop diffMap = new TwoHop();
		
		
		/* JAVA removeAll
		   https://docs.oracle.com/javase/7/docs/api/java/util/Collection.html#removeAll(java.util.Collection)
		   Removes all of this collection's elements that are also contained in the specified collection 
		   (optional operation). After this call returns, this collection will contain no elements in 
		   common with the specified collection. */
		
		/* C++ set_difference
		   std::set_difference(smaller_set.vid_vec.begin(), smaller_set.vid_vec.end(),
                               larger_set.vid_vec.begin(), larger_set.vid_vec.end(),

                            http://www.cplusplus.com/reference/iterator/back_inserter/
                            Constructs a back-insert iterator that inserts new elements at the end of x.
                            A back-insert iterator is a special type of output iterator designed to allow algorithms that usually overwrite
                            elements (such as copy) to instead insert new elements automatically at the end of the container.

                            std::back_inserter(temp));
                               
                               
	        1) https://books.google.it/books?id=YTj0B0fcdqIC&pg=PA141&lpg=PA141&dq=std::set_difference+java&source=bl&ots=-xdjudBTcM&sig=KqQbxXS6hQ90ZJlTzCiTxO7KIg0&hl=it&sa=X&ved=0ahUKEwiE-6m0u_XZAhXF1qQKHVtOBXcQ6AEIRzAD#v=onepage&q=std%3A%3Aset_difference%20java&f=false
	        2) http://www.cplusplus.com/reference/algorithm/set_difference/
	        The difference of two sets is formed by the elements that are present in the first set, but not in the second one.
	        The elements copied by the function come always from the first range, in the same order.                               
        */

		// difference is memorized in differenceBetweenTargetAndSource 
		differenceBetweenTargetAndSource.removeAll(sourceNeighborhood);  
		

		
		/* ### GRAPHLAB ###
		p is the source vertex 
		
        for (size_t k=0;k<temp.size();k++) {
                        
            if(p!=temp.at(k)) {
            
              idcount a;
              
              a.vert_id=temp.at(k);
              a.count=1;

              // http://www.cplusplus.com/reference/vector/vector/push_back/
              // Adds a new element at the end of the vector, after its current last element.
             
              diffvec.push_back(a);
            }
        }
		*/
		
		// Iterating of differenceBetweenTargetAndSource in order to populate diffMap (excluding source vertex)
		for (IntWritable vertexId:differenceBetweenTargetAndSource) {
			// source vertex is excluded from two hop histogram
			if (sourceVertexId.get() != vertexId.get()) {
				diffMap.map_vertex_count.put(
						vertexId, 
						INITIAL_VALUE);  //inizializzato a 1
			}
		}
		
		
		return diffMap;
		
	}

	
	
	/**
	 * This method performs the sum of differences contained in this map with the map passed as argument.
	 * 
	 * 
	 * ### GRAPHLAB ### 
	 * In GraphLab implementation of 4-profiles sumDifferenceCounts method corresponds to merge_many method
	 */	
	public void sumTwoHop(TwoHop arg) {

		// if this map is empty, the sum is simply obtained putting all pairs (key, value) of arg in this 
		if (this.map_vertex_count.size() == 0) {
			
			this.map_vertex_count.putAll(arg.map_vertex_count);
			
		}
		else {
			/* ... otherwise it is necessary to iterate on two hop histogram passed as parameter in order
			   to verify if it contains vertexes already contained in this map */
			Iterator<IntWritable> iter = arg.map_vertex_count.keySet().iterator();
			while (iter.hasNext()) {
				IntWritable paramVertexId = iter.next();
				Integer paramCount = arg.map_vertex_count.get(paramVertexId);

				
				Integer thisCount = this.map_vertex_count.get(paramVertexId);
				// if this map doesn't contain key paramVertexId then get returns a null value
				if (thisCount != null) {
					// parameter contains a vertex already contained in this map
					// so it necessary to update the count of this map with the count of param
					// (put overwrites previous value of count for paramVertexId)
					this.map_vertex_count.put(paramVertexId, thisCount + paramCount);
				}
				else {
					// this map doesn't contain paramVertexId so a new pair (paramVertexId, paramCount) must be inserted in this map
					this.map_vertex_count.put(paramVertexId, paramCount);
				}
			}
			
		}	
	
		
	}
	
	
	
	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("{");
		Iterator<IntWritable> iter = this.map_vertex_count.keySet().iterator(); 
		long j=0;
		while (iter.hasNext()) {
			  
			IntWritable vertexId = iter.next();
		    
			sb.append("(vertex=");
			sb.append(vertexId);
			sb.append(", count=");
			sb.append(this.map_vertex_count.get(vertexId));
			sb.append(")");
			if (j!=this.map_vertex_count.size()-1) { 
				sb.append(",");
			}
			j++;
		}
		sb.append("}");
		  
		return sb.toString();
	}	
	
}
