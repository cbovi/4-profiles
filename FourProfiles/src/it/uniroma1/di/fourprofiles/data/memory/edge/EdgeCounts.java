package it.uniroma1.di.fourprofiles.data.memory.edge;

import java.math.BigInteger;

import it.uniroma1.di.fourprofiles.data.memory.twohop.TwoHop;

/*
 * Used only in Worker_Superstep1_GAS2
 */
public class EdgeCounts {
	
	public BigInteger n1=BigInteger.ZERO;
	public BigInteger n3=BigInteger.ZERO ;
	public BigInteger n1_double = BigInteger.ZERO;
	public BigInteger n3_double = BigInteger.ZERO;
	public BigInteger n1_n3 = BigInteger.ZERO;
	public BigInteger n2c_n2e = BigInteger.ZERO;
	public BigInteger n2e = BigInteger.ZERO;
	public BigInteger n2c = BigInteger.ZERO;
	public BigInteger n2c_double = BigInteger.ZERO;
	public BigInteger n2e_double = BigInteger.ZERO;
	public BigInteger n1_n2c = BigInteger.ZERO;
	public BigInteger n1_n2e = BigInteger.ZERO;
	public BigInteger n2c_n3 = BigInteger.ZERO;
	public BigInteger n2e_n3 = BigInteger.ZERO;
	//public BigInteger triple = BigInteger.ZERO;
    
    /*
	 * Two hop a list of pairs (vertex, count) where:
	 * - vertex belongs to difference between source neighborhood and all target neighborhoods (excluding source)
	 * - count is number of times that vertex occurs in the difference
	 * 
	 * Two hop will be used to calculate u[11] in calculateUVector method of Worker_Superstep1_GAS2 class
     */
    public TwoHop twohop = new TwoHop(); 
    
}
