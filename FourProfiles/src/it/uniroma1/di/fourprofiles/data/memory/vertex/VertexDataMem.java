package it.uniroma1.di.fourprofiles.data.memory.vertex;

import java.util.HashMap;

import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.log4j.Logger;

import it.uniroma1.di.fourprofiles.data.memory.clique.TwoIDsHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.edge.EdgeData;
import it.uniroma1.di.fourprofiles.data.writable.set.IntHashSetWritable;
import it.uniroma1.di.fourprofiles.data.writable.vertex.VertexData;
import it.uniroma1.di.fourprofiles.util.Util;

/**
 * These vertex datas are not Writable because they are needed only in superstep 1 by GAS 2 and GAS 3.
 * Hence these vertex datas are not stored in vertex_data class (that is Writable).
 * 
 * 
 *
 *
 * ### NOTES ###
 * 1) 
 * source and target neighboorhods are declared of type LongHashSetWritable.
 * - Writable because vertex ids are Writable, hence contains method works only if both collections and vertex ids are Writable.
 *   This because contains method internally uses equals method that properly works only if both collections and vertex ids are 
 *   of the same type, i.e. Writable.
 *     
 *   contains method is used in:
 *   1) execute_gas3 method of Worker_Superstep1_GAS3 class
 *   2) compute method of Worker_Superstep2_GAS4 class
 *    
 * - HashSet instead of ArraList because calculateTwoHop method invoked by Worker_Superstep1_GAS2 class in turn invokes 
 *   AbstractSet.removeAll method that in turn invokes contains method.
 *     
 *   contains method for a HashSet is O(1) compared to O(n) for a ArrayList.
 *   (https://stackoverflow.com/questions/32552307/hashset-vs-arraylist-contains-performance)
 *     
 *   calculateTwoHop method is invoked by Worker_Superstep1_GAS2.calculateTwoHop() to calculate two hop that is the difference
 *   between target and source neighborhoods excluding source.
 *   
 *   
 *   
 *   
 * ### PAPER ###
 * APPENDIX
 * A. IMPLEMENTATION DETAILS
 * To improve the practical performance of 4-Prof-Dist
 * (see Algorithm 1 for pseudocode), we handle low and high
 * degree vertices differently. 
 * 
 * THIS PART IS RELATED TO VertexDataMem CLASS IN WHICH VERTEX DEGREE (I.E. NEIGHBORHOOD) IS IMPLEMENTED AS LongHashSetWritable CORRESPONDING TO cuckoo hash tables
 * As in GraphLab PowerGraph's
 * standard triangle counting, cuckoo hash tables are used if
 * the vertex degree is above a threshold. 
 * 
 * THIS PART IS REALTED TO TwoHop CLASS IN WHICH TWO HOP IS IMPLEMENTED AS HashMap
 * Now, we also threshold
 * vertices to determine whether the 2-hop histogram in
 * Section 2.3 will be either a vector or an unordered map.
 * 
 * This is because sorting and merging operations on a vector
 * scale poorly with increasing degree size, while an unordered
 * map has constant lookup time. We found that this
 * approach successfully trades off processing time and memory
 * consumption.
 */
public class VertexDataMem {

	
	private final static Logger logger = Logger.getLogger(VertexDataMem.class);
	//private static final Logger logger = LogManager.getLogger(VertexDataMem.class);//LOG4J2
		
	public long totalNumVertices;
	public long totalNumEdges;
	public long superstep;
	
	public MapWritable mapOfTargetNeighborhoods;
	
	public HashMap<IntWritable, IntHashSetWritable> mapOfIntersections;
	
	/**
	 * ### PAPER ###
	 * 4-PROFILES PAPER - page 486 - paragraph 2.2 Clique Counting 
	 * 
	 * 
	 * conn_neighbors 
	 * It is a set of twoids obtained iterating on all intersections between source neighborhood and target neighborhoods
	 * In every twoids:
	 * - the first one is the always the source vertex id
	 * - the second one is a vertex belonging to neighborhoods intersection always having id greater than source vertex id
	 * 
	 * conn_neighboors is calculated by calculateConn_neighbors method of Worker_Superstep1_GAS2 class
	 * 
	 * 
	 * - if "-ca global=true" argument is set, then conn_neighboors will be used by Worker_Superstep1_GAS3
	 *   to calculate eq10_const_sum
	 * 
	 * - if "-ca globalFromLocal=true" argument is set, then conn_neighboors will be used by Worker_Superstep2_GAS4
	 *   to calculate ecounts_h8v, ecounts_h10v
	 * 
	 */
	public TwoIDsHashSetWritable conn_neighbors;
	
	
	
	//MODIFICA_VICINATO
	public String sourceNeighborhoodStr;
	
	/**
	 * Source neighborhood
	 * 
	 * ### GRAPHLAB ###
	 * The corresponding implementation in Graphlab is the struct vid_set having vid_vec member.
	 * vid_vec is ordered by assign method of struct vid_set, in java the ordering is not executed because it is directly used an unordered hashset.
	 * (anyway to obtain a ordered hashset in Java could be used TreeSet but obviously it has a greater cost of execution)
	 */
	//MODIFICA_VICINATO
	public IntHashSetWritable sourceNeighborhood;
	
	
	
	public VertexDataMem(
			Vertex<IntWritable, VertexData, EdgeData> vertex,
			MapWritable mapOfTargetNeighborhoods,
			long totalNumVertices,
			long totalNumEdges,
			long superstep) {
		
		
		this.totalNumVertices = totalNumVertices;
		this.totalNumEdges = totalNumEdges / 2;
		this.superstep = superstep;
		
			
		this.mapOfTargetNeighborhoods=mapOfTargetNeighborhoods;
		
		        	
		/* ### NOTES ### 
           intersection is declared of LongHashSetWritable type because it will be used to populate twoids of conn_neighbors in WorkerSuperstep1_GAS2 
         */
		this.mapOfIntersections = new HashMap<IntWritable, IntHashSetWritable>();
    	
    	
    	/*
		 * ### PAPER ###
		 * 4-PROFILES PAPER - page 486 - paragraph 2.2 Clique Counting
    	 * 
    	 * ### NOTES ###
    	 * insieme di twoids costruiti scorrendo l'intersezione tra il vicinato del sorgente e il vicinato del destinatario,
         * nello specifico in ogni twoids il primo è sempre il sorgente e il secondo è sempre uno dei vertici dell'intersezione avente
         * id maggiore del sorgente
    	 * 
    	 * ### GRAPHLAB ###
    	 * std::vector<twoids> conn_neighbors;
    	 */
		this.conn_neighbors = new TwoIDsHashSetWritable(); 
		
		//MODIFICA_VICINATO
		this.sourceNeighborhood = new IntHashSetWritable();
		this.sourceNeighborhoodStr = Util.getNeighborhood(vertex, sourceNeighborhood);

	}
	
	

}
